﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;
using csci342;

namespace Chaos_Plotter
{
   
    public partial class ChaosForm : Form
    {
        private bool controlLoaded = false;
        private double initialValue
        {
            get { return Double.Parse(initial.Text); }
        }
        private double A 
        {
            get { return Double.Parse(txtA.Text); }
        }
        private IList<Point2D> orbit = new List<Point2D>();

        public ChaosForm()
        {
            InitializeComponent();
            
        }
        private double forx = 0.0 - 0.001;
        private void chaosForm_Load(object sender, EventArgs e)
        {

        }


        private void plotArea_Load(object sender, EventArgs e)
        {
            controlLoaded = true;
            GL.ClearColor(Color.White);
            GL.Color3(Color.Black);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, 1, 0, 1, -1, 1);
            
        }

        private void plotArea_Paint(object sender, PaintEventArgs e)
        {
            if (controlLoaded == false) return;
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            if (radioButton2.Checked == true) {
                GL.Begin(PrimitiveType.Points);
                GL.MatrixMode(MatrixMode.Projection);
                GL.Ortho(-1, 1, -1, 1, -1, 1);
                GL.Color3(Color.Orange);

                for (double x = 0; x <= 1; x += 0.001)
                {
                    GL.Vertex2(x, A * x * (1 - x));
                }
                foreach (var Point2D in orbit)
                {
                    GL.Vertex2(Point2D.X, A * Point2D.X * (1 - Point2D.X));
                }
            }
            else { 
            GL.Begin(PrimitiveType.LineStrip);
            GL.MatrixMode(MatrixMode.Projection);
            GL.Ortho(-1, 1, -1, 1, -1, 1);
            GL.Color3(Color.Orange);

            for (double x = 0; x <= 1; x += 0.001)
            {
                GL.Vertex2(x, A * x * (1 - x));
            }

            
            foreach(var Point2D in orbit){
                GL.Vertex2(Point2D.X, A * Point2D.X * (1 - Point2D.X));
            }
           }
            GL.End();
            glControl1.SwapBuffers();
 

        }

        private void plotArea_Resize(object sender, EventArgs e)
        {
            if (controlLoaded == false) return;
            GL.Viewport(5, 15, 400, 600);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lblA_Click(object sender, EventArgs e)
        {

        }

        private void plotArea_Click(object sender, EventArgs e)
        {

        }

        private double f(double inV)
        {
            forx += 0.001;
            return inV * forx * (1 - forx);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            orbit = new List<Point2D>();
            button2.Enabled = true;
            orbit.Add(new Point2D(initialValue, 0));
            orbit.Add(new Point2D(initialValue, f(initialValue)));
            glControl1.Invalidate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (double i = Double.Parse(numlterations.Text); i > 0; i = -1)
            {
            Point2D lastPont = orbit[-1];
            orbit.Add(new Point2D(lastPont.Y, lastPont.Y));
            orbit.Add(new Point2D(lastPont.Y, f(lastPont.Y)));
            glControl1.Invalidate();
        } 
        }

        private void txtA_TextChanged(object sender, EventArgs e)
        {
            glControl1.Invalidate();
        }

        private void initial_TextChanged(object sender, EventArgs e)
        {
            glControl1.Invalidate();
        }

        private void numlterations_ValueChanged(object sender, EventArgs e)
        {
            glControl1.Invalidate();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            glControl1.Invalidate();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            glControl1.Invalidate();
        }
    }
}
