﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using csci342.Text;
using OpenTK;

namespace Chart_Creator
{
    public abstract class AbstractChart
    {
        public const string ColorString = "Colors";
        public static bool save = false;
        public string Title { get; set; }
        protected double availableWidth;

        public Color3 TitleColor { get; set; }

        public byte[,] Colors = {
            {69, 114, 167},
            {170, 70, 67},
            {137, 165, 78},
            {113, 88, 143},
            {65, 152, 175},
            {219, 132, 61},
            {147, 169, 207},
            {209, 147, 146}
        };

        protected Rectangle Viewport;

        public Boolean ColorsSpecified = false;

        public abstract void ReadFile(string firstLine, StreamReader reader);

        public abstract void SaveFile(StreamWriter writer);

        public abstract void Draw(GLControl glControl);

        public abstract ICollection<string> LegendEntries
        {
            get;
        }

        public abstract char ChartTypeSpecifier { get; }

        public abstract Color3 GetLegendColor(string item);

        public abstract Selection GetSelection(double wx, double wy);

        public abstract Selection SetLegendSelection(string changeLegend);

        public abstract void ClearSelection();

        public abstract CoordinateSystem World { get; }

        public abstract void DrawLegendMarker(string seriesName, double x, double y);

        public virtual Rectangle GetViewport(double availableWidth, double availableHeight)
        {
            this.availableWidth = availableWidth;
            Viewport = new Rectangle(0, 0, (int) availableWidth, (int) availableHeight);
            return Viewport;
        }        

        protected void ReadColors(StreamReader reader)
        {
            ColorsSpecified = true;
            char[] delimiters = {' ', '\t'};
            for (var i = 0; i < 8; i++)
            {
                var currentLine = reader.ReadLine();
                String[] colors = currentLine.Split(delimiters);
                if (colors.Length != 3)
                {
                    throw new Exception("Color specification must contain exactly 3 components");
                }
                
                Colors[i, 0] = Byte.Parse(colors[0]);
                Colors[i, 1] = Byte.Parse(colors[1]);
                Colors[i, 2] = Byte.Parse(colors[2]);
            }
        }

        public static AbstractChart LoadChartFromFile(string filename)
        {
            AbstractChart newChart = null;
            using (StreamReader reader = new StreamReader(filename))
            {                
                String chartType = reader.ReadLine();
                if (chartType.Length != 1)
                {
                    throw new FormatException("The length of the chart type specified on the first line of the file must note be 0");
                }

                switch (Char.ToLower(chartType[0]))
                {
                    case PieChart.TypeSpecifier:
                        newChart= new PieChart();
                        break;
                    case ScatterPlot.TypeSpecifier:
                        newChart = new ScatterPlot();
                        break;
                    case DonutChart.TypeSpecifier:
                        newChart = new DonutChart();
                        break;
                    case ColumnChart.TypeSpecifier:
                        newChart = new ColumnChart();
                        break;
                    default:
                        throw new FormatException("Invalid chart type " + chartType[0]);                        
                }

                newChart.Title = reader.ReadLine();
                
                string nextLine = reader.ReadLine();
                if (nextLine.Equals(AbstractChart.ColorString))
                {
                    newChart.ReadColors(reader);
                    nextLine = reader.ReadLine();
                }
                else
                {
                    newChart.UseDefaultColors();
                }
                newChart.ReadFile(nextLine, reader);
            }
            
            newChart.TitleColor = new Color3(0, 0, 0);

            return newChart;
        }

        protected void UseDefaultColors()
        {
            for (int i = 0; i < Colors.GetLength(0); i++)
            {
                for (int j = 0; j < Colors.GetLength(1); j++)
                {
                    Colors[i, j] = ChartCreator.DefaultColors[i, j];
                }
            }

            ColorsSpecified = false;
        }
    }
}
