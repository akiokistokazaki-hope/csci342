using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using ArcDrawer;
using csci342;
using csci342.Text;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using ChartCreatorSolution;
using OpenTK;
using System.Collections.Generic;

namespace Chart_Creator
{
    public partial class ChartCreator : Form
    {
        private Selection _currentSelection;
        private static readonly Color3 SelectionHandleColor = new Color3 (200, 200, 255);
        

        private const int LegendMarginY = 10;
        private const int LegendMarginX = 10;
        private const int LegendTextMargin = 10;
        public const int BoxWidth = 20;
        public const int BoxHeight = 10;

        public static int sx = 0;
        public static int sy = 0;


        public static Rectangle _chartViewport = new Rectangle();
        //public static double divX = 1.0;
        //public static double divY= 1.0;
        private static double height;


        private static double worldWidth;
        private static double worldHeight;

        private static double worldLeft;
        private static double worldBottom;

        private int _fontID;
        private int _legendWidth;

        //  The maximum width, in pixels, of an item label
        //  NOT the width of the string that occurs most frequently
        private int _maxItemWidth;

        private Point2D _chartTitleLocation;
        private Size _titleSize;

        private int TitleWidth => _titleSize.Width;
        private int TitleHeight => _titleSize.Height;

        private bool _chartTitleSelected;

        private AbstractChart _chart;        

        public bool FileOpened => _chartFileName != null;

        private string _fileToOpenOnLoad;

        public static readonly byte[,] DefaultColors =
        {
            {69, 114, 167},
            {170, 70, 67},
            {137, 165, 78},
            {113, 88, 143},
            {65, 152, 175},
            {219, 132, 61},
            {147, 169, 207},
            {209, 147, 146}
        };

        private string _chartFileName;

        public ChartCreator()
        {
            InitializeComponent();                                    
            _fileToOpenOnLoad = null;
            _chartFileName = null;
        }

        public ChartCreator(string fileToOpen) : this()
        {
            _fileToOpenOnLoad = fileToOpen;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowOpenFileDialog();
        }

        private void ShowOpenFileDialog()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                LoadFile(openFileDialog1.FileName);
            }
        }
        private void DrawChartTitle() {
            int offset = 2;
            
            var titleColor = _chart.TitleColor;
            GL.Color3(titleColor.Red, titleColor.Green, titleColor.Blue); 
                      
            GL.Viewport(0, 0, canvas.Width, canvas.Height);
            Utilities.SetWindow(0, canvas.Width, 0, canvas.Height);

            _chartTitleLocation = new Point2D(canvas.Width / 2 - TitleWidth / 2, canvas.Height - 1.5*TitleHeight);     
            TextDrawer.DrawString(_chart.Title, _chartTitleLocation.X, _chartTitleLocation.Y, _fontID);       

            if (_chartTitleSelected)
            {
                GL.Color3(SelectionHandleColor.Red, SelectionHandleColor.Green, SelectionHandleColor.Blue);
                GL.Begin(PrimitiveType.LineLoop);
                {
                    GL.Vertex2(_chartTitleLocation.X-offset, _chartTitleLocation.Y-offset);
                    GL.Vertex2(_chartTitleLocation.X + TitleWidth + offset, _chartTitleLocation.Y - offset);
                    GL.Vertex2(_chartTitleLocation.X + TitleWidth + offset, _chartTitleLocation.Y + TitleHeight + offset);
                    GL.Vertex2(_chartTitleLocation.X - offset, _chartTitleLocation.Y + TitleHeight + offset);
                }
                GL.End();
                
                DrawSelectionHandle(_chartTitleLocation.X-offset, _chartTitleLocation.Y-offset, 2);
                DrawSelectionHandle(_chartTitleLocation.X+TitleWidth+offset, _chartTitleLocation.Y-offset, 2);
                DrawSelectionHandle(_chartTitleLocation.X+TitleWidth+offset, _chartTitleLocation.Y+TitleHeight+offset, 2);
                DrawSelectionHandle(_chartTitleLocation.X-offset, _chartTitleLocation.Y+TitleHeight+offset, 2);

            }
        }

        private void LoadFile(String filename)
        {
            try
            {
                _chart = AbstractChart.LoadChartFromFile(filename);
            }
            catch (FormatException e)
            {
                MessageBox.Show(this, e.Message, $"Unable to load file {filename}");
                return;
            }
            
            _titleSize = FontGenerator.GetSize(_chart.Title);                        
            _maxItemWidth = -1;            
            _chartFileName = filename;

            _chartTitleSelected = false;            

            canvas.Invalidate();
        }
                
        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            if (!canvas.IsHandleCreated) return;

            GL.ClearColor(Color.White);
            GL.Clear(ClearBufferMask.ColorBufferBit);
            
            if (!FileOpened)
            {
                return;
            }

            if (_maxItemWidth == -1)
            {
                foreach (String item in _chart.LegendEntries)
                {
                    var size = FontGenerator.GetSize(item);
                    _maxItemWidth = Math.Max(_maxItemWidth, size.Width);
                }
                
                _legendWidth = LegendMarginX + BoxWidth + LegendTextMargin + _maxItemWidth + LegendMarginX /*+ TextMarginRight*/;
            }
            
            GL.Clear(ClearBufferMask.ColorBufferBit);            
            DrawChartTitle();
            DrawLegend();

            Utilities.SetWindow(_chart.World.Left, _chart.World.Right, _chart.World.Bottom, _chart.World.Top);
            
            double widthAvailable = (int) (canvas.Width - _legendWidth);
            double heightAvailable = canvas.Height - 1.5*TitleHeight;

            _chartViewport = _chart.GetViewport(widthAvailable, heightAvailable);

            Point2D availableCenter = new Point2D(widthAvailable / 2, heightAvailable / 2);
            int viewportLeft = (int) (availableCenter.X - _chartViewport.Size.Width / 2);
            int viewportBottom = (int)(availableCenter.Y - _chartViewport.Size.Height / 2);
            _chartViewport.Location = new Point(viewportLeft, viewportBottom);

            GL.Viewport(_chartViewport.Location.X, _chartViewport.Location.Y, _chartViewport.Width, _chartViewport.Height);

            _chart.Draw(canvas);

            canvas.SwapBuffers();
        }

        public static void DrawSelectionHandle(double x, double y, double selectionRadius)
        {
            //  Save the current drawing color
            double[] currentColor = new double[4];
            GL.GetDouble(GetPName.CurrentColor, currentColor);

            GL.Color3(SelectionHandleColor.Red, SelectionHandleColor.Green, SelectionHandleColor.Blue);

            GL.Begin(PrimitiveType.Polygon);
            {
                Arc.DrawCircle(x, y, selectionRadius);
            }
            GL.End();

            GL.Color3(0.1, 0.1, 0.1);
            GL.Begin(PrimitiveType.LineLoop);
            {
                Arc.DrawCircle(x, y, selectionRadius);
            }
            GL.End();

            //  Restore the previous drawing color
            GL.Color3(currentColor[0], currentColor[1], currentColor[2]);
        }

        public static void DrawSelectionHandle (double x, double y) {
            DrawSelectionHandle(x, y, 0.05);
        }
        
        private void DrawLegend()
        {                        
            int viewportLeft = canvas.Width - _legendWidth - 10;
            int viewportHeight = 10 + _chart.LegendEntries.Count * (BoxHeight + 10);

            GL.Viewport(viewportLeft, canvas.Height/2 - viewportHeight/2, _legendWidth, viewportHeight);

            int worldHeight = canvas.Height - 2 * LegendMarginY;

            Utilities.SetWindow(0, _legendWidth, 0, viewportHeight);

            int i = 0;
            int y = 10 + (BoxHeight + 10) * (_chart.LegendEntries.Count-1);

            foreach (String item in _chart.LegendEntries)
            {
                Color3 c = _chart.GetLegendColor(item);
                GL.Color3(c.Red, c.Green, c.Blue);
                _chart.DrawLegendMarker(item, LegendMarginX, y);
                i++;

                TextDrawer.DrawString(item, LegendMarginX + BoxWidth + LegendTextMargin, y - 5, _fontID);
                y -= (BoxHeight + 10);
            }
            y = 10 + (BoxHeight + 10) * (_chart.LegendEntries.Count);
            Utilities.SetForegroundBlack();
            GL.Begin(PrimitiveType.LineLoop);
            {
                GL.Vertex2(0, 0);
                GL.Vertex2(_legendWidth - 1, 0);
                GL.Vertex2(_legendWidth - 1, y);
                GL.Vertex2(0, y);
            }
            GL.End();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void simpleOpenGlControl1_MouseClick(object sender, MouseEventArgs e)
        {
            
            if (!FileOpened) return;
          

            sx = e.X;
            sy = canvas.Height - e.Y;
            _currentSelection = null;
            _chartTitleSelected = false;
            _chart.ClearSelection();


            if (InLegengArea(e)) {
                int topY = canvas.Height / 2 + (10 + _chart.LegendEntries.Count * (BoxHeight + 10)) / 2;

                int currentTopY = topY;
                int currentbottomY = topY - (BoxHeight + 10);

                int topX = canvas.Width - _legendWidth - 10;
                int bottomX = canvas.Width - 10;
                foreach (string s in _chart.LegendEntries)
                {
                    if (inRect(new Point2D(topX, currentTopY), new Point2D(bottomX, currentbottomY), new Point2D(sx, sy)))
                    {
                        _currentSelection = _chart.SetLegendSelection(s);
                    }
                    currentTopY -= (BoxHeight + 10);
                    currentbottomY -= (BoxHeight + 10);
                }
            }
            else if (InChartArea(e))
            {
                _chartTitleSelected = false;
                worldWidth = _chart.World.Right - _chart.World.Left;
                worldHeight = _chart.World.Top - _chart.World.Bottom;
                worldLeft = _chart.World.Left;
                worldBottom = _chart.World.Bottom;
                height = canvas.Height;
                double wx = (sx - _chartViewport.Location.X) / (_chartViewport.Width * 1.0) * worldWidth + _chart.World.Left;
                double wy = (sy - _chartViewport.Location.Y) / (_chartViewport.Height * 1.0) * worldHeight + _chart.World.Bottom;

                if (AbstractChart.save) {
                    saveToolStripMenuItem.Enabled = true;
                    canvas.Invalidate();
                }
                _currentSelection = _chart.GetSelection(wx, wy);
            }
            else
            {
                if (InChartTitle(e))
                {
                    _currentSelection = new ChartTitleSelection(_chart.TitleColor);
                    _chartTitleSelected = true;
                }
            }

            if (_currentSelection != null)
            {
                menuChangeColor.Enabled = _currentSelection.CanChangeColor;
                menuChangeFillColor.Enabled = _currentSelection.CanChangeFillColor;
                formatMenu.Enabled = true;
            }
            else
            {
                formatMenu.Enabled = false;
            }
            //divX=(_chartViewport.Width * 1.0) * (_chart.World.Right - _chart.World.Left) + _chart.World.Left;
            //divY= (_chartViewport.Height * 1.0) * (_chart.World.Top - _chart.World.Bottom) + _chart.World.Bottom;
            canvas.Invalidate();
        }

        public static Point2D getWorldPoint(double ex, double ey) {
            int sx = (int)ex;
            int sy = (int)(height - ey);
            double wx = (sx - _chartViewport.Location.X) / (_chartViewport.Width * 1.0) * worldWidth + worldLeft;
            double wy = (sy - _chartViewport.Location.Y) / (_chartViewport.Height * 1.0) * worldHeight + worldBottom;
            //return null;
            return new Point2D(wx,wy);

        }

        private bool InLegengArea(MouseEventArgs e)
        {
            int topX = canvas.Width - _legendWidth - 10;
            int bottomX = canvas.Width - 10;
            int topY = canvas.Height / 2 + (10 + _chart.LegendEntries.Count * (BoxHeight + 10)) /2;
            int bottomY = canvas.Height / 2 - (10 + _chart.LegendEntries.Count * (BoxHeight + 10)) / 2;

            int sx = e.X;
            int sy = canvas.Height - e.Y;
            return inRect(new Point2D(topX,topY),new Point2D(bottomX,bottomY), new Point2D(sx,sy));
        }

        private bool InChartTitle(MouseEventArgs e)
        {
            int sx = e.X;
            int sy = canvas.Height - e.Y;

            return sx >= _chartTitleLocation.X && sx <= _chartTitleLocation.X + TitleWidth &&
                   sy >= _chartTitleLocation.Y;
        }

        private bool InChartArea(MouseEventArgs e)
        {
            int sx = e.X;
            int sy = canvas.Height - e.Y;

            return
                sx >= _chartViewport.Location.X && sx <= _chartViewport.Location.X + _chartViewport.Width &&
                sy >= _chartViewport.Location.Y && sy <= _chartViewport.Location.Y + _chartViewport.Height;
        }

        /*private void ChangeColor(Color3 fromColor, Color toColor)
        {
            var found = false;
            for (int colorIndex = 0; !found && colorIndex < AbstractChart.Colors.GetLength(0); colorIndex++)
            {
                var red = AbstractChart.Colors[colorIndex, 0];
                var green = AbstractChart.Colors[colorIndex, 1];
                var blue = AbstractChart.Colors[colorIndex, 2];

                if (fromColor.Red == red && fromColor.Green == green && fromColor.Blue == blue)
                {
                    AbstractChart.Colors[colorIndex, 0] = toColor.R;
                    AbstractChart.Colors[colorIndex, 1] = toColor.G;
                    AbstractChart.Colors[colorIndex, 2] = toColor.B;
                    found = true;
                }
            }            
        }*/

        public void ChangeSelectionColor()
        {
            Color3 initialColor = _currentSelection.CurrentColor;
            colorDialog1.Color = Color.FromArgb(initialColor.Red, initialColor.Green, initialColor.Blue);
            DialogResult result = colorDialog1.ShowDialog(this);
            if (result == DialogResult.OK)
            {                                
                Color selectedColor = colorDialog1.Color;
                //ChangeColor(_currentSelection.CurrentColor, selectedColor);
                _currentSelection.ChangeColor(selectedColor.R, selectedColor.G, selectedColor.B);
                canvas.Invalidate();
                _chart.ColorsSpecified = true;
                saveToolStripMenuItem.Enabled = true;
            }
        }

        public void ChangeFillColor()
        {
            Color3 initialColor = _currentSelection.CurrentFillColor;
            colorDialog1.Color = Color.FromArgb(initialColor.Red, initialColor.Green, initialColor.Blue);
            DialogResult result = colorDialog1.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                Color selectedColor = colorDialog1.Color;
                _currentSelection.ChangeFillColor(selectedColor.R, selectedColor.G, selectedColor.B);
                _chart.ColorsSpecified = true;
                saveToolStripMenuItem.Enabled = true;
                canvas.Invalidate();
            }
        }

        private void simpleOpenGlControl1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            simpleOpenGlControl1_MouseClick(sender, e);

            if (_currentSelection != null)
            {
                switch (_currentSelection.DefaultAction)
                {
                    case Action.ChangeFillColor:
                        ChangeFillColor();
                        break;

                    case Action.ChangeSelectionColor:
                        ChangeSelectionColor();
                        break;

                    default:
                        throw new Exception("Unhandled case " + _currentSelection.DefaultAction + " in MouseDoubleClick");
                }
            }
        }

        private void canvas_Resize(object sender, EventArgs e)
        {
            canvas.Invalidate();
        }

        private void colorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeSelectionColor();
        }

        private void fillColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeFillColor();
        }

        private void canvas_Load(object sender, EventArgs e)
        {
            DrawingTools.EnableDefaultAntiAliasing();            
            _fontID = FontGenerator.LoadTexture("consolas");

            if (_fileToOpenOnLoad != null)
            {
                LoadFile(_fileToOpenOnLoad);
            }
            else
            {
                ShowOpenFileDialog();                
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //  If the colors haven't been changed, don't do anything
            if (!_chart.ColorsSpecified) return;

            using (var writer = new StreamWriter(_chartFileName))
            {
                writer.WriteLine(_chart.ChartTypeSpecifier);
                writer.WriteLine(_chart.Title);
                if (_chart.ColorsSpecified)
                {
                    writer.WriteLine(AbstractChart.ColorString);
                    for (int i = 0; i < _chart.Colors.GetLength(0); i++)
                    {
                        //  See https://msdn.microsoft.com/en-us/library/dn961160.aspx for details on string interpolation in C#
                        writer.WriteLine($"{_chart.Colors[i, 0]} {_chart.Colors[i, 1]} {_chart.Colors[i, 2]}");
                    }
                }
                _chart.SaveFile(writer);
            }                                    
        }
        private bool inRect(Point2D leftTop, Point2D bottomRight, Point2D p)
        {
            double minX = leftTop.X;
            double maxX = bottomRight.X;
            double minY = bottomRight.Y;
            double maxY = leftTop.Y;
            if (p.X >= minX && p.X <= maxX && p.Y >= minY && p.Y <= maxY)
            {
                return true;
            }

            return false;
        }
        

        public void setSave() {
            saveToolStripMenuItem.Enabled = true;
        }
    }
}