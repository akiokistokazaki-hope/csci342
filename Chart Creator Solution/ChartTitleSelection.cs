﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chart_Creator
{
    class ChartTitleSelection : Selection
    {
        public bool CanChangeColor
        {
            get { return true; }
        }

        public bool CanChangeFillColor
        {
            get { return false; }
        }

        private Color3 color;

        public ChartTitleSelection(Color3 color)
        {
            this.color = color;
        }

        public void ChangeColor(byte r, byte g, byte b)
        {
            color.Red = r;
            color.Green = g;
            color.Blue = b;
        }

        public void ChangeFillColor(byte r, byte g, byte b)
        {
            throw new NotImplementedException();
        }

        public Color3 CurrentColor
        {
            get { return color; }
        }

        public Color3 CurrentFillColor
        {
            get { throw new NotImplementedException(); }
        }

        public Action DefaultAction
        {
            get { return Action.ChangeSelectionColor; }
        }
    }
}
