﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using ArcDrawer;
using csci342;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using ChartCreatorSolution;
using csci342.Text;

namespace Chart_Creator
{
    class ColumnChart: AbstractChart
    {
        private List<string> category = new List<string>();
        private List<string> series = new List<string>();
        public static Dictionary<string, Color3> colors = new Dictionary<string, Color3>();
        protected String selectedItem;
        private GLControl c;

        public const char TypeSpecifier = 'c';
        public override char ChartTypeSpecifier => TypeSpecifier;

        private Dictionary<string, Dictionary<string,  double>> seriesMap = new Dictionary<string, Dictionary<string, double>>();
        //private Dictionary<string, double> categoryValue;
        private double Maximum = Double.MinValue;

        public ColumnChart()
        {
        }


        public override void ReadFile(string firstLine, StreamReader reader)
        {
            List<string> eries = new List<string>();
        colors = new Dictionary<string, Color3>();
            seriesMap = new Dictionary<string, Dictionary<string, double>>();
        Maximum = Double.MinValue;

        var line = firstLine;
            while (!string.IsNullOrEmpty(line)) // !reader.EndOfStream || line != null &&!line.Equals("")
            {
                category.Add(line);
                line = reader.ReadLine();
            }
            
            while (!reader.EndOfStream)
            {
                line = reader.ReadLine();
                String[] tokens = line.Split(',');
                if (tokens.Length != 2)
                {
                    throw new FormatException("Data series description must be in format DS,Name");
                }
                if (tokens[0] != "DS")
                {
                    throw new FormatException($"First portion of data series specification must be DS (was {tokens[0]})");
                }
                String dataseriesName = tokens[1];
                series.Add(dataseriesName);
                colors.Add(dataseriesName, new Color3(Colors[series.Count-1, 0], Colors[series.Count - 1, 1], Colors[series.Count - 1, 2]));
                seriesMap.Add(dataseriesName, new Dictionary<string,  double>());
                foreach (string s in category)
                {
                    String doubleAsString = reader.ReadLine().Trim();
                    if ((doubleAsString[0] > 57 && doubleAsString[0] < 48))
                    {
                        throw new FormatException($"Needs to have same nuber of categories for all");
                    }
                    double d = Convert.ToDouble(doubleAsString);
                    Check(d);
                    seriesMap[dataseriesName].Add(s, d);
                }
            }

        }
        public override ICollection<string> LegendEntries
        {
            get
            {
                return series;
            }
        }

        public override CoordinateSystem World
        {
            get
            {
                return new CoordinateSystem(-1.2, 1.2, -1.2, 1.2);
            }
        }

        public override Color3 GetLegendColor(string item)
        {
            return colors[item];
        }

        public static Color3 GetColor(string item)
        {
            return colors[item];
        }

        public static void SetColor(string s,byte r, byte g, byte b)
        {
            colors[s] = new Color3(r, g, b);
        }

        public override void SaveFile(StreamWriter writer)
        {
            foreach (string cate in category)
            {
                writer.WriteLine(cate);
            }
            writer.WriteLine("");
            foreach (string name in series) {
                writer.WriteLine("DS,"+name);
                foreach (string cate in category) {
                    writer.WriteLine(seriesMap[name][cate]);
                }
            }
        }

        public void Check(double d)
        {
            Maximum = Math.Max(Maximum, d);
        }

        public override void Draw(GLControl c)
        {
            this.c = c;

            int[] viewport = new int[4];
            GL.GetInteger(GetPName.Viewport, viewport);

            GL.MatrixMode(MatrixMode.Projection);
            GL.PushMatrix();
            GL.LoadIdentity();

            GL.Ortho(0, category.Count*series.Count+ category.Count+1, 0- Maximum * 0.1, Maximum+ Maximum*0.1, 0,1);
            double currentX = 1;
            double size = availableWidth;
            double one = availableWidth / (category.Count * series.Count + category.Count + 1);
            double currentTextX =one;
            foreach (var s in category)
            {
                GL.PushMatrix();
                GL.LoadIdentity();
                DrawingTools.EnableDefaultAntiAliasing();
                GL.Color3(Color.Black);
                GL.Ortho(0, availableWidth, 0, 450, 0,1);
                TextDrawer.DrawString(s, currentTextX, 15, FontGenerator.LoadTexture("consolas"));
                currentTextX += one* (series.Count+1);
                GL.PopMatrix();
                foreach (var name in series) {
                    GL.Color3(colors[name].Red, colors[name].Green, colors[name].Blue);
                    GL.Begin(PrimitiveType.Polygon);
                    {
                        GL.Vertex2(currentX, 0);
                        GL.Vertex2(currentX, seriesMap[name][s]);
                        GL.Vertex2(currentX + 1, seriesMap[name][s]);
                        GL.Vertex2(currentX+1, 0);
                    }
                    GL.End();
                    GL.Color3(Color.Black);
                    GL.Begin(PrimitiveType.LineLoop);
                    {
                        GL.Vertex2(currentX, 0);
                        GL.Vertex2(currentX, seriesMap[name][s]);
                        GL.Vertex2(currentX + 1, seriesMap[name][s]);
                        GL.Vertex2(currentX + 1, 0);
                    }
                    GL.End();
                    if (name.Equals(selectedItem))
                    {
                        GL.Color3(Color.White);
                        GL.Begin(PrimitiveType.LineLoop);
                        {
                            GL.Vertex2(currentX, 0);
                            GL.Vertex2(currentX, seriesMap[name][s]);
                            GL.Vertex2(currentX + 1, seriesMap[name][s]);
                            GL.Vertex2(currentX + 1, 0);
                        }
                        GL.End();
                    }

                    currentX++;
                }
                currentX ++;
            }
            GL.PopMatrix();
        }

        public override Selection GetSelection(double wx, double wy)
        {
            double worldWidth = World.Right - World.Left;
            double worldHeight = World.Top - World.Bottom;
            double worldLeft = World.Left;
            double worldBottom = World.Bottom;

            double wx1 = ChartCreator.sx;
            double wy1 = ChartCreator.sy;

            double size = availableWidth;
            double one = availableWidth / (category.Count * series.Count + category.Count + 1);
            double currentX = one;
            int count = 2;

            foreach (string name in series) {
                for (int i = 0; i < category.Count; i++)
                {
                    if (currentX <= wx1 && wx1 <= currentX + one)
                    {
                        selectedItem = name;
                        return new DataSelection(selectedItem);
                    }
                    currentX += one * (series.Count+1);
                }
                currentX = one*count;
                count++;
            }

            return null;
        }

        public override Selection SetLegendSelection(string changeLegend)
        {
            selectedItem = changeLegend;
            return new DataSelection(selectedItem);
        }

        public override void ClearSelection()
        {
            selectedItem = null;
        }

        public override void DrawLegendMarker(string seriesName, double x, double y)
        {
            GL.Rect((int)x, (int)y, (int)x + ChartCreator.BoxWidth, (int)y + ChartCreator.BoxHeight);
        }

        class DataSelection : Selection
        {
            private string selected;

            public DataSelection(string selected)
            {
                this.selected = selected;
            }

            public bool CanChangeColor
            {
                get { return true; }
            }

            public bool CanChangeFillColor
            {
                get { return true; }
            }

            public void ChangeColor(byte r, byte g, byte b)
            {
                SetColor(selected, r, g, b);
            }

            public void ChangeFillColor(byte r, byte g, byte b)
            {
                SetColor(selected,r,g,b);
            }

            public Color3 CurrentColor
            {
                get
                {
                    return GetColor(selected);
                }
            }

            public Color3 CurrentFillColor
            {
                get { throw new NotImplementedException(); }
            }

            public Action DefaultAction
            {
                get { return Action.ChangeSelectionColor; }
            }
        }
    }

}
