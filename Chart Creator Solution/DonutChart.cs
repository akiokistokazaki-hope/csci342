﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using ArcDrawer;
using csci342;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using ChartCreatorSolution;

namespace Chart_Creator
{
    class DonutChart : PieChart
    {

        public const char TypeSpecifier = 'd';
        public override char ChartTypeSpecifier => TypeSpecifier;

        public DonutChart() {
            moveHandler = new MoveHandler();
            items = new Dictionary<string, SliceInfo>();
            selectedItem = null;
        }

        public override void Draw(GLControl c)
        {
            this.c = c;
            foreach (String item in items.Keys)
            {
                if (!item.Equals(explodeSlice))
                {
                    SliceInfo slice = items[item];

                    GL.Color3(slice.Color.Red, slice.Color.Green, slice.Color.Blue);

                    GL.Begin(PrimitiveType.Polygon);
                    {
                        GL.Vertex2(0.0, 0.0);
                        Arc.DrawArc(0, 0, 1.0, slice.BeginAngle, slice.Sweep);
                        GL.End();
                    }

                    Utilities.SetForegroundBlack();
                    GL.Begin(PrimitiveType.LineLoop);
                    {
                        GL.Vertex2(0.0, 0.0);
                        Arc.DrawArc(0, 0, 1.0, slice.BeginAngle, slice.Sweep);
                    }
                    GL.End();

                    GL.Begin(PrimitiveType.LineLoop);
                    {
                        GL.Vertex2(0.0, 0.0);
                        Arc.DrawArc(0, 0, 0.75, slice.BeginAngle, slice.Sweep);
                    }
                    GL.End();

                    GL.Color3(Color.White);
                    GL.Begin(PrimitiveType.Polygon);
                    {
                        GL.Vertex2(0.0, 0.0);
                        Arc.DrawArc(0, 0, 0.75, 0, 360);
                        GL.End();
                    }
                }
                if (exploded)
                {
                    GL.Color3(items[explodeSlice].Color.Red, items[explodeSlice].Color.Green, items[explodeSlice].Color.Blue);
                    Point2D dif = moveHandler.dif;
                    Vector2 difV = new Vector2((float)dif.X, (float)dif.Y);
                    dis = difV.X + dif.Y;
                    if (moveHandler.left)
                    {
                        dis = dis * -1;
                    }
                    double midAngle = (items[explodeSlice].EndAngle - items[explodeSlice].BeginAngle) / 2 + items[explodeSlice].BeginAngle;
                    double midAngleRadius = Math.PI / 180 * midAngle;
                    Vector2 midV = new Vector2((float)Math.Cos(midAngleRadius), (float)Math.Sin(midAngleRadius));
                    Vector2 oneV = new Vector2(midV.X / midV.Length, midV.Y / midV.Length);
                    if (dis > 0.2)
                    {
                        dis = 0.2;
                    }
                    if (dis < 0)
                    {
                        dis = 0;
                    }
                    double n = items[explodeSlice].EndAngle;//360;//4960;
                    double rate;
                    List<Point2D> outPoints = new List<Point2D>();
                    List<Point2D> inPoints = new List<Point2D>();
                    double x;
                    double y;
                    for (double i = items[explodeSlice].BeginAngle; i <= n; i = i + 3)
                    {
                        rate = i / 360;
                        x = 1 * Math.Cos(2.0 * Math.PI * rate);
                        y = 1 * Math.Sin(2.0 * Math.PI * rate);
                        outPoints.Add(new Point2D(x, y));
                    }
                    x = 1 * Math.Cos(2.0 * Math.PI * n / 360);
                    y = 1 * Math.Sin(2.0 * Math.PI * n / 360);
                    outPoints.Add(new Point2D(x, y));

                    for (double i = items[explodeSlice].BeginAngle; i <= n; i = i + 3)
                    {
                        rate = i / 360;
                        x = 0.75 * Math.Cos(2.0 * Math.PI * rate);
                        y = 0.75 * Math.Sin(2.0 * Math.PI * rate);
                        inPoints.Add(new Point2D(x, y));
                    }
                    x = 0.75 * Math.Cos(2.0 * Math.PI * n / 360);
                    y = 0.75 * Math.Sin(2.0 * Math.PI * n / 360);
                    inPoints.Add(new Point2D(x, y));
                    x = 1 * Math.Cos(2.0 * Math.PI * n / 360);
                    y = 1 * Math.Sin(2.0 * Math.PI * n / 360);
                    inPoints.Add(new Point2D(x, y));

                    GL.Begin(PrimitiveType.Polygon);
                    {
                        foreach (Point2D point in outPoints)
                        {
                        GL.Vertex2(point.X+ oneV.X * dis, point.Y+ oneV.Y * dis);
                        }
                        foreach (Point2D point in inPoints)
                        {
                            GL.Vertex2(point.X+ oneV.X * dis, point.Y+ oneV.Y * dis);
                        }
                    }
                    GL.End();
                    if (selectedItem != null && _currentSelection.Equals(items[explodeSlice]))
                    {
                        double beginAngleDegrees = items[selectedItem].BeginAngle;
                        double endAngleDegrees = items[selectedItem].EndAngle;
                        double beginAngleRadians = Math.PI / 180 * beginAngleDegrees;
                        double endAngleRadians = Math.PI / 180 * endAngleDegrees;

                        ChartCreator.DrawSelectionHandle(Math.Cos(beginAngleRadians) + oneV.X * dis, Math.Sin(beginAngleRadians) + oneV.Y * dis);
                        ChartCreator.DrawSelectionHandle(Math.Cos(beginAngleRadians) * .75 + oneV.X * dis, Math.Sin(beginAngleRadians) * .75 + oneV.Y * dis);
                        ChartCreator.DrawSelectionHandle(Math.Cos(endAngleRadians) + oneV.X * dis, Math.Sin(endAngleRadians) + oneV.Y * dis);
                        ChartCreator.DrawSelectionHandle(Math.Cos(endAngleRadians) * .75 + oneV.X * dis, Math.Sin(endAngleRadians) * .75 + oneV.Y * dis);
                    }
                }

                if (selectedItem != null && !explode)
                {
                    if (!items.ContainsKey(explodeSlice) || !_currentSelection.Equals(items[explodeSlice]))
                    {
                        double beginAngleDegrees = items[selectedItem].BeginAngle;
                        double endAngleDegrees = items[selectedItem].EndAngle;
                        double beginAngleRadians = Math.PI / 180 * beginAngleDegrees;
                        double endAngleRadians = Math.PI / 180 * endAngleDegrees;

                        ChartCreator.DrawSelectionHandle(Math.Cos(beginAngleRadians), Math.Sin(beginAngleRadians));
                        ChartCreator.DrawSelectionHandle(Math.Cos(beginAngleRadians) * .75, Math.Sin(beginAngleRadians) * .75);
                        ChartCreator.DrawSelectionHandle(Math.Cos(endAngleRadians), Math.Sin(endAngleRadians));
                        ChartCreator.DrawSelectionHandle(Math.Cos(endAngleRadians) * .75, Math.Sin(endAngleRadians) * .75);
                    }
                }
            }
        }

        public override Selection GetSelection(double wx, double wy)
        {
            double r = wx * wx + wy * wy;
            if (inCircle(0.75, new Point2D(0,0), new Point2D(wx,wy))) {
                // if the click is in the inner circle return null
                return null;
            }
            if (r < 1)
            {
                r = Math.Sqrt(r);
                double theta = Math.Acos(wx / r) * (180 / Math.PI);
                //  Acos returns angles between 0 and pi; if the y coordinate is negative, we then need to start at 180
                if (wy < 0)
                {
                    theta = 180 + (180 - theta);
                }

                foreach (var key in items.Keys)
                {
                    SliceInfo slice = items[key];
                    if (slice.BeginAngle <= theta && slice.EndAngle >= theta)
                    {
                        selectedItem = key;
                    }
                }
                if (items[selectedItem].Equals(_currentSelection) && !explode)
                {
                    explode = true;
                    exploded = true;
                    explodeSlice = selectedItem;
                    moveHandler = new MoveHandler();
                    save = true;
                    moveHandler.Activate(c, wx, wy);
                    return null;
                }
                else
                {
                    explode = false;
                    _currentSelection = items[selectedItem];
                    return new PieSliceSelection(items[selectedItem]);
                }
            }
            else
            {
                selectedItem = null;
                return null;
            }
        }

        private bool inCircle(double r, Point2D center, Point2D p)
        {
                if ((center.X - p.X) * (center.X - p.X) + (center.Y - p.Y) * (center.Y - p.Y) <= r * r)
                {
                    return true;
                }
            return false;
        }
    }
}
