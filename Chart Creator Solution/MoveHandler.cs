﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using OpenTK;
using Chart_Creator;

namespace ChartCreatorSolution
{
    public class MoveHandler
    {
        private Point2D currentP;
        private double mouseX;
        private double mouseY;
        public Point2D dif { get; set; }
        public bool left{ get; set; }
        //private Point2D currentP;
        //private EditorForm editor;
        Vector2 midV;
        double mul = 0.0;
        
        private GLControl glControl;

        public Boolean IsComplete { get; private set; }

        public MoveHandler()
        {
            IsComplete = false;
            left = false;
        }
        

        public void Activate(GLControl control, double mouseX, double mouseY)
        {
            this.mouseX = mouseX;
            this.mouseY = mouseY;
            currentP = new Point2D(mouseX,mouseY);
            if (mouseX<0) {
                left = true;
                Console.WriteLine(mouseX);
            }
            glControl = control;
            glControl.MouseMove += MoveAndUpdate;
            glControl.MouseClick += MoveEnd;
        }

        private void MoveAndUpdate(object sender, MouseEventArgs e)
        {
            Point2D p = ChartCreator.getWorldPoint(e.X, e.Y);
            double wx = p.X;// (e.X - ChartCreator._chartViewport.Location.X) / ChartCreator.divX;
            double wy = p.Y;//(glControl.Height - e.Y - ChartCreator._chartViewport.Location.Y) / ChartCreator.divY;

            Point2D moveP = new Point2D(wx, wy);
            //Console.WriteLine(wx); Console.WriteLine(wy);
            //Console.WriteLine(currentP.X); Console.WriteLine(currentP.Y);
            dif = new Point2D(moveP.X - currentP.X, moveP.Y - currentP.Y);
            
            //currentP = moveP;
            glControl.Invalidate();
        }

        private void MoveEnd(object sender, MouseEventArgs e)
        {
            MoveAndUpdate(sender, e);
            glControl.MouseMove -= MoveAndUpdate;
            glControl.MouseClick -= MoveEnd;
            glControl.Invalidate();
        }
        
}
}
