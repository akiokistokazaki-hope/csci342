﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using ArcDrawer;
using csci342;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using ChartCreatorSolution;

namespace Chart_Creator
{
    public class PieChart : AbstractChart
    {
        
        protected GLControl c;
        protected Dictionary<string, SliceInfo> items;
        protected string explodeSlice = "";
        protected bool explode = false;
        protected bool exploded = false;
        protected SliceInfo _currentSelection;
        protected String selectedItem;
        protected Rectangle chartViewport = new Rectangle();
        protected MoveHandler moveHandler = new MoveHandler();
        protected double dis;
        public const char TypeSpecifier = 'p';

        public override char ChartTypeSpecifier => TypeSpecifier;

        public PieChart () : base () {
            items = new Dictionary<string, SliceInfo>();
            selectedItem = null;
        }

        public class SliceInfo
        {
            public PieChart Chart;

            public int ColorIndex;
            public int Count;

            public Color3 Color
            {
                get
                {
                    var r = Chart.Colors[ColorIndex, 0];
                    var g = Chart.Colors[ColorIndex, 1];
                    var b = Chart.Colors[ColorIndex, 2];
                    return new Color3(r, g, b);
                }    
            }
            public double BeginAngle;
            public double EndAngle;

            public double Sweep => EndAngle - BeginAngle;

            public SliceInfo(PieChart chart)
            {
                Chart = chart;
                Count = 0;
            }
        }

        public override void ReadFile(string firstLine, StreamReader reader)
        {
            int totalItems = 0;
            var line = firstLine;
            if (line.Contains("explode")) {
                exploded = true;
                var rest = line.Split(':')[1].Split(',');
                explodeSlice = rest[0];
                moveHandler = new MoveHandler();
                moveHandler.dif = new Point2D(double.Parse(rest[1]), double.Parse(rest[2]));
                line = reader.ReadLine();
            }
            while (line != null)
            {                
                SliceInfo sliceInfo;
                if (items.ContainsKey(line))
                {
                    sliceInfo = items[line];
                }
                else
                {
                    sliceInfo = new SliceInfo(this);
                    items[line] = sliceInfo;
                    
                    if (items.Keys.Count> Colors.GetLength(0))
                    {
                        throw new FormatException($"Too many values specified for pie chart; limit is {Colors.GetLength(0)}");
                    }
                }
                
                sliceInfo.Count++;
                totalItems++;

                line = reader.ReadLine();
            }

            double beginAngle = 0;
            int i = 0;

            foreach (var key in items.Keys)
            {
                SliceInfo slice = items[key];
                double percentage = 1.0 * slice.Count / totalItems;
                slice.BeginAngle = beginAngle;
                slice.EndAngle = beginAngle + percentage * 360;
                slice.ColorIndex = i;
                beginAngle = slice.EndAngle;
                i++;
            }
        }

        public override void SaveFile(StreamWriter writer)
        {
            if (exploded)
            {
                writer.WriteLine("explode:"+explodeSlice + "," + moveHandler.dif.X + "," + moveHandler.dif.Y);
            }
            foreach (var sliceInfo in items)
            {
                var text = sliceInfo.Key;
                var data = sliceInfo.Value;
                for (int i = 0; i < data.Count; i++)
                {
                    writer.WriteLine(text);
                }
            }
        }

        public override void Draw(GLControl c)
        {
            this.c = c;
            foreach (String item in items.Keys)
            {
                SliceInfo slice = items[item];
                if (!item.Equals(explodeSlice)) {
                GL.Color3(slice.Color.Red, slice.Color.Green, slice.Color.Blue);

                GL.Begin(PrimitiveType.Polygon);
                {
                    GL.Vertex2(0.0, 0.0);
                    Arc.DrawArc(0, 0, 1.0, slice.BeginAngle, slice.Sweep);
                    GL.End();
                }

                Utilities.SetForegroundBlack();
                GL.Begin(PrimitiveType.LineLoop);
                {
                    GL.Vertex2(0.0, 0.0);
                    Arc.DrawArc(0, 0, 1.0, slice.BeginAngle, slice.Sweep);
                }
                GL.End();
                }
            }

            if (exploded) {
                GL.Color3(items[explodeSlice].Color.Red, items[explodeSlice].Color.Green, items[explodeSlice].Color.Blue);
                Point2D dif = moveHandler.dif;
                Vector2 difV = new Vector2((float)dif.X, (float)dif.Y);
                dis = difV.X + dif.Y;
                if (moveHandler.left)
                {
                    dis = dis * -1;
                }
                double midAngle = (items[explodeSlice].EndAngle - items[explodeSlice].BeginAngle) / 2 + items[explodeSlice].BeginAngle;
                double midAngleRadius = Math.PI / 180 * midAngle;
                Vector2 midV = new Vector2((float)Math.Cos(midAngleRadius), (float)Math.Sin(midAngleRadius));
                Vector2 oneV = new Vector2(midV.X / midV.Length, midV.Y / midV.Length);
                if (dis > 0.2) {
                    dis = 0.2;
                }
                if (dis < 0)
                {
                    dis = 0;
                }
                GL.Begin(PrimitiveType.Polygon);
                {
                    GL.Vertex2(oneV.X * dis, oneV.Y * dis);
                    Arc.DrawArc(oneV.X * dis, oneV.Y * dis, 1.0, items[explodeSlice].BeginAngle, items[explodeSlice].Sweep);
                    GL.End();
                }

                Utilities.SetForegroundBlack();
                GL.Begin(PrimitiveType.LineLoop);
                {
                    GL.Vertex2(oneV.X * dis, oneV.Y * dis);
                    Arc.DrawArc(oneV.X * dis, oneV.Y * dis, 1.0, items[explodeSlice].BeginAngle, items[explodeSlice].Sweep);
                }
                GL.End();
                if (selectedItem != null) {
                    if (explode || _currentSelection.Equals(items[explodeSlice]))
                    {
                        double beginAngleDegrees = items[selectedItem].BeginAngle;
                        double endAngleDegrees = items[selectedItem].EndAngle;
                        double beginAngleRadians = Math.PI / 180 * beginAngleDegrees;
                        double endAngleRadians = Math.PI / 180 * endAngleDegrees;
                        ChartCreator.DrawSelectionHandle(oneV.X * dis, oneV.Y * dis);
                        ChartCreator.DrawSelectionHandle(Math.Cos(beginAngleRadians) + oneV.X * dis, Math.Sin(beginAngleRadians) + oneV.Y * dis);
                        ChartCreator.DrawSelectionHandle(Math.Cos(endAngleRadians) + oneV.X * dis, Math.Sin(endAngleRadians) + oneV.Y * dis);
                        //ChartCreator.DrawSelectionHandle(Math.Cos(beginAngleRadians)+ (oneV.X * dis), Math.Sin(beginAngleRadians + (oneV.Y * dis)));
                        //ChartCreator.DrawSelectionHandle(Math.Cos(endAngleRadians)+ (oneV.X * dis), Math.Sin(endAngleRadians + (oneV.Y * dis)));

                    }
                }
            }

            if (selectedItem != null && !explode )
            {
                if (!items.ContainsKey(explodeSlice) || !_currentSelection.Equals(items[explodeSlice])) {
                    double beginAngleDegrees = items[selectedItem].BeginAngle;
                    double endAngleDegrees = items[selectedItem].EndAngle;
                    double beginAngleRadians = Math.PI / 180 * beginAngleDegrees;
                    double endAngleRadians = Math.PI / 180 * endAngleDegrees;

                    ChartCreator.DrawSelectionHandle(0, 0);
                    ChartCreator.DrawSelectionHandle(Math.Cos(beginAngleRadians), Math.Sin(beginAngleRadians));
                    ChartCreator.DrawSelectionHandle(Math.Cos(endAngleRadians), Math.Sin(endAngleRadians));
                }
            }
        }

        public override ICollection<string> LegendEntries
        {
            get
            {
                return items.Keys;
            }
        }

        public override Color3 GetLegendColor(string item)
        {
            return items[item].Color;
        }

        public override void ClearSelection()
        {
            selectedItem = null;
        }

        public override Selection GetSelection(double wx, double wy)
        {

            double r = wx * wx + wy * wy;
            if (r < 1)
            {
                r = Math.Sqrt(r);
                double theta = Math.Acos(wx / r) * (180 / Math.PI);
                //  Acos returns angles between 0 and pi; if the y coordinate is negative, we then need to start at 180
                if (wy < 0)
                {
                    theta = 180 + (180 - theta);
                }

                foreach (var key in items.Keys)
                {
                    SliceInfo slice = items[key];
                    if (slice.BeginAngle <= theta && slice.EndAngle >= theta)
                    {
                        selectedItem = key;
                    }
                }
                if (items[selectedItem].Equals(_currentSelection) && !explode)
                {
                    explode = true;
                    exploded = true;
                    explodeSlice = selectedItem;
                    moveHandler = new MoveHandler();
                    save = true;
                    moveHandler.Activate(c, wx, wy);
                    return null;
                }
                else
                {
                    explode = false;
                    _currentSelection = items[selectedItem];
                    return new PieSliceSelection(items[selectedItem]);
                }
            }
            else
            {
                selectedItem = null;
                return null;
            }
        }

        public override CoordinateSystem World {
            get
            {
                return new CoordinateSystem(-1.2, 1.2, -1.2, 1.2);                
            }
        }

        public override Rectangle GetViewport(double widthAvailable, double heightAvailable)
        {
            chartViewport.Location = new Point(0, 0);

            if (widthAvailable > heightAvailable)
            {
                chartViewport.Height = (int)heightAvailable;
                chartViewport.Width = (int)heightAvailable;
            }
            else
            {            
                chartViewport.Width = (int)widthAvailable;
                chartViewport.Height = (int)widthAvailable;
            }
            return chartViewport;
        }

        public override void DrawLegendMarker(string seriesName, double x, double y)
        {
            GL.Rect((int) x, (int) y, (int) x + ChartCreator.BoxWidth, (int) y + ChartCreator.BoxHeight);
        }

        public override Selection SetLegendSelection(string changeLegend)
        {
            selectedItem = changeLegend;
            explode = false;
            _currentSelection = items[changeLegend];
            return new PieSliceSelection(items[changeLegend]);
        }
    }    
}
