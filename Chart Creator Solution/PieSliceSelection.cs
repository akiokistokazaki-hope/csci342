﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chart_Creator
{
    public class PieSliceSelection : Selection
    {
        private PieChart.SliceInfo slice;

        public Color3 CurrentColor
        {
            get
            {
                throw new NotImplementedException("PieSliceSelection does not implement CurrentColor");
            }
        }

        public Color3 CurrentFillColor
        {
            get
            {
                return slice.Color;
            }
        }

        public bool CanChangeColor
        {
            get
            {
                return false;
            }
        }

        public bool CanChangeFillColor
        {
            get
            {
                return true;
            }
        }

        public PieSliceSelection(PieChart.SliceInfo slice)
        {
            this.slice = slice;
        }

        public void ChangeColor(byte r, byte g, byte b)
        {
            throw new NotImplementedException("Pie charts do not support changing color, only fill color");
        }

        public void ChangeFillColor(byte r, byte g, byte b)
        {
            slice.Chart.Colors[slice.ColorIndex, 0] = r;
            slice.Chart.Colors[slice.ColorIndex, 1] = g;
            slice.Chart.Colors[slice.ColorIndex, 2] = b;
        }

        public Action DefaultAction
        {
            get { return Action.ChangeFillColor; }
        }
    }
}
