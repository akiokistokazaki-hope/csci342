using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Chart_Creator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (args.Length == 0)
            {
                Application.Run(new ChartCreator());
            }
            else if (args.Length == 1)
            {
                Application.Run(new ChartCreator(args[0]));
            }
            else
            {
                Console.WriteLine("Error:  only 1 command line argument should be present");
                Application.Exit();
            }
        }
    }
}