﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Text;
using ArcDrawer;
using csci342;
using csci342.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Boolean = System.Boolean;
using Vector3 = csci342.Vector3;

namespace Chart_Creator
{
    public class ScatterPlot : AbstractChart
    {
        enum Axis
        {
            Primary, Secondary, Category
        };

        public delegate void MarkerDrawer(double x, double y);

        class DataSeries
        {
            public ScatterPlot Chart;

            public int ColorIndex { get; set; }  

            public string Name { get; set; }

            public Color3 Color
            {
                get
                {
                    var red = Chart.Colors[ColorIndex, 0];
                    var green = Chart.Colors[ColorIndex, 1];
                    var blue = Chart.Colors[ColorIndex, 2];
                    return new Color3(red, green, blue);
                }
            }


            public Axis Axis { get; set; }            
            public SortedList<double, double> points;
            public Marker Marker { get; set; }

            public IList<double> X
            {
                get
                {
                    return points.Keys;
                }
            }

            public double this[double x] {
                get {
                    return points[x];
                }
            }

            public DataSeries(ScatterPlot chart, string name, int colorIndex, Axis axis)
            {
                Chart = chart;                
                Name = name;
                ColorIndex = colorIndex;
                Axis = axis;

                points = new SortedList<double, double>();
            }

            public void AddPoint(double x, double y)
            {
                points.Add(x, y);
            }

        }

        class Interval
        {
            public Interval()
            {
                Minimum = Double.MaxValue;
                Maximum = Double.MinValue;
            }

            public double Minimum { get; set; }
            public double Maximum { get; set; }

            public double MinimumX { get; set; }
            public double MaximumX { get; set; }
            public void CheckX(double d)
            {
                MinimumX = Math.Min(MinimumX, d);
                MaximumX = Math.Max(MaximumX, d);
            }

            public double Range
            {
                get
                {
                    return Maximum - Minimum;
                }
            }

            public void Check(double d)
            {
                Minimum = Math.Min(Minimum, d);
                Maximum = Math.Max(Maximum, d);
            }
        }

        class DataSeriesSelection : Selection
        {
            private DataSeries series;

            public DataSeriesSelection(DataSeries selectedSeries)
            {
                series = selectedSeries;
            }

            public bool CanChangeColor
            {
                get { return true; }
            }

            public bool CanChangeFillColor
            {
                get { return false; }
            }

            public void ChangeColor(byte r, byte g, byte b)
            {
                series.Chart.Colors[series.ColorIndex, 0] = r;
                series.Chart.Colors[series.ColorIndex, 1] = g;
                series.Chart.Colors[series.ColorIndex, 2] = b;
            }

            public void ChangeFillColor(byte r, byte g, byte b)
            {
                throw new NotImplementedException();
            }

            public Color3 CurrentColor
            {
                get {
                    return series.Color;
                }
            }

            public Color3 CurrentFillColor
            {
                get { throw new NotImplementedException(); }
            }

            public Action DefaultAction
            {
                get { return Action.ChangeSelectionColor; }
            }
        }

        static ScatterPlot()
        {
            GL.Enable(EnableCap.LineStipple);
        }

        private Dictionary<String, DataSeries> dataSeries;
        private Dictionary<Axis, Interval> axisBounds;

        private const int NUMBER_OF_TICKS = 10;
        private const double tickLengthPixels = 10;
        private static int MARKER_SIZE_PIXELS = 6;
        

        private DataSeries selectedSeries;

        public const char TypeSpecifier = 'l';

        public override char ChartTypeSpecifier => TypeSpecifier;

        #region Markers
        class Marker
        {
            public Marker(MarkerDrawer drawer, PrimitiveType defaultPrimitive = PrimitiveType.Polygon, PrimitiveType selectedPrimitive = PrimitiveType.LineLoop)
            {
                Drawer = drawer;
                DefaultPrimitive = defaultPrimitive;
                SelectedPrimitive = selectedPrimitive;
            }

            public MarkerDrawer Drawer { get; set; }
            public PrimitiveType DefaultPrimitive { get; set; }
            public PrimitiveType SelectedPrimitive { get; set; }
        }

        public static void DrawCircle (double x, double y)
        {            
            Arc.DrawArc(x, y, MARKER_SIZE_PIXELS, 0, 360);
        }

        public static void DrawRectangle(double x, double y)
        {
            GL.Vertex2(x-MARKER_SIZE_PIXELS, y-MARKER_SIZE_PIXELS);
            GL.Vertex2(x+MARKER_SIZE_PIXELS, y-MARKER_SIZE_PIXELS);
            GL.Vertex2(x+MARKER_SIZE_PIXELS, y+MARKER_SIZE_PIXELS);
            GL.Vertex2(x-MARKER_SIZE_PIXELS, y+MARKER_SIZE_PIXELS);   
        }

        public static void DrawDiamond(double x, double y)
        {
            GL.Vertex2(x, y + MARKER_SIZE_PIXELS);
            GL.Vertex2(x - MARKER_SIZE_PIXELS, y);
            GL.Vertex2(x, y - MARKER_SIZE_PIXELS);
            GL.Vertex2(x + MARKER_SIZE_PIXELS, y);
        }

        public static void drawPlus(double x, double y)
        {
            GL.Vertex2(x - MARKER_SIZE_PIXELS, y);
            GL.Vertex2(x + MARKER_SIZE_PIXELS, y);

            GL.Vertex2(x, y - MARKER_SIZE_PIXELS);
            GL.Vertex2(x, y + MARKER_SIZE_PIXELS);
        }

        public static void drawX(double x, double y)
        {
            GL.Vertex2(x - MARKER_SIZE_PIXELS, y + MARKER_SIZE_PIXELS);
            GL.Vertex2(x + MARKER_SIZE_PIXELS, y - MARKER_SIZE_PIXELS);

            GL.Vertex2(x + MARKER_SIZE_PIXELS, y + MARKER_SIZE_PIXELS);
            GL.Vertex2(x - MARKER_SIZE_PIXELS, y - MARKER_SIZE_PIXELS);
        }

        public static void drawTriangle(double x, double y)
        {
            double r = MARKER_SIZE_PIXELS;

            double oneTwentyDegreesInRadians = Utilities.DegreesToRadians(120);
            double ninetyDegreesInRadians = Math.PI / 2;
            GL.Vertex2(x + r * Math.Cos(ninetyDegreesInRadians), y + r * Math.Sin(ninetyDegreesInRadians));
            GL.Vertex2(x + r * Math.Cos(oneTwentyDegreesInRadians + ninetyDegreesInRadians), y + r * Math.Sin(oneTwentyDegreesInRadians + ninetyDegreesInRadians));
            GL.Vertex2(x + r * Math.Cos(2*oneTwentyDegreesInRadians + ninetyDegreesInRadians), y + r * Math.Sin(2*oneTwentyDegreesInRadians + ninetyDegreesInRadians));
        }

        Marker[] markers = 
        {
            new Marker(new MarkerDrawer(DrawCircle)),
            new Marker(new MarkerDrawer(DrawDiamond)),
            new Marker(new MarkerDrawer(drawTriangle)),
            new Marker(new MarkerDrawer(DrawRectangle)),
            new Marker(new MarkerDrawer(drawX), PrimitiveType.Lines, PrimitiveType.Lines),
            new Marker(new MarkerDrawer(drawPlus), PrimitiveType.Lines, PrimitiveType.Lines)
            
        };

        #endregion

        public ScatterPlot() : base()
        {
            dataSeries = new Dictionary<string, DataSeries>();
            axisBounds = new Dictionary<Axis, Interval>();
            axisBounds[Axis.Primary] = new Interval();
            axisBounds[Axis.Secondary] = new Interval();
            axisBounds[Axis.Category] = new Interval();
        }

        #region Open and Save file
        public override void ReadFile(string firstLine, System.IO.StreamReader reader)
        {
            char[] delimiters = {','};
            int dataSeriesIndex = 0;            
            var currentLine = firstLine;

            while (!reader.EndOfStream)
            {                                                        
                String[] tokens = currentLine.Split(delimiters);

                if (tokens.Length != 3)
                {
                    throw new FormatException("Data series description must be in format DS,Name,[p|s]");
                }

                if (tokens[0] != "DS")
                {
                    throw new FormatException($"First portion of data series specification must be DS (was {tokens[0]})");
                }
                
                    Axis axis = Axis.Primary;

                try
                {
                    axis = (Axis)Enum.Parse(typeof(Axis), tokens[2]);
                }
                catch (ArgumentException badAxis)
                {
                    throw new FormatException($"Invalid value for axis; must be either p (primary) or s (secondary); was {tokens[2]}");
                }
                
                String dataseriesName = tokens[1];                
                DataSeries series = new DataSeries(this, dataseriesName, dataSeriesIndex, axis);

                //  Re-use markers if more data series than markers are available
                series.Marker = markers[dataSeriesIndex % markers.Length];

                dataSeries.Add(dataseriesName, series);

                //  Read the points in the series
                bool done = false;
                while (!done)
                {
                    String pointAsString = reader.ReadLine().Trim();
                    if (pointAsString != "-1")
                    {
                        String[] components = pointAsString.Split(delimiters);
                        if (components.Length != 2)
                        {
                            throw new Exception("Point must contain both an X and a Y");
                        }
                        try
                        {
                            double x = Convert.ToDouble(components[0]);
                            double y = Convert.ToDouble(components[1]);
                            series.AddPoint(x, y);
                            axisBounds[Axis.Category].Check(x);
                            axisBounds[axis].Check(y);
                        }
                        catch (FormatException badNumber)
                        {
                            throw new Exception("Invalid number in " + pointAsString);
                        }
                    }
                    else
                    {
                        done = true;
                    }
                }
                currentLine = reader.ReadLine();
                dataSeriesIndex++;

                if (dataSeriesIndex > Colors.GetLength(0))
                {
                    throw new FormatException($"Too many data series specified; maximum is {Colors.GetLength(0)}");
                }
            }

            if (dataSeriesIndex == 0)
            {
                throw new FormatException("At least one data series must be specified for a line chart");
            }                                        
        }

        public override void SaveFile(StreamWriter writer)
        {
            foreach (var seriesName in dataSeries.Keys)
            {
                var series = dataSeries[seriesName];
                writer.WriteLine($"DS,{series.Name},{series.Axis}");
                foreach (var xValue in series.X)
                {
                    writer.WriteLine($"{xValue},{series[xValue]}");
                }
                writer.WriteLine("-1");
            }
        }
        #endregion

        #region Drawing the chart
        public override void Draw(GLControl c)
        {
            DrawChartAxes();
            int[] viewport = new int[4];
            GL.GetInteger(GetPName.Viewport, viewport);            

            foreach (var series in dataSeries)
            {
                var value = series.Value;
                Boolean isSelectedSeries = selectedSeries != null && selectedSeries.Name.Equals(value.Name);
                short stipplePattern = (short) ( isSelectedSeries ? 0xF0F0 : 0xFFFF);
                GL.LineStipple(1, stipplePattern);
                GL.Color3(value.Color.Red, value.Color.Green, value.Color.Blue);
                GL.MatrixMode(MatrixMode.Projection);
                GL.PushMatrix();
                GL.LoadIdentity();
                double ofsetX = (axisBounds[Axis.Category].Maximum - axisBounds[Axis.Category].Minimum)/20;
                double ofsetY = 0.0;
                if (value.Axis.Equals(Axis.Primary))
                {
                    ofsetY = (axisBounds[Axis.Primary].Maximum - axisBounds[Axis.Primary].Minimum) / 20;
                    GL.Ortho(axisBounds[Axis.Category].Minimum - ofsetX, axisBounds[Axis.Category].Maximum + ofsetX,
                        axisBounds[Axis.Primary].Minimum  - ofsetY, axisBounds[Axis.Primary].Maximum + ofsetY, 0, 1);
                }
                else
                {
                    ofsetY = (axisBounds[Axis.Secondary].Maximum - axisBounds[Axis.Secondary].Minimum) / 20;
                    GL.Ortho(axisBounds[Axis.Category].Minimum - ofsetX, axisBounds[Axis.Category].Maximum + ofsetX
                        , axisBounds[Axis.Secondary].Minimum - ofsetY, axisBounds[Axis.Secondary].Maximum + ofsetY, 0, 1);
                }
                GL.Begin(PrimitiveType.LineStrip);
                {
                    foreach (double x in value.X)
                    {
                        GL.Vertex2(x, value[x]);
                    }
                }
                GL.End();
                GL.PopMatrix();
                GL.LineStipple(1, 0xFFFF);


                GL.MatrixMode(MatrixMode.Projection);
                GL.PushMatrix();
                GL.LoadIdentity();
                
                GL.Ortho(viewport[0], viewport[0] + viewport[2], viewport[1], viewport[1] + viewport[3], 0, 1);

                foreach (double x in value.X)
                {
                    Point2D viewportCoordinates = Utilities.WorldToViewport(x, value[x], World.Left, World.Right, World.Bottom, World.Top);
                    if (value.Axis.Equals(Axis.Secondary)) {
                        viewportCoordinates = Utilities.WorldToViewport(x, value[x], World2.Left, World2.Right, World2.Bottom, World2.Top);
                    }
                    var operation = isSelectedSeries ? value.Marker.SelectedPrimitive : value.Marker.DefaultPrimitive;
                    GL.Begin(operation);
                    {
                        value.Marker.Drawer(viewportCoordinates.X, viewportCoordinates.Y);
                    }
                    GL.End();
                }
                GL.PopMatrix();

                            
            }
        }
        
        private void DrawChartAxes()
        {
            GL.Color3(Color.Black);
            GL.Begin(PrimitiveType.Lines);
            {
                //  Category Axis
                GL.Vertex2(axisBounds[Axis.Category].Minimum, axisBounds[Axis.Primary].Minimum);
                GL.Vertex2(axisBounds[Axis.Category].Maximum, axisBounds[Axis.Primary].Minimum);

                //  Primary Value Axis
                GL.Vertex2(axisBounds[Axis.Category].Minimum, axisBounds[Axis.Primary].Minimum);
                GL.Vertex2(axisBounds[Axis.Category].Minimum, axisBounds[Axis.Primary].Maximum);

                //  Secondary Value Axis
                GL.Vertex2(axisBounds[Axis.Category].Maximum, axisBounds[Axis.Primary].Minimum);
                GL.Vertex2(axisBounds[Axis.Category].Maximum, axisBounds[Axis.Primary].Maximum);

                //  Top
                GL.Vertex2(axisBounds[Axis.Category].Maximum, axisBounds[Axis.Primary].Maximum);
                GL.Vertex2(axisBounds[Axis.Category].Minimum, axisBounds[Axis.Primary].Maximum);
            }
            GL.End();

            double increment = axisBounds[Axis.Category].Range / (NUMBER_OF_TICKS);                        
            double tickLength = tickLengthPixels * axisBounds[Axis.Primary].Range / Viewport.Height;

            //  Horizontal tick marks
            GL.Begin(PrimitiveType.Lines);
            {
                for (int i = 0; i <= NUMBER_OF_TICKS; i++)
                {
                    var x = axisBounds[Axis.Category].Minimum + i*increment;
                    GL.Vertex2(x, axisBounds[Axis.Primary].Minimum);
                    GL.Vertex2(axisBounds[Axis.Category].Minimum + i * increment, axisBounds[Axis.Primary].Minimum + tickLength);                    
                }
            }
            GL.End();

            //  Vertical tick marks            
            increment = axisBounds[Axis.Primary].Range / (NUMBER_OF_TICKS);
            tickLength = tickLengthPixels * axisBounds[Axis.Category].Range / Viewport.Width;

            GL.Begin(PrimitiveType.Lines);
            {
                for (int i = 1; i <= NUMBER_OF_TICKS + 1; i++)
                {
                    GL.Vertex2(axisBounds[Axis.Category].Minimum - tickLength, axisBounds[Axis.Primary].Minimum + i*increment);
                    GL.Vertex2(axisBounds[Axis.Category].Minimum, axisBounds[Axis.Primary].Minimum + i*increment);
                }
            }
            GL.End();
        }

        #endregion

        public override ICollection<string> LegendEntries
        {
            get {
                return dataSeries.Keys;
            }
        }

        public override Color3 GetLegendColor(string item)
        {
            return dataSeries[item].Color;
        }

        public override void ClearSelection()
        {
            selectedSeries = null;
        }

        public override Selection GetSelection(double wx, double wy)
        {
            int xFudge = 5;
            double distanceThreshold = 5;

            foreach (DataSeries ds in dataSeries.Values)
            {
                double Right = World.Right;
                double Left =  World.Left;
                double Bottom = World.Bottom;
                double Top = World.Top;

                double wx1 = wx;
                double wy1 = wy;
                if (ds.Axis.Equals(Axis.Secondary))
                {
                    Right = World2.Right;
                    Left = World2.Left;
                    Bottom = World2.Bottom;
                    Top = World2.Top;

                    double worldWidth = World2.Right - World2.Left;
                    double worldHeight = World2.Top - World2.Bottom;
                    double worldLeft = World2.Left;
                    double worldBottom = World2.Bottom;

                    wx1 = (ChartCreator.sx - ChartCreator._chartViewport.Location.X) / (ChartCreator._chartViewport.Width * 1.0) * worldWidth + World2.Left;
                    wy1 = (ChartCreator.sy - ChartCreator._chartViewport.Location.Y) / (ChartCreator._chartViewport.Height * 1.0) * worldHeight + World2.Bottom;
                }

                Point2D clickLocation = Utilities.WorldToViewport(wx1, wy1, Left, Right, Bottom, Top);
                for (int ptIndex = 0; ptIndex < ds.points.Count - 1; ptIndex++)
                {
                    double x = ds.X[ptIndex];
                    double y = ds[x];
                    Point2D start = Utilities.WorldToViewport(x, y, Left, Right, Bottom, Top);
                    x = ds.X[ptIndex + 1];
                    y = ds[x];
                    Point2D end = Utilities.WorldToViewport(x, y, Left, Right, Bottom, Top);


                    if (clickLocation.X >= (start.X - xFudge) && clickLocation.X <= (end.X + xFudge))
                    {
                        Vector3 v = new Vector3(start, end);
                        Vector3 vPerp = v.Perp;
                        
                        Vector3 c = new Vector3(start, clickLocation);
                        double distance = Math.Abs(vPerp.Dot(c) / vPerp.Length);
                        if (distance <= distanceThreshold)
                        {
                            selectedSeries = ds;
                            return new DataSeriesSelection(ds);
                        }
                    }
                }
                
            }

            selectedSeries = null;
            return null;
        }
        
        public override CoordinateSystem World
        {
            get
            {
                double width = axisBounds[Axis.Category].Maximum - axisBounds[Axis.Category].Minimum;
                double extraWidth = 0.05 * width;

                double height = axisBounds[Axis.Primary].Maximum - axisBounds[Axis.Primary].Minimum;
                double extraHeight = 0.05 * height;

                return new CoordinateSystem(
                    axisBounds[Axis.Category].Minimum-extraWidth,
                    axisBounds[Axis.Category].Maximum+extraWidth,
                    axisBounds[Axis.Primary].Minimum-extraHeight,
                    axisBounds[Axis.Primary].Maximum+extraHeight
                );
            }
        }

        public CoordinateSystem World2
        {
            get
            {
                double width = axisBounds[Axis.Category].Maximum - axisBounds[Axis.Category].Minimum;
                double extraWidth = 0.05 * width;

                double height = axisBounds[Axis.Secondary].Maximum - axisBounds[Axis.Secondary].Minimum;
                double extraHeight = 0.05 * height;

                return new CoordinateSystem(
                    axisBounds[Axis.Category].Minimum - extraWidth,
                    axisBounds[Axis.Category].Maximum + extraWidth,
                    axisBounds[Axis.Secondary].Minimum - extraHeight,
                    axisBounds[Axis.Secondary].Maximum + extraHeight
                );
            }
        }

        public override void DrawLegendMarker(string seriesName, double dx, double dy)
        {
            int x = (int)dx;
            int y = (int)dy;

            GL.Begin(PrimitiveType.Lines);
            {
                GL.Vertex2(x, y + ChartCreator.BoxHeight / 2);
                GL.Vertex2(x + ChartCreator.BoxWidth, y + ChartCreator.BoxHeight / 2);
            }
            GL.End();

            DataSeries series = dataSeries[seriesName];

            GL.Begin(series.Marker.DefaultPrimitive);
            {
                series.Marker.Drawer(x + ChartCreator.BoxWidth / 2, y + ChartCreator.BoxHeight / 2);
            }
            GL.End();
        }

        public override Selection SetLegendSelection(string changeLegend)
        {
            selectedSeries = dataSeries[changeLegend];
            return new DataSeriesSelection(dataSeries[changeLegend]);
        }
    }
}
