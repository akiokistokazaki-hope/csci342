﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chart_Creator
{
    public interface Selection
    {
        

        bool CanChangeColor { get; }
        bool CanChangeFillColor { get; }
        void ChangeColor(byte r, byte g, byte b);
        void ChangeFillColor(byte r, byte g, byte b);

        Color3 CurrentColor { get; }
        Color3 CurrentFillColor { get; }

        Action DefaultAction { get; }
    }

    public enum Action
    {
        ChangeSelectionColor, ChangeFillColor
    };
}
