﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using csci342;
using OpenTK.Graphics.OpenGL;

namespace Chart_Creator
{
    public static class Utilities
    {
        public static Point2D WorldToViewport(double wx, double wy, double wl, double wr, double wb, double wt)
        {
            var viewport = new int[4];
            GL.GetInteger(GetPName.Viewport, viewport);
            double sl = viewport[0];
            double sb = viewport[1];
            double sr = sl + viewport[2];
            double st = sb + viewport[3];

            var sx = (wx - wl)/(wr - wl)*(sr - sl) + sl;
            var sy = (wy - wb)/(wt - wb)*(st - sb) + sb; //var sy = (wy - wb)/(wt - wb)*(st - sb) + wb;

            return new Point2D(sx, sy);
        }

        public static double DegreesToRadians(double degrees)
        {
            return Math.PI/180*degrees;
        }

        public static void SetForegroundBlack()
        {
            GL.Color3(0, 0, 0);
        }

        public static void SetWindow(double left, double right, double bottom, double top)
        {
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(left, right, bottom, top, -1, 1);
        }
    }
}
