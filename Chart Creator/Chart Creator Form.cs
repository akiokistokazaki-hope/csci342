﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using mygraphicslib;
using OpenTK.Graphics.OpenGL;
using csci342.Text;
using System.IO;

namespace Chart_Creator
{
    public partial class Form1 : Form
    {
        private bool controlLoaded = false;
        private bool Seted = false;
        public static int MenuY = 24;
        private String Title;
        private String FileLocation;
        private System.IO.StreamReader TextFile;
        private int SelectedIndex = 0;
        private OpenFileDialog ofd = new OpenFileDialog();
        private SaveFileDialog sfd = new SaveFileDialog();
        private ColorDialog cd = new ColorDialog();
        private String FileContent;
        private ChartState chartState = new NullCahrtState();
        private Color[] Colors = new Color[8];
        private Color[] OriginColors;
        private Color TitleColor;


        public Form1()
        {
            InitializeComponent();
            LoadFile();
            Set();
        }

        public Form1(String[] args)
        {
            InitializeComponent();
            if (File.Exists(args[0]))
            {
                LoadFileFromString(args[0]);
            }
            else
            {
                LoadFile();
            }
            Set();
        }

        private void Set()
        {
            Seted = true;
            TitleColor = Color.Black;
            
            Colors[0] = (Color.FromArgb(69, 114, 167));
            Colors[1] = (Color.FromArgb(170, 70, 67));
            Colors[2] = (Color.FromArgb(137, 165, 78));
            Colors[3] = (Color.FromArgb(113, 88, 143));
            Colors[4] = (Color.FromArgb(65, 152, 175));
            Colors[5] = (Color.FromArgb(219, 132, 61));
            Colors[6] = (Color.FromArgb(147, 169, 207));
            Colors[7] = (Color.FromArgb(209, 147, 146));
            OriginColors = new Color[8];
            OriginColors[0] = (Color.FromArgb(69, 114, 167));
            OriginColors[1] = (Color.FromArgb(170, 70, 67));
            OriginColors[2] = (Color.FromArgb(137, 165, 78));
            OriginColors[3] = (Color.FromArgb(113, 88, 143));
            OriginColors[4] = (Color.FromArgb(65, 152, 175));
            OriginColors[5] = (Color.FromArgb(219, 132, 61));
            OriginColors[6] = (Color.FromArgb(147, 169, 207));
            OriginColors[7] = (Color.FromArgb(209, 147, 146));

            glControl1.MouseClick += SelectMousePoint;
        }
        
        private void LoadFile()
        {
            ofd.FileName = "";
            ofd.Filter = "TXT files|*.txt";
            ofd.FilterIndex = 1;
            ofd.Title = "Select text to load";
            ofd.RestoreDirectory = true;
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                LoadFileFromString(ofd.FileName);
            }
        }


        private void LoadFileFromString(string v)
        {
            ofd.FileName = v;
            // make save enable
            saveToolStripMenuItem.Enabled = true;

            FileLocation = ofd.FileName;
            string line;
            StringBuilder fileString = new StringBuilder();

            TextFile = new System.IO.StreamReader(ofd.FileName);

            // to allow user to move the file somewhere will using this
            //FileContent = TextFile.ReadToEnd();
            //TextFile.DiscardBufferedData();
            //TextFile.BaseStream.Seek(0, SeekOrigin.Begin);
            //TextFile.BaseStream.Position = 0;

            line = TextFile.ReadLine();
            if (line != null)
            {
                Title = TextFile.ReadLine();
                if (Title != null)
                {
                    if (TextFile.ReadLine().Equals("Colors"))
                    {
                        int[] rgb = new int[3];
                        for (int i = 0; i < 8; i++)
                        {
                            rgb = Array.ConvertAll(TextFile.ReadLine().Split(' '), int.Parse);
                            Colors[i] = (Color.FromArgb(rgb[0], rgb[1], rgb[2]));
                        }
                    }
                    else
                    {
                        Colors[0] = (Color.FromArgb(69, 114, 167));
                        Colors[1] = (Color.FromArgb(170, 70, 67));
                        Colors[2] = (Color.FromArgb(137, 165, 78));
                        Colors[3] = (Color.FromArgb(113, 88, 143));
                        Colors[4] = (Color.FromArgb(65, 152, 175));
                        Colors[5] = (Color.FromArgb(219, 132, 61));
                        Colors[6] = (Color.FromArgb(147, 169, 207));
                        Colors[7] = (Color.FromArgb(209, 147, 146));
                        TextFile.DiscardBufferedData();
                        TextFile.BaseStream.Seek(0, SeekOrigin.Begin);
                        TextFile.BaseStream.Position = 0;
                        TextFile.ReadLine();
                        TextFile.ReadLine();
                    }
                    if (line == "p")
                    {
                        chartState = new PieChartState(TextFile, Title, this);
                    }
                    else if (line == "l")
                    {
                        chartState = new LineChartState(TextFile, Title, this);
                    }
                    if (Seted){ 
                    chartState.SetViewport(glControl1.Width, glControl1.Height);
                    glControl1.Invalidate(); }
                }
            }
            if (Seted) {
                PaintChart();
            }
            TextFile.Close();
        }

        private void SelectMousePoint(object sender, MouseEventArgs e)
        {
            //Console.WriteLine(e.X);
            //Console.WriteLine(glControl1.Height - e.Y);
            SelectedIndex = chartState.SetSelect(new Point2D(e.X, glControl1.Height - e.Y));
            PaintChart();
        }


        public void SetFillColor(bool b)
        {
            fillColorToolStripMenuItem.Enabled = b;
        }

        public void SetFormatColor(bool b)
        {
            colorToolStripMenuItem.Enabled = b;
        }

        public Color GetColor(int i)
        {
            return Colors[i];
        }
        public Color GetTitleColor()
        {
            return TitleColor;
        }
        private void glControl1_Load(object sender, EventArgs e)
        {
            controlLoaded = true;
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            DrawingTools.EnableDefaultAntiAliasing();
            FontGenerator.LoadTexture("Consolas");
        }

       

        private void loadFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadFile();
        }

        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            if (!controlLoaded) return;
            PaintChart();
        }

        private void PaintChart()
        {
            chartState.Paint();
            glControl1.SwapBuffers();
        }
        private void glControl1_Resize(object sender, EventArgs e)
        {
            chartState.SetViewport(glControl1.Width, glControl1.Height);
        }

        private void colorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cd.AllowFullOpen = true;
            cd.SolidColorOnly = true;
            cd.AnyColor = true;

            if (cd.ShowDialog() == DialogResult.OK)
            {
                TitleColor = cd.Color;
            }
            PaintChart();
        }

        private void fillColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cd.AllowFullOpen = true;
            cd.SolidColorOnly = true;
            cd.AnyColor = true;

            if (cd.ShowDialog() == DialogResult.OK)
            {
                Colors[SelectedIndex] = cd.Color;
            }
            PaintChart();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            TextFile = new System.IO.StreamReader(FileLocation);
            sfd.FileName = Title + ".txt";
            sfd.InitialDirectory = @"C:\";
            sfd.Filter = "TXT(*.txt;)|*.txt";
            sfd.FilterIndex = 2;
            sfd.Title = "Select the location for save";
            sfd.RestoreDirectory = true;
            sfd.OverwritePrompt = true;
            sfd.CheckPathExists = true;

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                System.IO.Stream stream;
                stream = sfd.OpenFile();
                if (stream != null)
                {
                    //write things
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(stream);
                    // read up to title
                    sw.WriteLine(TextFile.ReadLine());
                    sw.WriteLine(TextFile.ReadLine());

                    bool changed = false;
                    for (int i = 0; i < 8; i++)
                    {
                        if (OriginColors[i] != Colors[i])
                        {
                            changed = true;
                        }
                    }

                    if (changed)
                    {
                        if (TextFile.ReadLine().Equals("Colors"))
                        {
                            for (int i = 0; i < 8; i++)
                            {
                                sw.Write(Colors[i].R);
                                sw.Write(' ');
                                sw.Write(Colors[i].G);
                                sw.Write(' ');
                                sw.Write(Colors[i].B);
                                sw.Write('\n');
                                TextFile.ReadLine();
                            }
                        }
                        else
                        {
                            TextFile.DiscardBufferedData();
                            TextFile.BaseStream.Seek(0, SeekOrigin.Begin);
                            TextFile.BaseStream.Position = 0;
                            TextFile.ReadLine();
                            TextFile.ReadLine();
                            sw.WriteLine("Colors");
                            for (int i = 0; i < 8; i++)
                            {
                                sw.Write(Colors[i].R);
                                sw.Write(' ');
                                sw.Write(Colors[i].G);
                                sw.Write(' ');
                                sw.Write(Colors[i].B);
                                sw.Write('\n');
                            }
                        }
                        
                    }
                    while (TextFile.Peek() > -1)
                    {
                        sw.WriteLine(TextFile.ReadLine());
                    }
                    sw.Close();
                    stream.Close();
                }
                
            }
        }
    }
}
