﻿using csci342;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chart_Creator
{
    interface ChartState
    {
        void Paint();// 画面にチャートを表示

        void SetViewport(int width, int height);// SetViewport using glControl's w and h

        String GetSelected();

        int SetSelect(Point2D clickP);//選択されている部分をクリックされた所から設定してpaintを呼ぶ
    }
}
