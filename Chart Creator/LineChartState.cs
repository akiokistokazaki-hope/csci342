﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using csci342;
using mygraphicslib;
using OpenTK.Graphics.OpenGL;
using csci342.Text;
using System.Drawing;

namespace Chart_Creator
{
    class LineChartState : ChartState
    {
        private List<PrimitiveType> TypeList;
        private int CurrentGlHeight;
        private int CurrentGlWidth;
        private double MaxX=0;
        private double MaxY=0;
        private double MaxStringX=10;
        private Form1 form1;
        private StreamReader textFile;
        private string title;
        private Dictionary<String, List<Point2D>> PointMap;
        private List<List<Point2D>> realPointMap;
        private int Selected=-1;
        private int size;

        public LineChartState(StreamReader textFile, string title, Form1 form1)
        {
            this.textFile = textFile;
            this.title = title;
            this.form1 = form1;
            PointMap = new Dictionary<string, List<Point2D>>();
            TypeList = new List<PrimitiveType>();
            String line;
            String name;
            double[] xy;
            realPointMap = new List<List<Point2D>>();


            int counter = 0;
            while ((line = textFile.ReadLine()) != null)
            {
                String[] lineS = line.Split(',');
                if (lineS[0].Equals("DS") ){
                    List<Point2D> points = new List<Point2D>();
                    name = lineS[1];
                    while (textFile.Peek() != '-' && textFile.Peek()>-1) {
                        xy = Array.ConvertAll(textFile.ReadLine().Split(','), double.Parse);
                        if (xy[0]>MaxX) { MaxX = xy[0]; }
                        if (xy[1]>MaxY) { MaxY = xy[1]; }
                           
                        Point2D p = new Point2D(xy[0], xy[1]);
                        points.Add(p);
                    }
                    TypeList.Add(PrimitiveType.Polygon);
                    PointMap.Add(name,points);
                    if (FontGenerator.GetSize(name).Width>MaxStringX) {
                        MaxStringX = FontGenerator.GetSize(name).Width;
                    }
                    textFile.ReadLine();
                }
                counter++;
            }
            textFile.Close();
            size = PointMap.Count();
        }

        public string GetSelected()
        {
            return "";
        }

        public void Paint()
        {
            GL.ClearColor(Color.White);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.MatrixMode(MatrixMode.Projection);
            if (0 < (int)(CurrentGlWidth - (MaxStringX * 1.2) - CurrentGlWidth * 0.1)) {
                GL.Viewport((int)(CurrentGlWidth * 0.05), (int)(CurrentGlHeight * 0.05),
                (int)(CurrentGlWidth - (MaxStringX * 1.2) - CurrentGlWidth * 0.1), (int)(CurrentGlHeight * 0.9));
            }

            Utilities.SetWindow(-1*MaxX*0.02, MaxX + MaxX*0.02, -1*MaxY*0.02, MaxY + MaxY*0.02);


            GL.Color3(Color.Black);
            GL.Begin(PrimitiveType.LineLoop);
            {
                // draw the outside box
                GL.Vertex2(0, 0);
                GL.Vertex2(0, MaxY);
                GL.Vertex2(MaxX, MaxY);
                GL.Vertex2(MaxX, 0);
            }
            GL.End();

            GL.Begin(PrimitiveType.Lines);
            {
                for (int i=1;i<11;i++) {
                    // draw the 10 small lines
                    GL.Vertex2(i*(MaxX/10), 0);
                    GL.Vertex2(i*(MaxX/10),  MaxY * 0.02);
                    GL.Vertex2(0, i * (MaxY / 10));
                    GL.Vertex2(-1*MaxX * 0.02, i * (MaxY / 10));
                }
            }
            GL.End();

            size = PointMap.Count();
            GL.PointSize(10);
            for (int i = 0; i < size; i++) {
                if (i==Selected)
                {
                    GL.Disable(EnableCap.LineStipple);
                    GL.Enable(EnableCap.LineStipple);
                    GL.LineStipple(2, 0x0F0F);
                }
                List<Point2D> points = PointMap.Values.ToArray()[i];
                GL.Color3(form1.GetColor(i));
                    GL.Begin(PrimitiveType.LineStrip);
                    {
                        foreach (Point2D point in points)
                        {
                            GL.Vertex2(point.X, point.Y);
                        }
                    }
                    GL.End();
                GL.Disable(EnableCap.LineStipple);
                if (i==0)
                {
                    // squar
                    GL.Color3(form1.GetColor(i));
                    if (TypeList[i] == PrimitiveType.Polygon)
                    {
                        GL.Begin(PrimitiveType.Points);
                        {
                            foreach (Point2D point in points)
                            {
                                GL.Vertex2(point.X, point.Y);
                            }
                        }
                        GL.End();
                    }
                    else {
                        
                            foreach (Point2D point in points)
                            {
                            GL.Begin(PrimitiveType.LineStrip);
                            {
                                GL.Vertex2(point.X + MaxX * 0.02, point.Y + MaxY * 0.02);
                                GL.Vertex2(point.X + MaxX * 0.02, point.Y - MaxY * 0.02);
                                GL.Vertex2(point.X - MaxX * 0.02, point.Y - MaxY * 0.02);
                                GL.Vertex2(point.X - MaxX * 0.02, point.Y + MaxY * 0.02);
                                GL.Vertex2(point.X + MaxX * 0.02, point.Y + MaxY * 0.02);
                            }
                            GL.End();
                        }
                        
                    }
                }
                if (i == 1)
                {
                    
                    //circle
                    GL.Color3(form1.GetColor(i));
                    foreach (Point2D point in points)
                    {
                        GL.Begin(TypeList[i]);
                        {
                            double circlesize = 0.15;
                            if (MaxX>MaxY) {
                                circlesize = MaxY * 0.02;
                            }
                            else {
                                circlesize = MaxX * 0.02;
                            }
                            Utilities.DrawArc(point.X, point.Y, circlesize);
                        }
                        GL.End();
                    }
                    
                }
                if (i == 2)
                {
                    //triangle
                    GL.Color3(form1.GetColor(i));
                    foreach (Point2D point in points)
                    {
                        GL.Begin(TypeList[i]);
                        {
                            GL.Vertex2(point.X, point.Y+MaxY*0.02);
                            GL.Vertex2(point.X+ MaxX*0.02, point.Y- MaxY * 0.01);
                            GL.Vertex2(point.X- MaxX*0.02, point.Y- MaxY * 0.01);
                            GL.Vertex2(point.X, point.Y + MaxY * 0.02);
                        }
                        GL.End();
                    }
                }
                if (i == 3)
                {
                    //ダイヤモンド
                    GL.Color3(form1.GetColor(i));
                    foreach (Point2D point in points)
                    {
                       GL.Begin(TypeList[i]);
                       {
                            GL.Vertex2(point.X+MaxX*0.02, point.Y);
                            GL.Vertex2(point.X, point.Y - MaxY * 0.02);
                            GL.Vertex2(point.X - MaxX*0.02, point.Y);
                            GL.Vertex2(point.X, point.Y+ MaxY * 0.02);
                            GL.Vertex2(point.X + MaxX * 0.02, point.Y);
                        }
                        GL.End();
                    }
                }
                    if (i == 4)
                {
                    GL.LineWidth(2);
                    GL.Begin(PrimitiveType.Lines);
                    {
                        // +
                        foreach (Point2D point in points)
                        {
                            GL.Vertex2(point.X + MaxX * 0.02, point.Y);
                            GL.Vertex2(point.X - MaxX * 0.02, point.Y);
                            GL.Vertex2(point.X, point.Y + MaxY * 0.02);
                            GL.Vertex2(point.X, point.Y - MaxY * 0.02);
                        }
                    }
                    GL.End();
                }
                if (i == 5)
                {
                    GL.LineWidth(2);
                    GL.Begin(PrimitiveType.Lines);
                    {
                        foreach (Point2D point in points)
                        {
                            // X
                            GL.Vertex2(point.X + MaxX * 0.018, point.Y + MaxY * 0.018);
                            GL.Vertex2(point.X - MaxX * 0.018, point.Y - MaxY * 0.018);
                            GL.Vertex2(point.X - MaxX * 0.018, point.Y + MaxY * 0.018);
                            GL.Vertex2(point.X + MaxX * 0.018, point.Y - MaxY * 0.018);
                        }
                    }
                    GL.End();
                }
                if (i == 6)
                {
                    GL.Begin(PrimitiveType.Lines);
                    {
                        foreach (Point2D point in points)
                        {
                            // *
                            GL.Vertex2(point.X + MaxX * 0.014, point.Y + MaxY * 0.014);
                            GL.Vertex2(point.X - MaxX * 0.014, point.Y - MaxY * 0.014);
                            GL.Vertex2(point.X - MaxX * 0.014, point.Y + MaxY * 0.014);
                            GL.Vertex2(point.X + MaxX * 0.014, point.Y - MaxY * 0.014);
                            GL.Vertex2(point.X, point.Y + MaxY * 0.014);
                            GL.Vertex2(point.X, point.Y - MaxY * 0.014);
                        }
                    }
                    GL.End();
                }
                if (i == 7)
                {
                        foreach (Point2D point in points)
                        {
                            GL.Begin(TypeList[i]);
                            {
                                GL.Vertex2(point.X + MaxX * 0.01, point.Y + MaxY * 0.01);

                                GL.Vertex2(point.X + MaxX * 0.02, point.Y);

                                GL.Vertex2(point.X + MaxX * 0.01, point.Y - MaxY * 0.01);
                                GL.Vertex2(point.X - MaxX * 0.01, point.Y - MaxY * 0.01);

                                GL.Vertex2(point.X - MaxX * 0.02, point.Y);

                                GL.Vertex2(point.X - MaxX * 0.01, point.Y + MaxY * 0.01);
                                GL.Vertex2(point.X + MaxX * 0.01, point.Y + MaxY * 0.01);
                        }
                            GL.End();
                        }
                }
                GL.LineWidth(1);
            }

            // display the file name
            GL.PushMatrix();
            {
                GL.Color3(form1.GetTitleColor());
                GL.Viewport(0, 0, CurrentGlWidth, CurrentGlHeight);
                Utilities.SetWindow(0, CurrentGlWidth, 0, CurrentGlHeight);

                TextDrawer.DrawString(title, CurrentGlWidth / 2 - FontGenerator.GetSize(title).Width / 2,
                    CurrentGlHeight - FontGenerator.GetSize(title).Height, 1);

            }
            GL.PopMatrix();
            // draw rightside
            DrawRight((int)(CurrentGlWidth - (MaxStringX * 1.5)));

        }

        private void DrawRight(int startX)
        {
            GL.PushMatrix();
            {
                GL.Viewport(0, 0, CurrentGlWidth, CurrentGlHeight);
                Utilities.SetWindow(0, CurrentGlWidth, 0, CurrentGlHeight);


                double Ssize = FontGenerator.GetSize("S").Height;
                double allowedY = size * Ssize * 1.5;
                double startP = CurrentGlHeight / 2 + allowedY / 2;
                double endP = CurrentGlHeight / 2 - allowedY / 2;
                double step = (startP - endP) / size;


                GL.Color3(Color.Black);
                GL.Begin(PrimitiveType.LineLoop);
                {
                    GL.Vertex2(CurrentGlWidth - MaxStringX * 1.2 - CurrentGlWidth * 0.05, endP);
                    GL.Vertex2(CurrentGlWidth - 1, endP);
                    GL.Vertex2(CurrentGlWidth - 1, startP);
                    GL.Vertex2(CurrentGlWidth - MaxStringX * 1.2 - CurrentGlWidth * 0.05, startP);
                }
                GL.End();

                for (int i = 0; i < size; i++)
                {
                    GL.Color3(form1.GetColor(i));

                    TextDrawer.DrawString(PointMap.Keys.ToArray()[i], CurrentGlWidth - MaxStringX * 1.1,
                    startP - (i * step) - step * 0.8, 1);

                    GL.Color3(form1.GetColor(i));
                    double Y = startP - (i * step) - step * 0.8;
                    GL.Begin(PrimitiveType.Lines);
                    {
                        GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01, Y + Ssize / 2);
                        GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.05, Y + Ssize / 2);
                    }
                    GL.End();

                    if (i == 0)
                    {
                        // squar
                        if (Selected!=0) { 
                        GL.Begin(PrimitiveType.Polygon);
                        {
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.02, Y);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.04, Y);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.04, Y + Ssize);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.02, Y + Ssize);
                         }
                        GL.End();
                        }
                        else
                        {
                            // squar
                            GL.Begin(PrimitiveType.LineLoop);
                            {
                                GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.02, Y);
                                GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.04, Y);
                                GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.04, Y + Ssize);
                                GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.02, Y + Ssize);
                            }
                            GL.End();
                        }
                    }
                    if (i == 1)
                    {

                        //circle
                        GL.Color3(form1.GetColor(i));
                        GL.Begin(TypeList[i]);
                        {
                            Utilities.DrawArc(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.025,
                                Y + Ssize / 2, step/3);
                        }
                        GL.End();

                    }
                    if (i == 2)
                    {
                        //triangle
                        GL.Color3(form1.GetColor(i));
                        GL.Begin(TypeList[i]);
                        {
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.025, Y + Ssize);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.02, Y);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.04, Y);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.025, Y + Ssize);
                        }
                        GL.End();
                    }
                    if (i == 3)
                    {
                        //ダイヤモンド
                        GL.Color3(form1.GetColor(i));
                        GL.Begin(TypeList[i]);
                        {
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.025, Y + Ssize);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.04, Y + Ssize / 2);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.025, Y);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.02, Y + Ssize / 2);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.025, Y + Ssize);
                        }
                        GL.End();
                    }
                    if (i == 4)
                    {
                        GL.LineWidth(2);
                        GL.Begin(PrimitiveType.Lines);
                        {
                            // + 
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.04, Y + Ssize / 2);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.02, Y + Ssize / 2);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.025, Y + Ssize);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.025, Y);
                        }
                        GL.End();
                    }
                    if (i == 5)
                    {
                        GL.LineWidth(2);
                        GL.Begin(PrimitiveType.Lines);
                        {
                            // X
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.04, Y + Ssize);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.02, Y);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.02, Y + Ssize);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.04, Y);
                        }

                        GL.End();
                    }
                    if (i == 6)
                    {
                        GL.LineWidth(2);
                        GL.Begin(PrimitiveType.Lines);
                        {
                            // *
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.04, Y + Ssize);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.02, Y);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.02, Y + Ssize);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.04, Y);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.04, Y + Ssize / 2);
                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.02, Y + Ssize / 2);
                        }
                        GL.End();
                    }
                    if (i == 7)
                    {
                        GL.Begin(TypeList[i]);
                        {
                            GL.Vertex2((CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.020), Y + Ssize);

                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.018, Y +Ssize / 2);

                            GL.Vertex2((CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.020), Y);

                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.035, Y);

                            GL.Vertex2(CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.045, Y + Ssize / 2);

                            GL.Vertex2((CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.035), Y + Ssize);

                            GL.Vertex2((CurrentGlWidth - MaxStringX * 1.1 - CurrentGlWidth * 0.01 - CurrentGlWidth * 0.020), Y + Ssize);

                        }
                        GL.End();
                    }
                    GL.LineWidth(1);
                }

            }
                
                
                GL.PopMatrix();
        }

        public int SetSelect(Point2D clickP)
        {
            double distance = double.MaxValue;
            int closeLineIndex=0;
            int size = realPointMap.Count();
            for (int i = 0; i < size; i++)
            {
                List<Point2D> points = realPointMap[i];
                for (int n = 1; n < points.Count();n++)
                {
                    double d = DistanceFromPoint(points[n-1], points[n],clickP);
                    if (d<distance) {
                        Console.WriteLine(d);
                        distance = d;
                        closeLineIndex = i;
                    }
                }
            }
            if (distance < 0.00005 * (CurrentGlHeight * CurrentGlWidth))
            {
                form1.SetFillColor(true);
                form1.SetFormatColor(false);
                Selected = closeLineIndex;
                for (int prim = 0; prim < TypeList.Count(); prim++)
                {
                    TypeList[prim] = PrimitiveType.Polygon;
                }
                TypeList[closeLineIndex] = PrimitiveType.LineStrip;
                return closeLineIndex;
            }


            Polyline textRect = new Polyline();
            textRect.AddPoint(new Point2D(CurrentGlWidth / 2 - FontGenerator.GetSize(title).Width / 2, CurrentGlHeight));
            textRect.AddPoint(new Point2D(CurrentGlWidth / 2 + FontGenerator.GetSize(title).Width / 2,
                CurrentGlHeight - FontGenerator.GetSize(title).Height));
            form1.SetFormatColor(InRect(textRect, clickP));

            form1.SetFillColor(false);


            if (Selected > -1) {
                TypeList[Selected] = PrimitiveType.Polygon;
                Selected = -1;
            }
            return 0;
        }

        private double DistanceFromPoint(Point2D lineP1, Point2D lineP2, Point2D clickP)
        {
            Vector3 u = new Vector3(lineP2.X - lineP1.X, lineP2.Y - lineP1.Y, 0);
            Vector3 v = new Vector3(clickP.X - lineP1.X, clickP.Y - lineP1.Y, 0);
            double L = (Math.Abs(u.Cross(v).Length / u.Length));
            if (L < 0.00005 * (CurrentGlHeight * CurrentGlWidth)) {
                Point2D sP;
                Point2D eP;
                if (lineP1.Y < lineP2.Y) {
                    sP = lineP1;
                    eP = lineP2;
                }
                else {
                    sP = lineP2;
                    eP = lineP1;
                }
                if (sP.Y - clickP.Y > 0.00005 * (CurrentGlHeight * CurrentGlWidth)) {
                    L = sP.Y - clickP.Y;
                }
                else if (clickP.Y - eP.Y > 0.00005 * (CurrentGlHeight * CurrentGlWidth)) {
                    L = clickP.Y - eP.Y;
                }
                if (lineP1.X < lineP2.X)
                {
                    sP = lineP1;
                    eP = lineP2;
                }
                else
                {
                    sP = lineP2;
                    eP = lineP1;
                }
                if (sP.X - clickP.X > 0.00005 * (CurrentGlHeight * CurrentGlWidth))
                {
                    L = sP.X - clickP.X;
                }
                else if (clickP.X - eP.X > 0.00005 * (CurrentGlHeight * CurrentGlWidth))
                {
                    L = clickP.X - eP.X;
                }
            }
            return L;
        }

        public void SetViewport(int width, int height)
        {
            CurrentGlWidth = width;
            CurrentGlHeight = height;
            realPointMap = new List<List<Point2D>>();
            
            for (int i = 0; i < size; i++)
            {
                String s = PointMap.Keys.ToList()[i];
                List<Point2D> realPoints = new List<Point2D>();
                foreach (Point2D p in PointMap[s])
                {
                    realPoints.Add(new Point2D(
                        ((p.X + MaxX * 0.02)  * ((CurrentGlWidth* 0.9 - MaxStringX * 1.2) / MaxX) + CurrentGlWidth * 0.05)*0.96,
                     ((p.Y + MaxY * 0.02) * (CurrentGlHeight * 0.9 / MaxY) + CurrentGlHeight * 0.05) * 0.96));

                    //-1 * MaxX * 0.02, MaxX + MaxX * 0.02
                    // convert the point to be the point on the sceen size
                    //Console.WriteLine((p.Y+ MaxX*0.02) * ((CurrentGlHeight * 0.86) / MaxY) + CurrentGlHeight * 0.05);
                    //Console.WriteLine((p.X+MaxX * 0.02) * (((CurrentGlWidth * 0.9 - MaxStringX * 1.2) / MaxX)) + (CurrentGlWidth * 0.05));
                }
                realPointMap.Add(realPoints);
            } 
                
        }

        
        public bool InLine(Point2D point1, Point2D point2,Point2D ClickP){
            double dxc = ClickP.X - point1.X;
            double dyc = ClickP.Y - point1.Y;

            double dxl = point2.X - point1.X;
            double dyl = point2.Y - point1.Y;

            double cross = dxc * dyl - dyc * dxl;

            if (cross != 0)
            {
                return false;
            }
            if (Math.Abs(dxl) >= Math.Abs(dyl))
                return dxl > 0 ?
                  point1.X <= ClickP.X && ClickP.X <= point2.X :
                  point2.X <= ClickP.X && ClickP.X <= point1.X;
            else
                return dyl > 0 ?
                  point1.Y <= ClickP.Y && ClickP.Y <= point2.Y :
                  point2.Y <= ClickP.Y && ClickP.Y <= point1.Y;
        }

        private bool InPoint(Point2D rect, Point2D p)
        {
            //Console.WriteLine(rect.X + ":" + rect.Y); 
            //Console.WriteLine(p.X + ":" + p.Y);
            // for 100 pix 1 size
            double minX = rect.X - CurrentGlWidth / 100;
            double maxX = rect.X + CurrentGlWidth / 100;
            double minY = rect.Y - CurrentGlHeight / 100;
            double maxY = rect.Y + CurrentGlHeight / 100;
            if (p.X >= minX && p.X <= maxX && p.Y >= minY && p.Y <= maxY)
            {
                return true;
            }

            return false;
        }

        private bool InRect(Polyline rect, Point2D p)
        {

            double minX = double.MaxValue;
            double maxX = double.MinValue;
            double minY = double.MaxValue;
            double maxY = double.MinValue;
            foreach (Point2D point in rect.points)
            {
                minX = Math.Min(point.X, minX);
                maxX = Math.Max(point.X, maxX);
                minY = Math.Min(point.Y, minY);
                maxY = Math.Max(point.Y, maxY);
            }
            if (p.X >= minX && p.X <= maxX && p.Y >= minY && p.Y <= maxY)
            {
                return true;
            }

            return false;
        }
    }

}
