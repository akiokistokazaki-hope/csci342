﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using csci342;
using mygraphicslib;
using System.Drawing;
using csci342.Text;

namespace Chart_Creator
{
    public class PieChartState : ChartState
    {
        private PieRightSide rightSide;
        private int CurrentViewportSize= 0;
        private int offsetY=0;
        private int offsetX = 0;
        private int CurrentGlWidth = 0;
        private int CurrentGlHeight= 0;
        private Point2D CenterP = new Point2D(0,0);
        private double radius = 0;
        private String FileName;
        private Point2D[] SelectedPoints= new Point2D[3];
        private Dictionary<String, double> degreeMap;
        private Dictionary<String, List<Point2D>> pointMap;
        private Dictionary<String, Polyline> realPointMap;
        private List<String> names;
        private Form1 form;

        public PieChartState(System.IO.StreamReader file, String fileName, Form1 form) {
            this.form = form;
            FileName = fileName;
            SelectedPoints[0] = new Point2D(-2, -2);
            SelectedPoints[1] = new Point2D(-2, -2);
            SelectedPoints[2] = new Point2D(-2, -2);
            names = new List<string>();
            degreeMap = new Dictionary<string, double>();
            pointMap = new Dictionary<string, List<Point2D>>();
            realPointMap = new Dictionary<string, Polyline>();
            int counter = 0;
            string line;

            while ((line = file.ReadLine()) != null)
            {
                if (degreeMap.ContainsKey(line)) {
                    degreeMap[line] = degreeMap[line] + 1;
                }
                else {
                    degreeMap.Add(line,1);
                    names.Add(line);
                    // if degreeMap.Count is bigger than 8 throw error
                }
                counter++;
            }

            rightSide = new PieRightSide(names, form);

            int size = degreeMap.Count();
            double total = 0.0;
            foreach (double d in degreeMap.Values)
            {
                total += d;
            }

            double strartDegree = 0;
            for (int i=0; i<size ;i++) {
                String s = degreeMap.Keys.ToList()[i];
                double degree = 360 / total * degreeMap[s];
                degreeMap[s] = degree;
                SetPoints(s, strartDegree, degree);
                strartDegree += degree;
            }
            file.Close();
            file.Close();
            
        }

        private void SetPoints(string s, double strartDegree, double degree)
        {
            // finout a better way than looping 36000 times
            double n = strartDegree+degree;//360;//4960;
            double rate;
            List<Point2D> points = new List<Point2D>();
            double x;
            double y;
            for (double i = strartDegree; i <= n; i=i+1)
            {
                rate = i / 360;
                x = 0.9 * Math.Cos(2.0 * Math.PI * rate);
                y = 0.9 * Math.Sin(2.0 * Math.PI * rate);
                points.Add(new Point2D(x,y));
            }
            x = 0.9 * Math.Cos(2.0 * Math.PI * n/360);
            y = 0.9 * Math.Sin(2.0 * Math.PI * n/360);
            points.Add(new Point2D(x, y));
            pointMap.Add(s,points);
        }
   

        public string GetSelected()
        {
            return "";
        }

        public void SetViewport(int width, int height) {
            realPointMap = new Dictionary<string, Polyline>();
            int w = CurrentGlWidth = width;
            int h = CurrentGlHeight = height;
            offsetY = 0;
            offsetX = 0;
            double circleSize = width * 0.6;
            if (width * 0.6 > height*0.7)
            {
                circleSize = height * 0.7;
                w = (int)(height * 0.7);
                h = (int)(height * 0.7);
                offsetY = (int)((height - height * 0.7) / 2);
            }
            else
            {
                w = (int)(width * 0.6);
                h = w;
                offsetY = (int)((height - w) / 2);

            }
            offsetY = (int)(offsetY * 0.92);// 少しだけ低く表示
            offsetX = (int)(w * 0.1); //(width * 0.06)だと心配
            CurrentViewportSize = (int)(w * 1.1);
            //中央に合わせる
            offsetX = (int)(width /2 - CurrentViewportSize + w * 0.25);

            GL.Viewport(offsetX, offsetY, CurrentViewportSize, CurrentViewportSize);
            //

            Utilities.SetWindow(-1, 1, -1, 1);
            radius = circleSize / 2;//width * 0.3;
            double startX = (offsetX) + (circleSize) * 0.05;
            double endX = startX + (circleSize);
            double MidX = radius + startX;

            double startY = offsetY + (circleSize) * 0.05;
            double endY = startY + circleSize;
            double MidY = radius + startY;

            CenterP = new Point2D(MidX,MidY);
            double range = circleSize  / 1.8;
            // create a polygon for each pie peice
            foreach (String  s in pointMap.Keys) {
                List<Point2D> points = pointMap[s];
                Polyline polyline = new Polyline();
                polyline.AddPoint(CenterP);
                foreach (Point2D p in points) {
                    Point2D realP = new Point2D((p.X+0.9)* range + startX, (p.Y+ 0.9)* range + startY);
                    polyline.AddPoint(realP);
                }
                polyline.AddPoint(CenterP);
                realPointMap.Add(s,polyline);
            }
            rightSide.SetViewport(width, height, endX);
        }

        public void Paint()
        {
            // first paint all the pie pieces
            GL.ClearColor(Color.White);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.MatrixMode(MatrixMode.Projection);

            int size = degreeMap.Count();
            GL.Viewport(offsetX, offsetY, CurrentViewportSize, CurrentViewportSize);
            GL.PushMatrix();
            {
                for (int i = 0; i < size; i++)
                {
                    String s = degreeMap.Keys.ToList()[i];

                    GL.Begin(PrimitiveType.Polygon);
                    {
                        GL.Color3(form.GetColor(i));
                        GL.Vertex2(0, 0);
                        foreach (Point2D point in pointMap[s])
                        {
                            GL.Vertex2(point.X, point.Y);
                        }
                        GL.Vertex2(0, 0);
                    }
                    GL.End();
                    // change the size for the line and outside circle
                    GL.LineWidth(2);
                    GL.Begin(PrimitiveType.Lines);
                    {
                        // draw the line in the circle
                        GL.Color3(Color.Black);
                        GL.Vertex2(0, 0);
                        GL.Vertex2(pointMap[s][0].X, pointMap[s][0].Y);
                        GL.Vertex2(0, 0);
                        GL.Vertex2(pointMap[s][pointMap[s].Count() - 1].X, pointMap[s][pointMap[s].Count() - 1].Y);
                    }
                    GL.End();


                    GL.Begin(PrimitiveType.LineLoop);
                    {
                        // set the outside black line
                        GL.Color3(Color.Black);
                        Utilities.DrawArc(0, 0, 0.9, 0, (float)(Math.PI * 2));
                    }
                    GL.End();

                    // draw the three circle to show the selected aria
                    foreach (Point2D p in SelectedPoints) {
                        GL.Color3(Color.Peru);
                        GL.Begin(PrimitiveType.Polygon);
                        {
                            Utilities.DrawArc(p.X, p.Y, 0.05, 0, (float)(Math.PI * 2));
                        }
                        GL.End();
                        GL.Color3(Color.Black);
                        GL.Begin(PrimitiveType.Lines);
                        {
                            Utilities.DrawArc(p.X, p.Y, 0.05, 0, (float)(Math.PI * 2));
                        }
                        GL.End();
                    }
                }
            }
            GL.PopMatrix();

            // display the file name
            GL.PushMatrix();
            {
                GL.Scale(20,20,1);
                GL.Color3(form.GetTitleColor());
                GL.Viewport(0, 0, CurrentGlWidth, CurrentGlHeight);
                Utilities.SetWindow(0, CurrentGlWidth,0, CurrentGlHeight);
                //FontGenerator.LoadTexture("Consolas");
                
                TextDrawer.DrawString(FileName, CurrentGlWidth/2- FontGenerator.GetSize(FileName).Width/2 , 
                    CurrentGlHeight- FontGenerator.GetSize(FileName).Height, 1);
                
            }
            GL.PopMatrix();

            GL.PushMatrix();
            {
                GL.LineWidth(1);
                // display right side
                rightSide.Paint();
            }
            GL.PopMatrix();
        }


        public int SetSelect(Point2D clickP)
        {
            int size = realPointMap.Count();
            for(int i = 0; i < size;i++) {
                String s = realPointMap.Keys.ToArray()[i];
                if (inPolygon(realPointMap[s], clickP)) {
                    form.SetFillColor(true);
                    form.SetFormatColor(false);
                    SelectedPoints[0] = new Point2D(0, 0);
                    SelectedPoints[1] = pointMap[s][0];
                    SelectedPoints[2] = pointMap[s][pointMap[s].Count()-1];
                    return i;
                }
            }

            Polyline textRect = new Polyline();
            textRect.AddPoint(new Point2D(CurrentGlWidth / 2 - FontGenerator.GetSize(FileName).Width/2, CurrentGlHeight));
            textRect.AddPoint(new Point2D(CurrentGlWidth / 2 + FontGenerator.GetSize(FileName).Width / 2, 
                CurrentGlHeight- FontGenerator.GetSize(FileName).Height));
            form.SetFormatColor(inRect(textRect,clickP));

            form.SetFillColor(false);

            SelectedPoints[0] = new Point2D(-2, -2);
            SelectedPoints[1] = new Point2D(-2,-2);
            SelectedPoints[2] = new Point2D(-2, -2);
            return 0;
        }

        public bool inPolygon(Polyline polylines, Point2D p)
        {
            Point2D testPoint = p;
            Polyline polygon = polylines;

            {
                bool result = false;
                int j = polygon.Count() - 1;
                for (int i = 0; i < polygon.Count(); i++)
                {
                    if (polygon.points[i].Y < testPoint.Y && polygon.points[j].Y >= testPoint.Y || polygon.points[j].Y < testPoint.Y && polygon.points[i].Y >= testPoint.Y)
                    {
                        if (polygon.points[i].X + (testPoint.Y - polygon.points[i].Y) / (polygon.points[j].Y - polygon.points[i].Y) * (polygon.points[j].X - polygon.points[i].X) < testPoint.X)
                        {
                            result = !result;
                        }
                    }
                    j = i;
                }
                return result;
            }

        }
        private bool inRect(Polyline rect, Point2D p)
        {

            double minX = double.MaxValue;
            double maxX = double.MinValue;
            double minY = double.MaxValue;
            double maxY = double.MinValue;
            foreach (Point2D point in rect.points)
            {
                minX = Math.Min(point.X, minX);
                maxX = Math.Max(point.X, maxX);
                minY = Math.Min(point.Y, minY);
                maxY = Math.Max(point.Y, maxY);
            }
            if (p.X >= minX && p.X <= maxX && p.Y >= minY && p.Y <= maxY)
            {
                return true;
            }

            return false;
        }
    }
}
