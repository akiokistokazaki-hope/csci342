﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using csci342.Text;
using OpenTK.Graphics.OpenGL;
using mygraphicslib;

namespace Chart_Creator
{
    class PieRightSide
    {
        private Form1 form;
        private List<String> Names;
        private int CurrentGlWidth = 0;
        private int CurrentGlHeight = 0;
        private double Ssize;
        private int size;
        private double allowedY;
        private double startP;
        private double endP;
        private double step;
        private double circleEndX;
        private double largestXString=0;

        public PieRightSide(List<String> Names, Form1 form) {
            this.Names = Names;
            this.form = form;
            size = Names.Count();
        }
        public void SetViewport(int Glwidth, int Glheight, double circleEndX) {
            this.circleEndX = circleEndX;
            CurrentGlWidth = Glwidth;
            CurrentGlHeight = Glheight;
            Ssize = FontGenerator.GetSize("S").Height;
            allowedY = size * Ssize * 1.5;
            startP = CurrentGlHeight / 2 + allowedY / 2;
            endP = CurrentGlHeight / 2 - allowedY / 2;
            step = (startP - endP) / size;
        }

            public void Paint() {
            GL.Viewport(0, 0, CurrentGlWidth, CurrentGlHeight);
            Utilities.SetWindow(0, CurrentGlWidth, 0, CurrentGlHeight);
            GL.Color3(Color.Black);
            for (int i = 0;i < size; i++)
            {
                String name = Names[i];
                if (FontGenerator.GetSize(name).Width > largestXString) {
                    largestXString = FontGenerator.GetSize(name).Width;
                }
                GL.Color3(form.GetColor(i));
                TextDrawer.DrawString(name, circleEndX * 1.05+35,
                    startP-(i*step)-step*0.8, 1);
                GL.Begin(PrimitiveType.Polygon);
                {
                    GL.Vertex2(circleEndX * 1.07+15, startP - (i * step) - step * 0.8+ Ssize);
                    GL.Vertex2(circleEndX * 1.07, startP - (i * step) - step * 0.8+ Ssize);
                    GL.Vertex2(circleEndX * 1.07, startP - (i * step) - step * 0.8);
                    GL.Vertex2(circleEndX * 1.07+15, startP - (i * step) - step * 0.8);
                }
                GL.End();

            }

            GL.Color3(Color.Black);
            GL.Begin(PrimitiveType.LineLoop);
            {
                GL.Vertex2(circleEndX*1.05, endP);
                GL.Vertex2(circleEndX * 1.05 + largestXString * 1.4, endP);
                GL.Vertex2(circleEndX * 1.05 + largestXString *1.4, startP);
                GL.Vertex2(circleEndX*1.05, startP);
            }
            GL.End();
            
        }
    }
}

