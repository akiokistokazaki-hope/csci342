﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Chart_Creator;
using System.Windows.Forms;
using System.Text;
using System.Collections.Generic;

namespace ChartTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "";
            ofd.Filter = "TXT files|*.txt";
            ofd.FilterIndex = 1;
            ofd.Title = "Select image to load";
            ofd.RestoreDirectory = true;
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string line;
                StringBuilder fileString = new StringBuilder();

                System.IO.StreamReader file = new System.IO.StreamReader(ofd.FileName);
                line = file.ReadLine();
                if (line != null)
                {
                    if (line == "p")
                    {
                        PieChartState chartState = new PieChartState(file);
                        Dictionary<String, double> test = chartState.test();

                        Assert.AreEqual(90,test["ads"]);
                        Assert.AreEqual(90, test["bb"]);
                        Assert.AreEqual(180, test["rrr"]);

                    }
                }
                

            }
        }
    }
}
