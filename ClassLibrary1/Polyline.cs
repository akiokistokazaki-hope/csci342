﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using csci342;

namespace mygraphicslib
{
    public class Polyline : IEnumerable<Point2D>
    {
        private List<Point2D> points;

        public Polyline()
        {
            points = new List<Point2D>();
        }

        public Polyline(Point2D pt) : this()
        {
            points.Add(pt);

        }

        public void AddPoint(Point2D pt)
        {
            points.Add(pt);
        }

        public void Draw(PrimitiveType par)
        {
            GL.Begin(par);
            foreach(var point in points)
            {
                GL.Vertex2(point.X, point.Y);
            }
            GL.End();
        }

        public IEnumerator<Point2D> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
