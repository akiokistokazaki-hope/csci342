﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using csci342;

namespace mygraphicslib
{

    public class Class1
    {
        public static void SetWindow
            (double left, double right, double bottom, double top)
        {
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(left, right, bottom, top, -1, 1);
        }

        public static void DrawArc(double cx, double cy, float r, float startAngle, float sweep)
        {
            float n = 100;
            float increment = sweep/n;

            float currentAngle = startAngle;

            for (;n>0;n-=-1)
            {
                
                GL.Vertex2(cx, cy);
                currentAngle += increment;
            }
            GL.Vertex2(startAngle, sweep);





        }
    }
}
