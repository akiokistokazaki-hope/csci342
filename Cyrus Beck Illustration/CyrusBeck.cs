﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using OpenTK.Platform;
using mygraphicslib;
using OpenTK.Graphics.OpenGL;

namespace Cyrus_Beck_Illustration
{
    public partial class CyrusBeck : Form
    {
        private bool controlLoaded = false;

        private List<Line> lines;
        private Line clippingLine;
        private double maxEn = 0;
        private double minEx = 1;

        public CyrusBeck()
        {
            InitializeComponent();

            lines = new List<Line>();
            lines.Add(new Line(1,   1,   2, 0.5));
            lines.Add(new Line(2, 0.5,   3,   1));
            lines.Add(new Line(3,   1,   3,   3));
            lines.Add(new Line(3,   3,   1,   3));
            lines.Add(new Line(1,   3,   1,   1));

            clippingLine = new Line(1.5, 0.2, 3.7, 3);

            // Calculate the times
            // (n (dot) (B - A)) / n (dot) c
            // n is normal line to boundary
            // A is a point on the line
            // B is a point on the boundry
            // C is the parameter vector


            // Calculate min and max times
            foreach (Line l in lines)
            {
                // handle n dot c = 0
                if (l.NormalVector.Dot(clippingLine.ParameterVector) == 0)
                {
                    continue;
                }

                double time = calqTime(l.NormalVector, new Vector(clippingLine.Point1), new Vector(l.Point1), clippingLine.ParameterVector);
                bool entering = isEntering(getAngle(clippingLine.ParameterVector, l.NormalVector));

                if (entering && time > maxEn)
                {
                    maxEn = time;
                }
                else if (!entering && time < minEx)
                {
                    minEx = time;
                }
            }

        }

        private double calqTime(Vector n, Vector A, Vector B, Vector c)
        {
            return n.Dot(B - A) * (1 / n.Dot(c));
        }

        private bool isEntering(double angle)
        {
            return angle < Math.PI/2.0;
        }

        private double getAngle(Vector A, Vector B)
        {
            return Math.Acos(A.Dot(B)/(A.Length*B.Length));
        }

        private void glControl_Load(object sender, EventArgs e)
        {
            controlLoaded = true;

            mygraphicslib.Utilities.SetWindow(0, 4, 4, 0);

            GL.ClearColor(Color.White);
        }

        private void glControl_Paint(object sender, PaintEventArgs e)
        {
            if (!controlLoaded) return;

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            // Line
            GL.Color3(Color.Gray);
            GL.Begin(PrimitiveType.LineStrip);
            {
                GL.Vertex2(clippingLine.Point1.X, clippingLine.Point1.Y);
                GL.Vertex2(clippingLine.Point2.X, clippingLine.Point2.Y);

            }
            GL.End();

            GL.Color3(Color.White);
            
            // Box
            GL.Begin(PrimitiveType.Polygon);
            foreach (Line l in lines)
            {
                GL.Vertex2(l.Point1.X, l.Point1.Y);
                GL.Vertex2(l.Point2.X, l.Point2.Y);
            }
            GL.End();

            GL.Color3(Color.Black);

            GL.Begin(PrimitiveType.LineLoop);
            foreach (Line l in lines)
            {
                GL.Vertex2(l.Point1.X, l.Point1.Y);
                GL.Vertex2(l.Point2.X, l.Point2.Y);
            }
            GL.End();

            
            GL.Begin(PrimitiveType.Lines);
            if ((minEx == 1 || minEx < 0) && (maxEn == 0 || maxEn > 1))
            {
                Console.WriteLine("");
            }
            else if (minEx == 1)
            {
                Point2D point = clippingLine.At(maxEn);
                GL.Vertex2(clippingLine.Point2.X, clippingLine.Point2.Y);
                GL.Vertex2(point.X, point.Y);
            }
            else if (maxEn == 0)
            {
                Point2D point = clippingLine.At(minEx);
                GL.Vertex2(clippingLine.Point1.X, clippingLine.Point1.Y);
                GL.Vertex2(point.X, point.Y);
            }
            else if (minEx < maxEn)
            {
                //GL.Vertex2(clippingLine.Point1.X, clippingLine.Point1.Y);
                //GL.Vertex2(clippingLine.Point2.X, clippingLine.Point2.Y);
            }
            else
            {
                Point2D point1 = clippingLine.At(minEx);
                Point2D point2 = clippingLine.At(maxEn);
                GL.Vertex2(point1.X, point1.Y);
                GL.Vertex2(point2.X, point2.Y);
            }
                
            GL.End();
            

            glControl.SwapBuffers();
        }
    }
}
