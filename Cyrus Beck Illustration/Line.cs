﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csci342;
using mygraphicslib;

namespace Cyrus_Beck_Illustration
{
    class Line
    {
        public Vector NormalVector;
        public Point2D Point1;
        public Point2D Point2;
        public Vector ParameterVector;

        public Line(Point2D p1, Point2D p2)
        {
            this.Point1 = p1; // A
            this.Point2 = p2;

            NormalVector = new Vector(p1.Y - p2.Y, p2.X - p1.X);
            //ParameterVector = new Vector(p1.X - p2.X, p1.Y - p2.Y); // C
            ParameterVector = new Vector(p2.X - p1.X, p2.Y - p1.Y); // C
        }

        public Line(double p1x, double p1y, double p2x, double p2y) : this(new Point2D(p1x, p1y), new Point2D(p2x, p2y))
        {
        }

        public Point2D At(double t)
        {
            return Point1 + ParameterVector.ScalarMultiplication(t);
        }
    }
}
