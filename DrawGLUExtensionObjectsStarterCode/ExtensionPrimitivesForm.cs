﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using mygraphicslib;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace DrawGLUExtensionObjects
{
    public partial class ExtensionPrimitivesForm : Form
    {
        private float[] _lightPosition;
        private float[] _ambientIntensity;
        private float[] _diffuseIntensity;

        public ExtensionPrimitivesForm()
        {
            InitializeComponent();
            _lightPosition = new float[4];
            _ambientIntensity = new float[4];
            _diffuseIntensity = new float[4];
        }

        private void canvas_Load(object sender, EventArgs e)
        {
            DrawingTools.enableDefaultAntiAliasing();
            GL.Enable(EnableCap.Lighting);
            GL.Enable(EnableCap.Light0);

            GL.ShadeModel(ShadingModel.Smooth);

            GL.Enable(EnableCap.DepthTest);

            SetLightParametersFromForm();
        }

        private void SetLightParametersFromForm()
        {
            _lightPosition[0] = (float) lightPositionX.Value;
            _lightPosition[1] = (float)lightPositionY.Value;
            _lightPosition[2] = (float)lightPositionZ.Value;
            _lightPosition[3] = 0;

            _ambientIntensity[0] = (float) ambientRed.Value;
            _ambientIntensity[1] = (float) ambientGreen.Value;
            _ambientIntensity[2] = (float) ambientBlue.Value;
            _ambientIntensity[3] = 1;

            _diffuseIntensity[0] = (float) diffuseRed.Value;
            _diffuseIntensity[1] = (float)diffuseGreen.Value;
            _diffuseIntensity[2] = (float)diffuseBlue.Value;
            _diffuseIntensity[3] = 1;
        }

        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            if (!canvas.IsHandleCreated) return;

            GL.ClearColor(Color.White);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GLU.Instance.Perspective(45, canvas.Width * 1.0 / canvas.Height, 1, 20);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            var eye = new Point3D(0, 0, 7.5);
            var look = new Point3D(0, 0, 0);
            GLU.Instance.LookAt(eye, look);
                                   
            GL.Light(LightName.Light0, LightParameter.Position, _lightPosition);
            GL.Light(LightName.Light0, LightParameter.Ambient, _ambientIntensity);
            GL.Light(LightName.Light0, LightParameter.Diffuse, _diffuseIntensity);
            
            

            GL.Enable(EnableCap.Lighting);
            GL.Material(MaterialFace.FrontAndBack, MaterialParameter.AmbientAndDiffuse, Color4.DarkSeaGreen);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.PushMatrix();
            {
                GL.Scale((double) scaleSphereX.Value, (double) scaleSphereY.Value, (double) scaleSphereZ.Value);
                if (btnSphereSolid.Checked)
                {
                    GLU.Instance.SolidSphere((double) radius.Value, (int) verticalDivisions.Value,
                        (int) horizontalDivisions.Value);
                }
                else
                {
                    GLU.Instance.WireSphere((double) radius.Value, (int) verticalDivisions.Value,
                        (int) horizontalDivisions.Value);
                }
            }
            GL.PopMatrix();

            GL.Disable(EnableCap.Lighting);            
            GL.Color3(Color.DarkSeaGreen);

            GL.Translate(2.5, 0, 0);            
            GL.Rotate(-90, 1, 0, 0);

            /*  Signature of WireCylinder is
               public static void WireCylinder(this GLU glu, double baseRadius = 1, double topRadius = 1, double height = 1,
                                               int numSlices = 30, int numStacks = 30)

               This method draws a cylinder oriented along the z axis.  The base of the cylinder
               is placed at z=0 and the top at z=height.  The cylinder is subdivided around the
               z axis into slices, and along the z axis into stacks.
            */
            GLU.Instance.WireCylinder();
                        
            canvas.SwapBuffers();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            SetLightParametersFromForm();
            canvas.Invalidate();
        }

        private void canvas_Resize(object sender, EventArgs e)
        {
            canvas.Invalidate();
        }
    }
}
