﻿namespace HelloWorld
{
    partial class HelloWorldForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.glControl2 = new OpenTK.GLControl();
            this.SuspendLayout();
            // 
            // glControl2
            // 
            this.glControl2.BackColor = System.Drawing.Color.Black;
            this.glControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.glControl2.Location = new System.Drawing.Point(0, 0);
            this.glControl2.Name = "glControl2";
            this.glControl2.Size = new System.Drawing.Size(284, 262);
            this.glControl2.TabIndex = 0;
            this.glControl2.VSync = false;
            this.glControl2.Load += new System.EventHandler(this.glControl1_Load);
            this.glControl2.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl1_Paint);
            this.glControl2.Resize += new System.EventHandler(this.glControl1_Resize);
            // 
            // HelloWorldForm
            // 
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.glControl2);
            this.Name = "HelloWorldForm";
            this.Load += new System.EventHandler(this.HelloWorldForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private OpenTK.GLControl glControl3;
        private OpenTK.GLControl glControl1;
        private OpenTK.GLControl glControl2;
    }
}

