﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using mygraphicslib;
using OpenTK;

namespace Illustrating_Reflections
{
    
    public class LineDrawingEventHandler
    {
        public delegate void LineCompletedHandler(Polyline line);

        public event LineCompletedHandler LineCompleted;

        public Polyline line { get; set; }
        private GLControl glControl;

        public Boolean IsComplete { get; private set; }


        public LineDrawingEventHandler(Polyline _line)
        {
            line = _line;
            IsComplete = false;
        }

        public void Activate(GLControl control)
        {
            glControl = control;
            glControl.MouseClick += SelectFirstEndPointOfLine;

        }

        public void NormalV(GLControl control,  Polyline W)
        {
            glControl = control;
            Point2D vectorA = new Point2D(W.points[1].X - W.points[0].X, W.points[1].Y - W.points[0].Y);

            Point2D vectorN = new Point2D(-1 * vectorA.Y, vectorA.X);

            //double size = Math.Sqrt(vectorN.X * vectorN.X + vectorN.Y * vectorN.Y);
            //Console.Write(size + "\n");
            vectorN = new Point2D(vectorN.X/3, vectorN.Y/3);
            

           double xdis = Math.Sqrt((W.points[0].X - W.points[1].X) * (W.points[0].X - W.points[1].X)) / 2;
           double ydis = Math.Sqrt((W.points[0].Y - W.points[1].Y) * (W.points[0].Y - W.points[1].Y)) / 2;

            double bx;
            double by;
            if(W.points[0].X > W.points[1].X){
               bx = W.points[0].X - xdis;
            }else{
                bx = W.points[1].X - xdis;
            }
            if (W.points[0].Y > W.points[1].Y) {
                by = W.points[0].Y - ydis;
            } else {
                by = W.points[1].Y - ydis;
            }
            Point2D newA = new Point2D(bx,by);
            line.AddPoint(new Point2D(newA.X, newA.Y));
            Point2D endP = new Point2D(newA.X + vectorN.X, newA.Y + vectorN.Y);
            line.AddPoint(endP);


            Point2D vectorT = new Point2D((W.points[0].X - endP.X)/6, (W.points[0].Y - endP.Y)/6);

            line.AddPoint(new Point2D(endP.X + vectorT.X, endP.Y + vectorT.Y));

            Point2D vectorTT = new Point2D((W.points[1].X - endP.X)/6, (W.points[1].Y - endP.Y)/6);

            line.AddPoint(new Point2D(endP.X + vectorTT.X, endP.Y + vectorTT.Y));

            line.AddPoint(endP);


            glControl.Invalidate();
        }

        public void Reflect(GLControl control, Polyline R, Polyline NV)
        {
            glControl = control;
            
            line.AddPoint(new Point2D(R.points[1].X, R.points[1].Y));

            double x;
            double y;

            Point2D v = new Point2D(R.points[1].X - R.points[0].X, R.points[1].Y - R.points[0].Y);
            Point2D n = new Point2D(NV.points[1].X - NV.points[0].X, NV.points[1].Y - NV.points[0].Y);
            double size = Math.Sqrt(n.X * n.X + n.Y * n.Y);
            n = new Point2D(n.X / size, n.Y / size);

            //R=F+2(−F⋅N)N

            Point2D vNeg = new Point2D(-1 * v.X, -1 * v.Y);
            Point2D a = new Point2D(2*n.X*Dot(n,vNeg), 2*n.Y*Dot(n,vNeg));

            Point2D r = new Point2D(v.X + a.X, v.Y + a.Y);

            x = r.X;
            y = r.Y;
            line.AddPoint(new Point2D(R.points[1].X + x, R.points[1].Y + y));


            glControl.Invalidate();
        }

        public double Dot(Point2D v1, Point2D v2) {
            return v1.X * v2.X + v1.Y * v2.Y;
        
        }

        private void SelectFirstEndPointOfLine(object sender, MouseEventArgs e)
        {
            line.AddPoint(new Point2D(e.X, glControl.Height - e.Y));
            glControl.MouseClick -= SelectFirstEndPointOfLine;
            glControl.MouseMove += MouseMotionWhileSettingUpLine;
            glControl.Invalidate();
        }

        private void MouseMotionWhileSettingUpLine(object sender, MouseEventArgs e)
        {
            if (line.Count() == 2)
            {
                line.RemoveLast();
            }
            else
            {
                //  First time we've moved the mouse after the first click, so allow the second click to occur
                glControl.MouseClick += SelectLastPointOfLine;
            }
            line.AddPoint(new Point2D(e.X, glControl.Height - e.Y));
            glControl.Invalidate();
        }

        private void SelectLastPointOfLine(object sender, MouseEventArgs e)
        {
            line.RemoveLast();
            line.AddPoint(new Point2D(e.X, glControl.Height - e.Y));

            
            glControl.MouseClick -= SelectLastPointOfLine;
            glControl.MouseMove -= MouseMotionWhileSettingUpLine;
            
            IsComplete = true;
            if (LineCompleted != null)
            {
                LineCompleted(line);
            }
            

            glControl.Invalidate();
        }
    }
}
