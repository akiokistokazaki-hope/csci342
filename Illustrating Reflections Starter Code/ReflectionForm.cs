﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using mygraphicslib;
using OpenTK.Graphics.OpenGL;
using OpenTK.Platform;

namespace Illustrating_Reflections
{
    public partial class ReflectionForm : Form
    {

        private Polyline LineToReflect { get; set; }
        private Polyline R { get; set; }
        private Polyline NV { get; set; }
        private Polyline Red { get; set; }

        private readonly LineDrawingEventHandler _recordFirstLineHandler;
        private readonly LineDrawingEventHandler _recordRHandler;
        private readonly LineDrawingEventHandler _recordNVHandler;
        private readonly LineDrawingEventHandler _recordRedHandler;

        public ReflectionForm()
        {
            InitializeComponent();
            

            LineToReflect = new Polyline();
            R = new Polyline();
            NV = new Polyline();
            Red = new Polyline();

            _recordFirstLineHandler = new LineDrawingEventHandler(LineToReflect);
            _recordRHandler = new LineDrawingEventHandler(R);
            _recordNVHandler = new LineDrawingEventHandler(NV);
            _recordRedHandler = new LineDrawingEventHandler(Red);

            _recordFirstLineHandler.Activate(canvas);
            _recordFirstLineHandler.LineCompleted += delegate(Polyline line)
            {
                _recordNVHandler.NormalV(canvas, LineToReflect);
                _recordRHandler.Activate(canvas);
            };

            _recordRHandler.LineCompleted += delegate(Polyline line) {
                _recordRedHandler.Reflect(canvas, R,NV);
            };
            
        }

        private void canvas_Resize(object sender, EventArgs e)
        {
            GL.Viewport(0, 0, canvas.Width, canvas.Height);
        }

        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            if (!canvas.IsHandleCreated) return;

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.ClearColor(Color.White);
            GL.MatrixMode(MatrixMode.Projection);


            GL.Color3(Color.Black);

            if (LineToReflect.Count() == 2)
            {
                LineToReflect.Draw(PrimitiveType.Lines);
            }

            GL.Color3(Color.Blue);

            if (R.Count() == 2)
            {
                R.Draw(PrimitiveType.Lines);
            }

            GL.Color3(Color.Red);
            if (NV.Count() > 1)
            {
                NV.Draw(PrimitiveType.LineStrip);
            }

            GL.Color3(Color.Green);
            if (Red.Count() == 2)
            {
                Red.Draw(PrimitiveType.Lines);
            }
            canvas.SwapBuffers();
        }

        private void canvas_Load(object sender, EventArgs e)
        {
            if (!canvas.IsHandleCreated) return;

            mygraphicslib.Utilities.SetWindow(0, canvas.Width, 0, canvas.Height);

            GL.ClearColor(Color.White);

            DrawingTools.enableDefaultAntiAliasing();
        }

        private void canvas_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
    }
}
