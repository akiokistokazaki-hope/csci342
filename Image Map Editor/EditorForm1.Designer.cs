﻿namespace Image_Map_Editor
{
    partial class EditorForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.glControl1 = new OpenTK.GLControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loeadHtmlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveHtmlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rectangleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.circleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.polygonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyMApToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveMApToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteVertexToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.moveVertexToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.addMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setHrefToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setAltToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // glControl1
            // 
            this.glControl1.BackColor = System.Drawing.Color.Black;
            this.glControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.glControl1.Location = new System.Drawing.Point(0, 24);
            this.glControl1.Name = "glControl1";
            this.glControl1.Size = new System.Drawing.Size(430, 428);
            this.glControl1.TabIndex = 0;
            this.glControl1.VSync = false;
            this.glControl1.Load += new System.EventHandler(this.glControl1_Load);
            this.glControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl1_Paint);
            this.glControl1.Resize += new System.EventHandler(this.glControl1_Resize);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileFToolStripMenuItem,
            this.addNapToolStripMenuItem,
            this.modifyMApToolStripMenuItem,
            this.addMapToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(430, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileFToolStripMenuItem
            // 
            this.fileFToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.loeadHtmlToolStripMenuItem,
            this.saveHtmlToolStripMenuItem});
            this.fileFToolStripMenuItem.Name = "fileFToolStripMenuItem";
            this.fileFToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.fileFToolStripMenuItem.Text = "File(&F)";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.loadToolStripMenuItem.Text = "Load image";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // loeadHtmlToolStripMenuItem
            // 
            this.loeadHtmlToolStripMenuItem.Name = "loeadHtmlToolStripMenuItem";
            this.loeadHtmlToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.loeadHtmlToolStripMenuItem.Text = "Loead html";
            this.loeadHtmlToolStripMenuItem.Click += new System.EventHandler(this.loeadHtmlToolStripMenuItem_Click);
            // 
            // saveHtmlToolStripMenuItem
            // 
            this.saveHtmlToolStripMenuItem.Name = "saveHtmlToolStripMenuItem";
            this.saveHtmlToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.saveHtmlToolStripMenuItem.Text = "Save html";
            this.saveHtmlToolStripMenuItem.Click += new System.EventHandler(this.saveHtmlToolStripMenuItem_Click);
            // 
            // addNapToolStripMenuItem
            // 
            this.addNapToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rectangleToolStripMenuItem,
            this.circleToolStripMenuItem,
            this.polygonToolStripMenuItem});
            this.addNapToolStripMenuItem.Name = "addNapToolStripMenuItem";
            this.addNapToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.addNapToolStripMenuItem.Text = "Add Shape";
            // 
            // rectangleToolStripMenuItem
            // 
            this.rectangleToolStripMenuItem.Name = "rectangleToolStripMenuItem";
            this.rectangleToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.rectangleToolStripMenuItem.Text = "Rectangle";
            this.rectangleToolStripMenuItem.Click += new System.EventHandler(this.rectangleToolStripMenuItem_Click);
            // 
            // circleToolStripMenuItem
            // 
            this.circleToolStripMenuItem.Name = "circleToolStripMenuItem";
            this.circleToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.circleToolStripMenuItem.Text = "Circle";
            this.circleToolStripMenuItem.Click += new System.EventHandler(this.circleToolStripMenuItem_Click);
            // 
            // polygonToolStripMenuItem
            // 
            this.polygonToolStripMenuItem.Name = "polygonToolStripMenuItem";
            this.polygonToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.polygonToolStripMenuItem.Text = "Polygon";
            this.polygonToolStripMenuItem.Click += new System.EventHandler(this.polygonToolStripMenuItem_Click);
            // 
            // modifyMApToolStripMenuItem
            // 
            this.modifyMApToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeMapToolStripMenuItem,
            this.moveMApToolStripMenuItem,
            this.deleteVertexToolStripMenuItem1,
            this.moveVertexToolStripMenuItem1});
            this.modifyMApToolStripMenuItem.Name = "modifyMApToolStripMenuItem";
            this.modifyMApToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
            this.modifyMApToolStripMenuItem.Text = "Modify Shape";
            // 
            // removeMapToolStripMenuItem
            // 
            this.removeMapToolStripMenuItem.Name = "removeMapToolStripMenuItem";
            this.removeMapToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.removeMapToolStripMenuItem.Text = "Delete Shape";
            this.removeMapToolStripMenuItem.Click += new System.EventHandler(this.removeMapToolStripMenuItem_Click);
            // 
            // moveMApToolStripMenuItem
            // 
            this.moveMApToolStripMenuItem.Name = "moveMApToolStripMenuItem";
            this.moveMApToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.moveMApToolStripMenuItem.Text = "Move Shape";
            this.moveMApToolStripMenuItem.Click += new System.EventHandler(this.moveMApToolStripMenuItem_Click);
            // 
            // deleteVertexToolStripMenuItem1
            // 
            this.deleteVertexToolStripMenuItem1.Name = "deleteVertexToolStripMenuItem1";
            this.deleteVertexToolStripMenuItem1.Size = new System.Drawing.Size(154, 22);
            this.deleteVertexToolStripMenuItem1.Text = "Delete Vertex";
            this.deleteVertexToolStripMenuItem1.Click += new System.EventHandler(this.deleteVertexToolStripMenuItem1_Click);
            // 
            // moveVertexToolStripMenuItem1
            // 
            this.moveVertexToolStripMenuItem1.Name = "moveVertexToolStripMenuItem1";
            this.moveVertexToolStripMenuItem1.Size = new System.Drawing.Size(154, 22);
            this.moveVertexToolStripMenuItem1.Text = "Move Vertex";
            this.moveVertexToolStripMenuItem1.Click += new System.EventHandler(this.moveVertexToolStripMenuItem1_Click);
            // 
            // addMapToolStripMenuItem
            // 
            this.addMapToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setHrefToolStripMenuItem,
            this.setAltToolStripMenuItem});
            this.addMapToolStripMenuItem.Name = "addMapToolStripMenuItem";
            this.addMapToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.addMapToolStripMenuItem.Text = "Add Map";
            this.addMapToolStripMenuItem.Click += new System.EventHandler(this.addMapToolStripMenuItem_Click);
            // 
            // setHrefToolStripMenuItem
            // 
            this.setHrefToolStripMenuItem.Name = "setHrefToolStripMenuItem";
            this.setHrefToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.setHrefToolStripMenuItem.Text = "Set Href";
            this.setHrefToolStripMenuItem.Click += new System.EventHandler(this.setHrefToolStripMenuItem_Click);
            // 
            // setAltToolStripMenuItem
            // 
            this.setAltToolStripMenuItem.Name = "setAltToolStripMenuItem";
            this.setAltToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.setAltToolStripMenuItem.Text = "Set Alt";
            this.setAltToolStripMenuItem.Click += new System.EventHandler(this.setAltToolStripMenuItem_Click);
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(316, 5);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(112, 19);
            this.textBox.TabIndex = 2;
            // 
            // EditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 452);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.glControl1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "EditorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Editor";
            this.Load += new System.EventHandler(this.EditorForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private OpenTK.GLControl glControl1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rectangleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem circleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem polygonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loeadHtmlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveHtmlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyMApToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveMApToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteVertexToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem moveVertexToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem addMapToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.ToolStripMenuItem setHrefToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setAltToolStripMenuItem;
    }
}

