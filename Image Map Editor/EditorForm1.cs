﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;
using csci342;
using mygraphicslib;
using OpenTK;
using AngleSharp;
using AngleSharp.Parser.Html;
using System.IO;

namespace Image_Map_Editor
{
    public partial class EditorForm : Form

    {
        TwoPointHandler rectHandler = new TwoPointHandler();
        TwoPointHandler circleHandler = new TwoPointHandler();
        PolygonHandler polygonHandler = new PolygonHandler();
        MoveHandler moveHandler = new MoveHandler();
        MoveVertexHandler moveVertexHandler = new MoveVertexHandler();

        public static double MenuY = 24;// ずれ修正
        private PictureDrawer pic;
        private OpenFileDialog ofd = new OpenFileDialog();
        private SaveFileDialog sfd = new SaveFileDialog();

        private int DefaultWidth = 446;
        public static double PointSize = 10.0;
        public static int VertexIndex = 0;

        private String fileName;
        private String loadDirectory;
        private bool LoadSuccess = false;
        
        private PolylineCollection rectLines = new PolylineCollection();
        private PolylineCollection circleLines = new PolylineCollection();
        public static PolylineCollection polygonLines = new PolylineCollection(new Point2D(0,0));

        private Dictionary<Polyline, String> shapeToHref = new Dictionary<Polyline, string>();
        private Dictionary<Polyline, String> shapeToAlt = new Dictionary<Polyline, string>();

        public EditorForm()
        {
            InitializeComponent();
            LoadImage();
        }

        public EditorForm(String[] args)
        {
            InitializeComponent();
            if (File.Exists(args[0]))
            {
                LoadImageString(args[0]);
            }
            else {
                LoadImage();
            }
        }

        private void glControl1_Load(object sender, EventArgs e)
        {
            if (!glControl1.IsHandleCreated) return;
            GL.ClearColor(Color.White);
            mygraphicslib.Utilities.SetWindow(0, glControl1.Width, 0, glControl1.Height);
            DrawingTools.enableDefaultAntiAliasing();
        }

        private void glControl1_Resize(object sender, EventArgs e)
        {
        }
        

        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            if (LoadSuccess) {
                GL.ClearColor(Color.Gray);
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
                //this.SetClientSizeCore(pic.Width, pic.Height + 24); // menustrip's heith is 24 
                GL.Viewport(0, 0, pic.Width, pic.Height);
                mygraphicslib.Utilities.SetWindow(0, pic.Width, 0, pic.Height - MenuY);
                pic.display();

                if (!glControl1.IsHandleCreated) {
                    return;
                }



                GL.Color3(Color.Blue);

                if (rectLines.polylines.Count > 0)
                {
                    foreach (var line in rectLines.polylines)
                    {
                        if (line.Count() > 1) {
                            line.DrawRectFromTwoPoints(PrimitiveType.Lines);
                            line.DrawRectPointFromTwoPoints(PrimitiveType.Lines);
                        }
                    }
                }

                if (circleLines.polylines.Count > 0)
                {
                    foreach (var line in circleLines.polylines)
                    {
                        if (line.Count() == 2)
                        {
                            line.DrawCircleFromTwoPoints(PrimitiveType.Lines);
                            line.DrawCirclePointFromTwoPoints(PrimitiveType.Lines);
                        }
                    }
                }

                int last = polygonLines.polylines.Count() - 1;

                for (int i = 0; i < last; i++) {
                    if (polygonLines.polylines[i].Count() > 1)
                    {
                        polygonLines.polylines[i].DrawPolygonLine(true);
                        polygonLines.polylines[i].DrawPolygonPoint(true);
                    }
                }
                polygonLines.polylines[last].DrawPolygonLine(false);
                polygonLines.polylines[last].DrawPolygonPoint(false);

                glControl1.SwapBuffers();
            }
        }

        private void EditorForm_Load(object sender, EventArgs e)
        {

        }

        private void File_Opening(object sender, CancelEventArgs e)
        {

        }

        private void resetMouse()
        {
            rectHandler.resetMouse(glControl1);
            circleHandler.resetMouse(glControl1);
            polygonHandler.resetMouse(glControl1);
            moveHandler.resetMouse(glControl1);
            moveVertexHandler.resetMouse(glControl1);
            glControl1.MouseClick -= getMoveMousePoint;
            glControl1.MouseClick -= getVertexMousePoint;
            glControl1.MouseClick -= LoadAltToShape;
            glControl1.MouseClick -= LoadHrefToShape;
            removeEmptyLine();
        }

        private void removeEmptyLine()
        {
            if (rectLines.polylines.Count > 0)
            {
                for(int i = 0; i < rectLines.polylines.Count() - 1; i++)
                {
                    if (rectLines.polylines[i].Count() < 2)
                    {
                        rectLines.polylines.RemoveAt(i);
                        i--;
                    }
                }
            }

            if (circleLines.polylines.Count > 0)
            {
                for (int i = 0; i < circleLines.polylines.Count() - 1; i++)
                {
                    if (circleLines.polylines[i].Count() < 2)
                    {
                        circleLines.polylines.RemoveAt(i);
                        i--;
                    }
                }
            }
            

            for (int i = 0; i < polygonLines.polylines.Count() - 1; i++)
            {
                if (polygonLines.polylines[i].Count() < 3)
                {
                    polygonLines.polylines.RemoveAt(i);
                    i--;
                }
            }
        }

        private void rectangleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rectLines.AddPolyline(new Polyline());
            resetMouse();
            rectHandler = new TwoPointHandler(rectLines.polylines[rectLines.polylines.Count - 1]);
            
            rectHandler.Activate(glControl1);
        }

        

        private void circleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            circleLines.AddPolyline(new Polyline());
            resetMouse();
            circleHandler = new TwoPointHandler(circleLines.polylines[circleLines.polylines.Count - 1]);
            circleHandler.Activate(glControl1);
        }

        private void polygonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resetMouse();
            polygonHandler = new PolygonHandler(polygonLines.polylines[polygonLines.polylines.Count - 1]);
            polygonHandler.Activate(glControl1);
        }

        private void LoadImageString(String fileDirec) {
            ofd.FileName = fileDirec;
            LoadSuccess = true;
            loadDirectory = "";
            String[] stt = fileDirec.Split('.');
            int last = stt.Count() - 2;
            for (int i = 0; i < last; i++)
            {
                loadDirectory += stt[i] + '.';
            }
            loadDirectory += stt[last];
            ofd.InitialDirectory = loadDirectory;
            sfd.InitialDirectory = loadDirectory;

            pic = new PictureDrawer(fileDirec);
            if (pic.Width > DefaultWidth)
            {
                this.MinimumSize = new System.Drawing.Size(pic.Width, pic.Height);
                this.MaximumSize = new System.Drawing.Size(pic.Width+60, pic.Height + 60);
                this.FormBorderStyle = FormBorderStyle.FixedDialog;
            }
            else {
                this.MinimumSize = new System.Drawing.Size(446, pic.Height);
                this.MaximumSize = new System.Drawing.Size(446, pic.Height + 60);
                this.FormBorderStyle = FormBorderStyle.Sizable;
            }

            string[] namest = fileDirec.Split(@"\"[0]);
            int lastN = namest.Count() - 1;
            string[] namestt = namest[lastN].Split('.');
            fileName = namestt[0];
            this.SetClientSizeCore(pic.Width, pic.Height + (int)MenuY);
            rectLines = new PolylineCollection();
            circleLines = new PolylineCollection();
            polygonLines = new PolylineCollection();
            polygonLines.AddPolyline(new Polyline());
            //this.FormBorderStyle = FormBorderStyle.FixedDialog;
            if (File.Exists(loadDirectory + ".html"))
            {
                ofd.FileName = loadDirectory + ".html";
                loadHTML();
            }
        }

        private void LoadImage() {
            ofd.FileName = "";
            ofd.Filter = "ImageFile(*.bmp,*.jpg,*.png,*.tif,*.ico)| *.bmp; *.jpg; *.png; *.tif; *.ico";
            ofd.FilterIndex = 1;
            ofd.Title = "Select image to load";
            ofd.RestoreDirectory = true;
            sfd.RestoreDirectory = true;
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                LoadImageString(ofd.FileName);
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadImage();
            
        }
        private void loadHTML()
        {
            rectLines = new PolylineCollection();
            circleLines = new PolylineCollection();
            polygonLines = new PolylineCollection(new Point2D(0, 0));
               
            bool hadPolygon = false;
            var parser = new HtmlParser();
            using (FileStream htmlSource = new FileStream(ofd.FileName, FileMode.Open))
            {
                var document = parser.Parse(htmlSource);
                var areas = document.QuerySelectorAll("area");
                foreach (var area in areas)
                {
                    var shapes = area.GetAttribute("shape");
                    var cordinates = area.GetAttribute("coords");
                    var hrefs = area.GetAttribute("href");
                    var alts = area.GetAttribute("alt");

                    String[] shapesL = shapes.Split('\n');
                    String[] cordinatesL = cordinates.Split('\n');
                    String[] hrefsL = hrefs.Split('\n');
                    String[] altsL = alts.Split('\n');

                    double[] cordinate;
                    for (int i = 0; i < shapesL.Length; i++)
                    {
                        Polyline currentPoly = new Polyline();
                        if (shapesL[i].Equals("rect"))
                        {
                            cordinate = Array.ConvertAll(cordinatesL[i].Split(','), Double.Parse);
                            cordinate[1] = glControl1.Height - cordinate[1] - (MenuY / glControl1.Height * (glControl1.Height - cordinate[1]));
                            cordinate[3] = glControl1.Height - cordinate[3] - (MenuY / glControl1.Height * (glControl1.Height - cordinate[3]));
                            currentPoly = new Polyline(cordinate);
                            rectLines.AddPolyline(currentPoly);
                        }
                        else if (shapesL[i].Equals("circle"))
                        {
                            cordinate = Array.ConvertAll(cordinatesL[i].Split(','), Double.Parse);
                            cordinate[1] = glControl1.Height - cordinate[1] - (MenuY / glControl1.Height * (glControl1.Height - cordinate[1]));
                            currentPoly = new Polyline(cordinate[0] - cordinate[2], cordinate[1], cordinate[0] + cordinate[2], cordinate[1]);
                            circleLines.AddPolyline(currentPoly);
                        }
                        else if (shapesL[i].Equals("poly"))
                        {
                            hadPolygon = true;
                            cordinate = Array.ConvertAll(cordinatesL[i].Split(','), Double.Parse);
                            for (int odd = 1; odd < cordinate.Length; odd = odd + 2)
                            {
                                cordinate[odd] = glControl1.Height - cordinate[odd] - (MenuY / glControl1.Height * (glControl1.Height - cordinate[odd]));
                            }
                            currentPoly = new Polyline(cordinate);
                            polygonLines.AddPolyline(currentPoly);
                        }

                        shapeToHref.Add(currentPoly, hrefsL[i]);
                        shapeToAlt.Add(currentPoly, altsL[i]);
                    }
                }
                htmlSource.Close();
            if (hadPolygon) { polygonLines.AddPolyline(new Polyline()); }
        } 
            glControl1.Invalidate(); 
        }
        private void loeadHtmlToolStripMenuItem_Click(object sender, EventArgs e)
        {

            ofd.FileName = "";
            ofd.Filter = "HTMLFile(*.html)|*.html;";
            ofd.Title = "Select HTML file to load";
            ofd.RestoreDirectory = true;
            sfd.RestoreDirectory = true;
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                loadHTML();
            }
        }

        private void saveHtmlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            
            sfd.FileName = fileName + ".html";
            sfd.InitialDirectory = @"C:\";
            sfd.Filter = "HTML(*.html;*.htm)|*.html;*.htm";
            sfd.FilterIndex = 2;
            sfd.Title = "Select the location for save";
            sfd.RestoreDirectory = true;
            sfd.OverwritePrompt = true;
            sfd.CheckPathExists = true;
            
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                System.IO.Stream stream;
                stream = sfd.OpenFile();
                if (stream != null)
                {
                    
                    //write things
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(stream);
                    
                    //<area shape="rect" coords="22,11,122,62" href="map1.html" alt="リンク1">
                    foreach (var shape in rectLines.polylines) {
                        if(shape.points.Count() > 1){
                            double minX = double.MaxValue;
                            double maxX = double.MinValue;
                            double minY = double.MaxValue;
                            double maxY = double.MinValue;
                            for (int i =0; i<2 ;i++)
                            {
                                minX = Math.Min(shape.points[i].X, minX);
                                maxX = Math.Max(shape.points[i].X, maxX);
                                minY = Math.Min(shape.points[i].Y, minY);
                                maxY = Math.Max(shape.points[i].Y, maxY);
                            }
                            sw.Write("<area shape=\"rect\" coords=\"");
                            sw.Write((int)minX);
                            sw.Write(",");
                            sw.Write((int)((glControl1.Height + maxY - maxY * 2) - maxY * (MenuY/glControl1.Height)));
                            sw.Write(",");
                            sw.Write((int)maxX);
                            sw.Write(",");
                            sw.Write((int)((glControl1.Height + minY - minY * 2) - minY * (MenuY / glControl1.Height)));
                            sw.Write("\" href = \"");
                            if (shapeToHref.ContainsKey(shape)) {
                                sw.Write(shapeToHref[shape]);
                            }
                            sw.Write("\" alt = \"");
                            if (shapeToAlt.ContainsKey(shape))
                            {
                                sw.Write(shapeToAlt[shape]);
                            }
                            sw.Write("\">\n");
                        }
                    }
                    //<area shape="circle" coords="184,86,30" href="map2.html" alt="リンク2">
                    foreach (var shape in circleLines.polylines)
                    {
                        if (shape.points.Count() > 1)
                        {
                            sw.Write("<area shape=\"circle\" coords=\"");
                            sw.Write((int)shape.GetMidPoint().X);
                            sw.Write(",");
                            sw.Write((int)((glControl1.Height + shape.GetMidPoint().Y - shape.GetMidPoint().Y * 2) - shape.GetMidPoint().Y * (MenuY / glControl1.Height)));
                            sw.Write(",");
                            sw.Write((int)shape.GetRadius());
                            sw.Write("\" href = \"");
                            if (shapeToHref.ContainsKey(shape))
                            {
                                sw.Write(shapeToHref[shape]);
                            }
                            sw.Write("\" alt = \"");
                            if (shapeToAlt.ContainsKey(shape))
                            {
                                sw.Write(shapeToAlt[shape]);
                            }
                            sw.Write("\">\n");
                        }
                    }
                    //< area shape = "poly" coords = "87,78,30,110,81,139,69,113" href = "map3.html" alt = "リンク3" >
                    foreach (var shape in polygonLines.polylines)
                    {
                        if (shape.points.Count() > 2)
                        {
                            sw.Write("<area shape=\"poly\" coords=\"");
                            sw.Write((int)shape.points[0].X);
                            sw.Write(",");
                            sw.Write((int)((glControl1.Height + shape.points[0].Y - shape.points[0].Y * 2) - shape.points[0].Y * (MenuY / glControl1.Height)));
                            for (int i=1; i < shape.points.Count()  ;i++) {
                                sw.Write(",");
                                sw.Write((int)shape.points[i].X);
                                sw.Write(",");
                                sw.Write((int)((glControl1.Height + shape.points[i].Y - shape.points[i].Y * 2) - shape.points[i].Y * (MenuY / glControl1.Height)));
                            }
                            sw.Write("\" href = \"");
                            if (shapeToHref.ContainsKey(shape))
                            {
                                sw.Write(shapeToHref[shape]);
                            }
                            sw.Write("\" alt = \"");
                            if (shapeToAlt.ContainsKey(shape))
                            {
                                sw.Write(shapeToAlt[shape]);
                            }
                            sw.Write("\">\n");
                        }
                    }
                    //close
                    sw.Close();
                    stream.Close();
                }
            }
        }



        private void removeMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resetMouse();
            glControl1.MouseClick += getMoveMousePoint;
        }

        private void getMoveMousePoint(object sender, MouseEventArgs e)
        {
            Point2D mouseP = new Point2D(e.X, glControl1.Height - e.Y - MenuY / glControl1.Height * (glControl1.Height - e.Y));
            glControl1.MouseClick -= getMoveMousePoint;
            removeShape(mouseP);
        }

        private void removeShape(Point2D mouseP)
        {
            for (int i =0; i < rectLines.polylines.Count; i++) {
                if (inRect(rectLines.polylines[i], mouseP))
                {
                    rectLines.polylines.RemoveAt(i);
                    i--;
                }
            }
            for (int i = 0; i < circleLines.polylines.Count; i++)
            {
                if (inCircle(circleLines.polylines[i], mouseP))
                {
                    circleLines.polylines.RemoveAt(i);
                    i--;
                }
            }
            for (int i = 0; i < polygonLines.polylines.Count; i++)
            {
                if (inPolygon(polygonLines.polylines[i], mouseP))
                {
                    polygonLines.polylines.RemoveAt(i);
                    i--;
                }
            }
            glControl1.Invalidate();
        }

        private bool inRect(Polyline rect, Point2D p) {

            double minX = double.MaxValue;
            double maxX = double.MinValue;
            double minY = double.MaxValue;
            double maxY = double.MinValue;
            foreach (Point2D point in rect.points) {
                minX = Math.Min(point.X, minX);
                maxX = Math.Max(point.X, maxX);
                minY = Math.Min(point.Y, minY);
                maxY = Math.Max(point.Y, maxY);
            }
            if (p.X >= minX && p.X <= maxX && p.Y >= minY && p.Y <= maxY) {
                return true;
            }

            return false;
        }

        private bool inCircle(Polyline circle, Point2D p)
        {
            if (circle.points.Count() > 1)
            {
                double r = circle.GetRadius();
                Point2D center = circle.GetMidPoint();
                if ((center.X - p.X) * (center.X - p.X) + (center.Y - p.Y) * (center.Y - p.Y) <= r * r)
                {
                    return true;
                }
            }
            return false;
        }




        public bool inPolygon(Polyline polylines, Point2D p)
        {
            Point2D testPoint = p;
            Polyline polygon = polylines;

            {
                bool result = false;
                int j = polygon.Count() - 1;
                for (int i = 0; i < polygon.Count(); i++)
                {
                    if (polygon.points[i].Y < testPoint.Y && polygon.points[j].Y >= testPoint.Y || polygon.points[j].Y < testPoint.Y && polygon.points[i].Y >= testPoint.Y)
                    {
                        if (polygon.points[i].X + (testPoint.Y - polygon.points[i].Y) / (polygon.points[j].Y - polygon.points[i].Y) * (polygon.points[j].X - polygon.points[i].X) < testPoint.X)
                        {
                            result = !result;
                        }
                    }
                    j = i;
                }
                return result;
            }

        }


        

        private void moveMApToolStripMenuItem_Click(object sender, EventArgs e)
        {
            moveHandler = new MoveHandler();
            resetMouse();
            moveHandler.Activate(glControl1,this);
        }
        

        public Polyline GetShape(Point2D mouseP)
        {
            for (int i = 0; i < rectLines.polylines.Count; i++)
            {
                    if (inRect(rectLines.polylines[i], mouseP))
                    {
                        return rectLines.polylines[i];
                    }
            }
            for (int i = 0; i < circleLines.polylines.Count; i++)
            {
                    if (inCircle(circleLines.polylines[i], mouseP))
                    {
                        return circleLines.polylines[i];
                    }
            }
            for (int i = 0; i < polygonLines.polylines.Count; i++)
            {
                if (inPolygon(polygonLines.polylines[i], mouseP))
                {
                    return polygonLines.polylines[i];
                }
            }
            return null;
        }

        private void deleteVertexToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            resetMouse();
            glControl1.MouseClick += getVertexMousePoint;
        }

        private void getVertexMousePoint(object sender, MouseEventArgs e)
        {
            Point2D mouseP = new Point2D(e.X, glControl1.Height - e.Y - MenuY / glControl1.Height * (glControl1.Height - e.Y));
            glControl1.MouseClick -= getVertexMousePoint;
            removeVertex(mouseP);
        }

        private void removeVertex(Point2D mouseP)
        {
            for (int i = 0; i < polygonLines.polylines.Count; i++)
            {
                for (int p = 0; p < polygonLines.polylines[i].points.Count(); p++)
                {
                    if (inVertex(polygonLines.polylines[i].points[p], mouseP))
                    {
                        if (polygonLines.polylines[i].points.Count() < 4) {
                            polygonLines.polylines.RemoveAt(i);
                            }
                        else { 
                        polygonLines.polylines[i].points.RemoveAt(p);
                        }
                        glControl1.Invalidate();
                        return;
                    }
                }
            }

            for (int i = 0; i < rectLines.polylines.Count; i++) {
                rectLines.polylines[i].SetFourPoints();
                for (int p = 0; p < rectLines.polylines[i].points.Count(); p++)
                {
                    if (inVertex(rectLines.polylines[i].points[p], mouseP))
                    {
                        rectLines.polylines[i].points.RemoveAt(p);
                        polygonLines.AddPolyline(rectLines.polylines[i]);
                        polygonLines.AddPolyline(new Polyline());
                        polygonLines.AddPolyline(new Polyline());
                        rectLines.polylines.RemoveAt(i);
                        glControl1.Invalidate();
                        return;
                    }
                }

            }

            for (int i = 0; i < circleLines.polylines.Count; i++)
            {
                if (circleLines.polylines[i].Count() > 0)
                {
                    var sav0 = circleLines.polylines[i].points[0];
                    var sav1 = circleLines.polylines[i].points[1];
                    circleLines.polylines[i].Set18Points();
                    for (int p = 0; p < circleLines.polylines[i].points.Count(); p++)
                    {
                        if (inVertex(circleLines.polylines[i].points[p], mouseP))
                        {
                            circleLines.polylines[i].points.RemoveAt(p);
                            polygonLines.AddPolyline(circleLines.polylines[i]);
                            polygonLines.AddPolyline(new Polyline());
                            circleLines.polylines.RemoveAt(i);
                            glControl1.Invalidate();
                            return;
                        }
                    }
                    circleLines.polylines[i] = new Polyline(sav0.X, sav0.Y, sav1.X, sav1.Y);
                }
            }
        }

        private bool inVertex(Point2D p, Point2D mouseP)
        {
            Polyline pointsRect = new Polyline();
            pointsRect.AddPoint(new Point2D(p.X - PointSize / 2, p.Y + PointSize / 2));
            pointsRect.AddPoint(new Point2D(p.X+PointSize/2,p.Y-PointSize/2));
            return inRect(pointsRect ,mouseP);
        }

        private void moveVertexToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            moveVertexHandler = new MoveVertexHandler();
            resetMouse();
            moveVertexHandler .Activate(glControl1, this);
        }

        public Polyline GetVertex(Point2D mouseP)
        {
            for (int i = 0; i < polygonLines.polylines.Count; i++)
            {
                for (int p = 0; p < polygonLines.polylines[i].points.Count(); p++)
                {
                    if (inVertex(polygonLines.polylines[i].points[p], mouseP))
                    {
                        VertexIndex = p;
                        return polygonLines.polylines[i];
                    }
                }
            }

            for (int i = 0; i < rectLines.polylines.Count; i++)
            {
                rectLines.polylines[i].SetFourPoints();
                for (int p = 0; p < rectLines.polylines[i].points.Count(); p++)
                {
                    if (inVertex(rectLines.polylines[i].points[p], mouseP))
                    {
                        VertexIndex = p;
                        Point2D now = rectLines.polylines[i].points[1];
                        rectLines.polylines[i].points[1] = rectLines.polylines[i].points[2];
                        rectLines.polylines[i].points[2] = now;
                        if (VertexIndex % 3 !=0) {
                            VertexIndex = VertexIndex % 2 + 1;
                        }
                        
                        polygonLines.AddPolyline(rectLines.polylines[i]);
                        Polyline rLine = rectLines.polylines[i];
                        polygonLines.AddPolyline(new Polyline());
                        polygonLines.AddPolyline(new Polyline());
                        rectLines.polylines.RemoveAt(i);
                        glControl1.Invalidate();

                        return rLine;
                    }
                }

            }

            for (int i = 0; i < circleLines.polylines.Count; i++)
            {
                if (circleLines.polylines[i].Count()>0) {
                    var sav0 = circleLines.polylines[i].points[0];
                    var sav1 = circleLines.polylines[i].points[1];
                    circleLines.polylines[i].Set18Points();
                    for (int p = 0; p < circleLines.polylines[i].points.Count(); p++)
                    {
                        if (inVertex(circleLines.polylines[i].points[p], mouseP))
                        {
                            VertexIndex = p;

                            polygonLines.AddPolyline(circleLines.polylines[i]);
                            Polyline rLine = circleLines.polylines[i];
                            polygonLines.AddPolyline(new Polyline());
                            circleLines.polylines.RemoveAt(i);
                            glControl1.Invalidate();

                            return rLine;
                        }
                    }
                    circleLines.polylines[i] = new Polyline(sav0.X, sav0.Y, sav1.X, sav1.Y);
                }

            }
            return null;
        }

        private void addMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void LoadHrefToShape(object sender, MouseEventArgs e)
        {
            glControl1.MouseClick -= LoadHrefToShape;
            Polyline shape = GetShape(new Point2D(e.X, glControl1.Height - e.Y - MenuY / glControl1.Height * (glControl1.Height - e.Y)));
            if (shape != null) {
                if (shapeToHref.ContainsKey(shape)) {
                    shapeToHref[shape] = textBox.Text;
                } else {
                    shapeToHref.Add(shape, textBox.Text);
                } }
        }

        public static void removeAllClickEvent(GLControl glControl1)
        {
            throw new NotImplementedException();
        }

        private void setHrefToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resetMouse();
            glControl1.MouseClick += LoadHrefToShape;
        }

        private void setAltToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resetMouse();
            glControl1.MouseClick += LoadAltToShape;
        }
        private void LoadAltToShape(object sender, MouseEventArgs e)
        {
            glControl1.MouseClick -= LoadAltToShape;
            Polyline shape = GetShape(new Point2D(e.X, glControl1.Height - e.Y - MenuY / glControl1.Height * (glControl1.Height - e.Y)));
            if (shape != null)
            {
                if (shapeToAlt.ContainsKey(shape))
                {
                    shapeToAlt[shape] = textBox.Text;
                }
                else
                {
                    shapeToAlt.Add(shape, textBox.Text);
                }
            }
        }
    }
}
