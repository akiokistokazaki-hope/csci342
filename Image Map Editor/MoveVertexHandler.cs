﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using mygraphicslib;
using OpenTK;

namespace Image_Map_Editor
{
    class MoveVertexHandler
    {
        public delegate void LineCompletedHandler(Polyline line);
        private double setY;
        private Point2D currentP;
        private EditorForm editor;
        public event LineCompletedHandler LineCompleted;

        public Polyline line { get; set; }
        private GLControl glControl;

        public Boolean IsComplete { get; private set; }

        public MoveVertexHandler()
        {

            IsComplete = false;
        }

        public void resetMouse(GLControl control)
        {
            control.MouseClick -= SelectShape;
            control.MouseClick -= SelectEnd;

        }

        public void Activate(GLControl control, EditorForm editor)
        {
            glControl = control;
            this.editor = editor;
            setY = EditorForm.MenuY / control.Height;
            glControl.MouseClick += SelectShape;

        }

        private void SelectShape(object sender, MouseEventArgs e)
        {
            currentP = (new Point2D(e.X, glControl.Height - e.Y - setY * (glControl.Height - e.Y)));
            line = editor.GetVertex(currentP);
            if (line != null)
            {
                glControl.MouseClick -= SelectShape;
                glControl.MouseMove += MoveAndUpdate;
                glControl.MouseClick += SelectEnd;
            }
            else
            {
                glControl.MouseClick -= SelectShape;
            }
        }

        private void MoveAndUpdate(object sender, MouseEventArgs e)
        {
            Point2D moveP = new Point2D(e.X, glControl.Height - e.Y - setY * (glControl.Height - e.Y));
            Point2D dif = new Point2D(moveP.X - currentP.X, moveP.Y - currentP.Y);
            int i = EditorForm.VertexIndex;
                line.points[i].X = line.points[i].X + dif.X;
                line.points[i].Y = line.points[i].Y + dif.Y;
            
            currentP = moveP;
            glControl.Invalidate();
        }

        private void SelectEnd(object sender, MouseEventArgs e)
        {
            Point2D moveP = new Point2D(e.X, glControl.Height - e.Y - setY * (glControl.Height - e.Y));
            Point2D dif = new Point2D(moveP.X - currentP.X, moveP.Y - currentP.Y);
            for (int i = 0; i < line.points.Count; i++)
            {
                line.points[i].X = line.points[i].X + dif.X;
                line.points[i].Y = line.points[i].Y + dif.Y;
            }

            glControl.MouseClick -= SelectEnd;
            glControl.MouseMove -= MoveAndUpdate;

            IsComplete = true;
            if (LineCompleted != null)
            {
                LineCompleted(line);
            }


            glControl.Invalidate();
        }

        
    }
}
