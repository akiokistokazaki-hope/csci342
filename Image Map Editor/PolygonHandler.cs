﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using mygraphicslib;
using OpenTK;


namespace Image_Map_Editor
{
    class PolygonHandler
    {
        public delegate void LineCompletedHandler(Polyline line);

        public event LineCompletedHandler LineCompleted;
        private double setY;
        public Polyline line { get; set; }
        private GLControl glControl;
        private int currentNumOfPoints = 1;

        public Boolean IsComplete { get; private set; }

        public PolygonHandler()
        {
        }

        public PolygonHandler(Polyline _line)
        {
            line = _line;
            IsComplete = false;
        }

        public void resetMouse(GLControl control)
        {
            control.MouseClick -= SelectFirstEndPointOfLine;
            control.MouseClick -= SelectNextPointOfLine;

        }

        public void Activate(GLControl control)
        {
            glControl = control;
            setY = EditorForm.MenuY / control.Height;
            glControl.MouseClick += SelectFirstEndPointOfLine;

        }

        private void SelectFirstEndPointOfLine(object sender, MouseEventArgs e)
        {
            line.AddPoint(new Point2D(e.X, glControl.Height - e.Y - setY * (glControl.Height - e.Y)));
            glControl.MouseClick -= SelectFirstEndPointOfLine;
            glControl.MouseMove += MouseMotionWhileSettingUpLine;
            glControl.Invalidate();
        }

        private void MouseMotionWhileSettingUpLine(object sender, MouseEventArgs e)
        {
            if (line.Count() > currentNumOfPoints)
            {
                line.RemoveLast();
            }
            else
            {
                //  First time we've moved the mouse after the first click, so allow the second click to occur
                glControl.MouseClick += SelectNextPointOfLine;
            }
            line.AddPoint(new Point2D(e.X, glControl.Height - e.Y - setY * (glControl.Height - e.Y)));
            glControl.Invalidate();
        }

        private void SelectNextPointOfLine(object sender, MouseEventArgs e)
        {
            line.RemoveLast();
            line.AddPoint(new Point2D(e.X, glControl.Height - e.Y - setY * (glControl.Height - e.Y)));
            currentNumOfPoints++;
            if (e.Button == MouseButtons.Right)
            {
                if (currentNumOfPoints < 3) {
                    EditorForm.polygonLines.RemoveLast();
                    }
                EditorForm.polygonLines.AddPolyline(new Polyline());
                glControl.MouseClick -= SelectNextPointOfLine;
                glControl.MouseMove -= MouseMotionWhileSettingUpLine;

                IsComplete = true;
                if (LineCompleted != null)
                {
                    LineCompleted(line);
                }
            }
            glControl.Invalidate();
        }

        
    }
}

