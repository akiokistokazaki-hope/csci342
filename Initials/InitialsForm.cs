﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;
using csci342;
using mygraphicslib;


namespace Initials
{
    public partial class InitialsForm : Form
    {
        private bool controlLoaded = false;
        private Polyline A;
        private Polyline K;
        private Polyline K2;

        private PolylineCollection polylines;

        public InitialsForm()
        {
            InitializeComponent();
            A = new Polyline();
            K = new Polyline();
            K2 = new Polyline();
            polylines = new PolylineCollection(A,K,K2);
            
            A.AddPoint(new Point2D(-2,-2));
            A.AddPoint(new Point2D(0, 2));
            A.AddPoint(new Point2D(0, 2));
            A.AddPoint(new Point2D(2, -2));
            A.AddPoint(new Point2D(-1, 0));
            A.AddPoint(new Point2D(1, 0));

            K.AddPoints(-1,-2,-1,2, -1,0,0.7,2, -1,0,0.8,-2);
            K2.AddPoints(-1, -2, -1, 2, -1, 0, 0.7, 2, -1, 0, 0.8, -2);
        }

        private void glControl1_Load(object sender, EventArgs e)
        {
            controlLoaded = true;
        }

        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            if (!controlLoaded) return;

            A.Draw(PrimitiveType.Lines);
        }

        private void glControl1_Resize(object sender, EventArgs e)
        {
            if (!controlLoaded) return;
            GL.Viewport(0, 0, glControl1.Width, glControl1.Height);
        }

        private void InitialsForm_Load(object sender, EventArgs e)
        {
            controlLoaded = true;
        }

        private void InitialsForm_Paint(object sender, PaintEventArgs e)
        {
            if (!controlLoaded) return;
            GL.ClearColor(Color.Blue);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.MatrixMode(MatrixMode.Projection);
            GL.Ortho(-2, 2, -2, 2, -2, 2);
           // A.Draw(PrimitiveType.Lines);
           // K.Draw(PrimitiveType.Lines);
           // K2.Draw(PrimitiveType.Lines);
            polylines.Draw(PrimitiveType.Lines);

           glControl1.SwapBuffers();
        }

        private void InitialsForm_Resize(object sender, EventArgs e)
        {
            if (!controlLoaded) return;
            GL.Viewport(0, 0, glControl1.Width, glControl1.Height);
        }
    }
}
