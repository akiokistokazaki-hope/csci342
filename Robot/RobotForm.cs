﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using csci342;
using mygraphicslib;

namespace Robot
{
    public partial class RobotForm : Form
    {
        #region Constants
        public const char BODY_POSITIVE = 'b';
        public const char BODY_NEGATIVE = 'B';
        public const char LEFT_ARM_POSITIVE = 'l';
        public const char LEFT_ARM_NEGATIVE = 'L';
        public const char LEFT_ELBOW_POSITIVE = 'e';
        public const char LEFT_ELBOW_NEGATIVE = 'E';
        public const char RIGHT_ELBOW_POSITIVE = 'x';
        public const char RIGHT_ELBOW_NEGATIVE = 'X';
        public const char RIGHT_ARM_POSITIVE = 'r';
        public const char RIGHT_ARM_NEGATIVE = 'R';
        public const char WAIST_POSITIVE = 'w';
        public const char WAIST_NEGATIVE = 'W';
        public const char HEAD_POSITIVE = 'h';
        public const char HEAD_NEGATIVE = 'H';
        #endregion

        #region Control Variables
        private int rotateAngle = 0;
        private int leftArmRotate = 0;
        private int rightArmRotate = 0;
        private int leftElbowRotate = 0;
        private int rightElbowRotate = 0;
        private int headRotate = 0;
        private int waistRotate = 0;
        #endregion





        public RobotForm()
        {
            InitializeComponent();
        }

        private void canvas_Load(object sender, EventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            DrawingTools.EnableDefaultAntiAliasing();
        }

        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GLU.Instance.Perspective(100, 1, 1, 40);
            var eye = new Point3D(-1, 1, 10);
            var look = new Point3D(0, 0, 0);
            GLU.Instance.LookAt(eye, look);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.Rotate(rotateAngle, 0, 1, 0);
            //robot head
            GL.PushMatrix();
            {
                GL.Rotate(waistRotate, 1, 0, 0);
                GL.Translate(0,4,0);
                GL.Rotate(headRotate, 0, 1, 0);
                GL.Translate(-1, 0, -1);
                GLU.Instance.DrawCube();
                GL.Translate(1,0,0);
                GLU.Instance.DrawCube();
                GL.Translate(1,0,0);
                GLU.Instance.DrawCube();
                GL.Translate(0, 0, 1);
                GLU.Instance.DrawCube();
                GL.Translate(-1,0,0);
                GLU.Instance.DrawCube();
                GL.Translate(-1, 0, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, 0, 1);
                GLU.Instance.DrawCube();
                GL.Translate(1, 0, 0);
                GLU.Instance.DrawCube();
                GL.Translate(1, 0, 0);
                GLU.Instance.DrawCube();
            }
            GL.PopMatrix();

            //left upper arm
            GL.PushMatrix();
            {
                GL.Rotate(waistRotate, 1, 0, 0);
                GL.Translate(1,2,0);
                GL.Rotate(leftArmRotate, 1, 0, 0);
                GLU.Instance.DrawCube();
                GL.Translate(1, 0, 0);
                GLU.Instance.DrawCube();
            }
            GL.PopMatrix();

            //left lower arm
            GL.PushMatrix();
            {
                GL.Rotate(waistRotate, 1, 0, 0);
                GL.Translate(2, 2, 0);
                GL.Rotate(leftArmRotate, 1, 0, 0);
                GL.Rotate(leftElbowRotate, 0, 0, 1);
                GL.Translate(1, 0, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, 1, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, 1, 0);
                GLU.Instance.DrawCube();
            }
            GL.PopMatrix();

            //right upper arm
            GL.PushMatrix();
            {
                GL.Rotate(waistRotate, 1, 0, 0);
                GL.Translate(-1, 2, 0);
                GL.Rotate(rightArmRotate, 1, 0, 0);
                GLU.Instance.DrawCube();
                GL.Translate(-1, 0, 0);
                GLU.Instance.DrawCube();
            }
            GL.PopMatrix();

            //right lower arm
            GL.PushMatrix();
            {
                GL.Rotate(waistRotate, 1, 0, 0);
                GL.Translate(-2,2,0);
                GL.Rotate(rightArmRotate, 1, 0, 0);
                GL.Rotate(rightElbowRotate, 0, 0, 1);
                GL.Translate(-1, 0, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, 1, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, 1, 0);
                GLU.Instance.DrawCube();
            }
            GL.PopMatrix();

            //upper body
            GL.PushMatrix();
            {
                GL.Rotate(waistRotate, 1, 0, 0);
                GL.Translate(0, 1, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, 1, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, 1, 0);
                GLU.Instance.DrawCube();
            }
            GL.PopMatrix();
			
			//lowerBody
            GL.PushMatrix();
            {
                GLU.Instance.DrawCube();
                GL.Translate(0, -1, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, -1, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, -1, 0);
                GLU.Instance.DrawCube();


                GL.Translate(-1, 3, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, -1, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, -1, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, -1, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, -1, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, -1, 0);
                GLU.Instance.DrawCube();


                GL.Translate(-1, 0, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, 0, 1);
                GLU.Instance.DrawCube();
                GL.Translate(1, 0, 0);
                GLU.Instance.DrawCube();

                GL.Translate(2, 5, -1);
                GLU.Instance.DrawCube();
                GL.Translate(0, -1, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, -1, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, -1, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, -1, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, -1, 0);
                GLU.Instance.DrawCube();

                GL.Translate(1, 0, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, 0, 1);
                GLU.Instance.DrawCube();
                GL.Translate(-1, 0, 0);
                GLU.Instance.DrawCube();
            }
            GL.PopMatrix();
			
            canvas.SwapBuffers();
        }

        private void canvas_Resize(object sender, EventArgs e)
        {
            canvas.Invalidate();
        }

        private void canvas_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case 'q':
                case 'Q':
                    Application.Exit();
                    break;

                //  Rotate left arm in positive direction
                case LEFT_ARM_POSITIVE:
                    leftArmRotate++;
                    break;

                //  Rotate left arm in negative direction
                case LEFT_ARM_NEGATIVE:
                    leftArmRotate--;
                    break;

                //  Rotate right arm in positive direction
                case RIGHT_ARM_POSITIVE:
                    rightArmRotate++;
                    break;

                //  Rotate right arm in negative direction
                case RIGHT_ARM_NEGATIVE:
                    rightArmRotate--;
                    break;

                //  Rotate left elbow in positive direction
                case LEFT_ELBOW_POSITIVE:
                    leftElbowRotate++;
                    break;

                //  Rotate left elbow in negative direction
                case LEFT_ELBOW_NEGATIVE:
                    leftElbowRotate--;
                    break;

                //  Rotate right elbow in positive direction
                case RIGHT_ELBOW_POSITIVE:
                    rightElbowRotate++;
                    break;

                //  Rotate right elbow in negative direction
                case RIGHT_ELBOW_NEGATIVE:
                    rightElbowRotate--;
                    break;

                //  Rotate head in positive direction
                case HEAD_POSITIVE:
                    headRotate++;
                    break;

                //  Rotate head in negative direction
                case HEAD_NEGATIVE:
                    headRotate--;
                    break;

                //  Rotate around neck in positive direction
                case WAIST_POSITIVE:
                    waistRotate++;
                    break;

                //  Rotate around neck in negative direction
                case WAIST_NEGATIVE:
                    waistRotate--;
                    break;

                //  Rotate the entire robot in positive direction
                case BODY_POSITIVE:
                    rotateAngle++;
                    break;

                //  Rotate the entire robot in negative direction
                case BODY_NEGATIVE:
                    rotateAngle--;
                    break;
            }
            canvas.Invalidate();
        }

        private void canvas_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.R) && ((e.Modifiers & Keys.Control) > 0))
            {
                Reset();
            }
        }

        public void Reset()
        {
            rotateAngle = 0;
            leftArmRotate = 0;
            rightArmRotate = 0;
            leftElbowRotate = 0;
            rightElbowRotate = 0;
            headRotate = 0;

            canvas.Invalidate();
        }
    }
}
