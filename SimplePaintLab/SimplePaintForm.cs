﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;
using mygraphicslib;

namespace SimplePaintLab
{
    public partial class SimplePaintForm : Form
    {
        private bool controlLoaded = false;
        public SimplePaintForm()
        {
            InitializeComponent();
        }

        private void SimplePaintForm_Load(object sender, EventArgs e)
        {

        }

        private void glControl1_Load(object sender, EventArgs e)
        {
            controlLoaded = true;
        }

        private void glControl1_Resize(object sender, EventArgs e)
        {
            if (!controlLoaded) return;
            GL.Viewport(0, 0, glControl1.Width, glControl1.Height);
        }

        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            if (!controlLoaded) return;
            GL.ClearColor(Color.Blue);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);



            GL.Begin(PrimitiveType.Lines);
            {
                GL.MatrixMode(MatrixMode.Projection);
                GL.Ortho(-1, 1, -1, 1, -1, 1);

                GL.Color3(Color.Orange);
                GL.Vertex2(-1, 1);
                GL.Vertex2(1, -1);

                GL.Vertex2(1, 1);
                GL.Vertex2(-1, -1);


            }
            GL.End();
            glControl1.SwapBuffers();
        }
    }
}
