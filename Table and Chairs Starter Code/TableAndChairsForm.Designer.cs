using OpenTK;

namespace Table_and_Chairs
{
    partial class TableAndChairsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.canvas = new OpenTK.GLControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnShowCameraControls = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // canvas
            // 
            this.canvas.BackColor = System.Drawing.Color.Black;
            this.canvas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.canvas.Location = new System.Drawing.Point(0, 0);
            this.canvas.Margin = new System.Windows.Forms.Padding(12, 12, 12, 12);
            this.canvas.Name = "canvas";
            this.canvas.Size = new System.Drawing.Size(1264, 1089);
            this.canvas.TabIndex = 0;
            this.canvas.VSync = false;
            this.canvas.Load += new System.EventHandler(this.canvas_Load);
            this.canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.canvas_Paint);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnShowCameraControls);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 1089);
            this.panel1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1264, 96);
            this.panel1.TabIndex = 1;
            // 
            // btnShowCameraControls
            // 
            this.btnShowCameraControls.Location = new System.Drawing.Point(505, 39);
            this.btnShowCameraControls.Name = "btnShowCameraControls";
            this.btnShowCameraControls.Size = new System.Drawing.Size(254, 45);
            this.btnShowCameraControls.TabIndex = 0;
            this.btnShowCameraControls.Text = "Show Camera Controls";
            this.btnShowCameraControls.UseVisualStyleBackColor = true;
            this.btnShowCameraControls.Click += new System.EventHandler(this.btnShowCameraControls_Click);
            // 
            // TableAndChairsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1264, 1185);
            this.Controls.Add(this.canvas);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "TableAndChairsForm";
            this.Text = "Drawing in 3D example";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private GLControl canvas;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnShowCameraControls;
    }
}

