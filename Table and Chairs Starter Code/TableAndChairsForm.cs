using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using csci342;
using csci342.Camera;
using csci342.GlutDrawingReplacements;
using OpenTK.Graphics.OpenGL;

namespace Table_and_Chairs
{
    public partial class TableAndChairsForm : Form
    {

        private Point3D _eye;
        private Point3D _look;

        private readonly int sphereComplexity = 50;

        private Camera _camera;

        // draw thin wall with top = xz-plane, corner at origin
        private void Wall(double thickness)
        {            
            GL.PushMatrix();
            {
                GL.Translate(0.5, 0.5 * thickness, 0.5);
                GL.Scale(1.0, thickness, 1.0);
                GLU.Instance.DrawCube(true);
            }
            GL.PopMatrix();
        }

        //  len is the vertical len of the leg; the leg is square in the xz direction
        private void TableLeg(double thick, double len)
        {
            GL.PushMatrix();
            {
                GL.Translate(0, len / 2, 0);
                GL.Scale(thick, len, thick);
                GLU.Instance.DrawCube(true);
            }
            GL.PopMatrix();
        }

        // draw one axis of the unit jack - a stretched sphere
        private void JackPart()
        {
            GL.PushMatrix();
            {
                GL.Scale(0.2, 0.2, 1.0);                
                GLU.Instance.SolidSphere(1, sphereComplexity, sphereComplexity);
            }
            GL.PopMatrix();

            GL.PushMatrix();
            {
                // ball on one end
                GL.Translate(0, 0, 1.2); 
                GLU.Instance.SolidSphere(0.2, sphereComplexity, sphereComplexity);

                // ball on the other end
                GL.Translate(0, 0, -2.4);
                GLU.Instance.SolidSphere(0.2, sphereComplexity, sphereComplexity);
            }
            GL.PopMatrix();
        }

        // draw a "unit jack" out of spheroids
        private void Jack()
        { 
            GL.PushMatrix();
            {
                JackPart();
                GL.Rotate(90.0, 0, 1, 0);
                JackPart();
                GL.Rotate(90.0, 1, 0, 0);
                JackPart();
            }
            GL.PopMatrix();
        }

        //<<<<<<<<<<<<<<<<<<<<<<< table >>>>>>>>>>>>>>>>>>>>
        private void Table(double topWid, double topThick, double legThick, double legLen)
        { // draw the table - a top and four legs

            // draw the table top
            GL.PushMatrix();
            {
                GL.Translate(0, legLen, 0);
                GL.Scale(topWid, topThick, topWid);
                GLU.Instance.DrawCube(true);
            }
            GL.PopMatrix();

            var dist = 0.95 * topWid / 2.0 - legThick / 2.0;
            GL.PushMatrix();
            {
                GL.Translate(dist, 0, dist);
                TableLeg(legThick, legLen);
                GL.Translate(0, 0, -2 * dist);
                TableLeg(legThick, legLen);
                GL.Translate(-2 * dist, 0, 2 * dist);
                TableLeg(legThick, legLen);
                GL.Translate(0, 0, -2 * dist);
                TableLeg(legThick, legLen);
            }
            GL.PopMatrix();
        }

        //<<<<<<<<<<<<<<<<<<<<<<< Chair >>>>>>>>>>>>>>>>>>>>
        private void Chair(double topWid, double topThick, double legThick, double legLen)
        { // draw the chair - a top, four legs, and back

            // draw the table top
            GL.PushMatrix();
            {
                GL.Translate(0, legLen, 0);
                GL.Scale(topWid, topThick, topWid);
                GLU.Instance.DrawCube(true);
            }
            GL.PopMatrix();

            var dist = 0.95 * topWid / 2.0 - legThick / 2.0;
            GL.PushMatrix();
            {
                GL.Translate(dist, 0, dist);
                TableLeg(legThick, legLen);
                GL.Translate(0, 0, -2 * dist);
                TableLeg(legThick, legLen);
                GL.Translate(-2 * dist, 0, 2 * dist);
                TableLeg(legThick, legLen);
                GL.Translate(0, 0, -2 * dist);
                TableLeg(legThick, legLen);
            }
            GL.PopMatrix();
        }
        //<<<<<<<<<<<<<<<<<<<<< displaySolid >>>>>>>>>>>>>>>>>>>>>>
        private void DisplaySolid()
        {
            SetupLighting();
                                  
            GLU.Instance.Perspective(60, 1.0 * canvas.Width / canvas.Height, 1, 20);
            
            GL.MatrixMode(MatrixMode.Modelview);
           
            //  Save the Modelview matrix, which is controlled by the camera initially, 
            //  so that our modeling transformations don't affect it for the next time
            //  we repaint
            GL.PushMatrix();
            {                
                // clear the screen and depth information
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

                // start drawing

                // wall #1: in xz-plane
                Wall(0.02);
                
                // wall #2: in yz-plane
                GL.PushMatrix();
                {
                    GL.Rotate(90.0, 0.0, 0.0, 1.0);
                    Wall(0.02);
                }
                GL.PopMatrix();
                
                // wall #3: in xy-plane
                GL.PushMatrix();
                {
                    GL.Rotate(-90.0, 1.0, 0.0, 0.0);
                    Wall(0.02);
                }
                GL.PopMatrix();
                
                GL.PushMatrix();
                {
                    GL.Translate(0.4, 0.4, 0.6);
                    GL.Rotate(45, 0, 0, 1);
                    GL.Scale(0.08, 0.08, 0.08);
                    Jack();
                }
                GL.PopMatrix();
                
                GL.PushMatrix();
                {
                    GL.Translate(0.6, 0.38, 0.5);
                    GL.Rotate(30, 0, 1, 0);
                    //Glut.glutSolidTeapot(0.08);
                    //  Sorry, no teapot yet :-(
                }
                GL.PopMatrix();
                
                GL.PushMatrix();
                {
                    GL.Translate(0.25, 0.42, 0.35);
                    GLU.Instance.SolidSphere(0.1, 30, 30);
                }
                GL.PopMatrix();
                
                GL.PushMatrix();
                {
                    GL.Translate(0.4, 0, 0.4);
                    Table(0.6, 0.02, 0.02, 0.3);
                }
                GL.PopMatrix();

                GL.PushMatrix();
                {
                    GL.Translate(0.4, 0, 0.4);
                    Table(0.6, 0.02, 0.02, 0.3);
                }
                GL.PopMatrix();

                GL.PushMatrix();
                {
                    GL.Translate(0.5, 0, 0.8);
                    Chair(0.2, 0.02, 0.02, 0.15);
                    
                }
                GL.PopMatrix();
                GL.PushMatrix();
                {
                GL.Translate(0.45, 0.195, 0.917);
                GL.Scale(0.2, 0.3, 0.9);
                GL.Rotate(90.0, 0.0, 0.0, 1.0);
                GL.Rotate(90.0, 1.0, 0.0, 0.0);
                Wall(0.02);
                }
                GL.PopMatrix();
            }
            GL.PopMatrix();       
        }

        private void SetupLighting()
        {
            // set properties of the surface material
            float[] matAmbient = {0.7f, 0.7f, 0.7f, 1.0f}; // gray
            float[] matDiffuse = {0.6f, 0.6f, 0.6f, 1.0f};
            float[] matSpecular = {1.0f, 1.0f, 1.0f, 1.0f};
            float[] matShininess = {50.0f};

            GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Ambient, matAmbient);
            GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, matDiffuse);
            GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Specular, matSpecular);
            GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Shininess, matShininess);

            // set the light source properties
            float[] lightIntensity = {0.7f, 0.7f, 0.7f, 1.0f};
            float[] lightPosition = {2.0f, 6.0f, 3.0f, 0.0f};
            GL.Light(LightName.Light0, LightParameter.Position, lightPosition);
            GL.Light(LightName.Light0, LightParameter.Ambient, lightIntensity);
            GL.Light(LightName.Light0, LightParameter.Diffuse, lightIntensity);
        }

        public TableAndChairsForm()
        {
            InitializeComponent();     
        }

        private void canvas_Load(object sender, EventArgs e)
        {            
            // enable the light source
            GL.Enable(EnableCap.Lighting); 
            GL.Enable(EnableCap.Light0);
            GL.ShadeModel(ShadingModel.Smooth);
            // for hidden surface removal
            GL.Enable(EnableCap.DepthTest);
            // normalize vectors for proper shading
            GL.Enable(EnableCap.Normalize);
            // background is light gray
            GL.ClearColor(0.1f, 0.1f, 0.1f, 0.0f);

            _eye = new Point3D(2.3, 1.3, 2);
            _look = new Point3D(0, 0.25, 0);

            _camera = new Camera(_eye, _look);
            _camera.ActivateCameraControls(canvas);       
        }

        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            if (!canvas.IsHandleCreated) return;

            DisplaySolid();
            canvas.SwapBuffers();
        }
        
        private void btnShowCameraControls_Click(object sender, EventArgs e)
        {
            var controls = new CameraControls(_camera, canvas);
            controls.Show(this);
        }
    }
}