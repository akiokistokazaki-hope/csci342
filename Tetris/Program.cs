﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    static class Program
    {
        static TextReader input = Console.In;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(String[] args)
        {
            if (args.Length > 2)
            {
                System.Console.WriteLine("Please do not enter more than 2 arguments");
                return;
            }
            if (args.Length == 2)
            {
                var path = args[1];
                if (File.Exists(path))
                {
                    input = File.OpenText(path);
                }
            }

            // use `input` for all input operations
            for (string line; (line = input.ReadLine()) != null; )
            {
                Console.WriteLine(line);
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (args.Length == 1)
            {
                Application.Run(new TetrisForm(args));
            }
            else {
                Application.Run(new TetrisForm());
            }
        }
    }
}
