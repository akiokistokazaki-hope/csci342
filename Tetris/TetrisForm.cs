﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;
using csci342;
// hi = y, si = x
namespace WindowsFormsApplication4
{
    public partial class TetrisForm : Form
    {
        private bool test = false;
        private bool controlLoaded = false;
        private List<List<Color>> board;
        private List<List<Point2D>> grid;
        private List<Point2D> current;
        private Point2D A;
        private Point2D B;
        private Point2D C;
        private Point2D D;
        private Point2D Edefault;
        private bool GamePause = false;
        private int addX = 10;
        private int subY = -12;

        private List<List<Point2D>> Pgrid;
        private List<Point2D> Pcurrent;
        private Point2D PA;
        private Point2D PB;
        private Point2D PC;
        private Point2D PD;
        private Point2D Pdefault;

        private List<List<Point2D>> P2grid;
        private List<Point2D> P2current;
        //private Point2D P2A;
        //private Point2D P2B;
        //private Point2D P2C;
        //private Point2D P2D;
        //private Point2D P2default; 

        private int P = 3;
        private int P2 = 2;
        private Color Pcolor = Color.Yellow;
        private Color P2color = Color.Yellow;
        private int deletedLine = 0;

        public TetrisForm()
        {
            test = false;
            InitializeComponent();
            SetUp();
        }

        public TetrisForm(String[] args)
        {
            InitializeComponent();
            if(args[0].Equals("test")){
                test = true;
            }
            else { test = false; }
            SetUp();
        }
        

        private void SetUp(){
            grid = new List<List<Point2D>>();
            current = new List<Point2D>();
            A = new Point2D(0, 0);
            B = new Point2D(0, 0);
            C = new Point2D(0, 0);
            D = new Point2D(0, 0);
            Edefault = new Point2D(0, 0);
            for (int o = 0; o < 3; o++)
            {
                List<Point2D> pointRow = new List<Point2D>();
                for (int i = 0; i < 3; i++)
                {
                    pointRow.Add(Edefault);
                }
                grid.Add(pointRow);
            }
            current.Add(A);
            current.Add(B);
            current.Add(C);
            current.Add(D);

            // set the board
            board = new List<List<Color>>();
            List<Color> row = new List<Color>();
            for (int i = 0; i < 14; i++)
            {
                row.Add(Color.Black);
            }
            for (int i = 0; i < 8; i++)
            {
                row.Add(Color.Blue);
            }
            board.Add(row);
            for (int o = 0; o < 19; o++)
            {
                row = new List<Color>();
                row.Add(Color.Black);
                for (int i = 0; i < 12; i++)
                {
                    row.Add(Color.White);
                }
                row.Add(Color.Black);
                for (int ex = 15; ex < 20; ex++)
                {
                    row.Add(Color.Blue);
                }
                board.Add(row);
            }

            Pgrid = new List<List<Point2D>>();
            Pcurrent = new List<Point2D>();
            PA = new Point2D(0, 0);
            PB = new Point2D(0, 0);
            PC = new Point2D(0, 0);
            PD = new Point2D(0, 0);
            Pdefault = new Point2D(0, 0);
            for (int o = 0; o < 3; o++)
            {
                List<Point2D> PpointRow = new List<Point2D>();
                for (int i = 0; i < 3; i++)
                {
                    PpointRow.Add(Pdefault);
                }
                Pgrid.Add(PpointRow);
            }
            Pcurrent.Add(PA);
            Pcurrent.Add(PB);
            Pcurrent.Add(PC);
            Pcurrent.Add(PD);

            P2grid = new List<List<Point2D>>();
            P2current = new List<Point2D>();
            for (int o = 0; o < 3; o++)
            {
                List<Point2D> P2pointRow = new List<Point2D>();
                for (int i = 0; i < 3; i++)
                {
                    P2pointRow.Add(new Point2D(0, 0));
                }
                P2grid.Add(P2pointRow);
            }

            P2grid[0][0] = new Point2D(0, 0);
            P2grid[0][1] = new Point2D(0, 0);
            P2grid[0][2] = new Point2D(0, 0);
            P2grid[1][0] = new Point2D(0, 0);
            P2grid[1][1] = new Point2D(0, 0);
            P2grid[1][2] = new Point2D(0, 0);
            P2grid[2][0] = new Point2D(0, 0);
            P2grid[2][1] = new Point2D(0, 0);
            P2grid[2][2] = new Point2D(0, 0);
    }

        private void glControl1_Load(object sender, EventArgs e)
        {
            controlLoaded = true;

        }

        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            if (!controlLoaded) return;


            GL.ClearColor(Color.Blue);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.MatrixMode(MatrixMode.Projection);
            GL.Ortho(0, 20, 0, 20, -1, 1);
            Pcolor = RandomColor();
            P = RandomCreate(Pcolor);
            MoveUp();
            GenerateNewPiece();
            ReDraw();
            timer1.Start();
        }


        // glControl1.Width, glControl1.Height
        private void ReDraw()
        {
            GL.ClearColor(Color.Blue);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.Begin(PrimitiveType.Quads);
            {

                for (int o = 0; o < 19; o++)
                {
                    for (int i = 0; i < 19; i++)
                    {
                        if (board[o][i] != null)
                        {
                        GL.Color3(board[o][i]);
                        GL.Vertex2(i, o);
                        GL.Vertex2(i, o + 1);
                        GL.Vertex2(i + 1, o + 1);
                        GL.Vertex2(i + 1, o);
                    }
                    }
                }
            }
            GL.End();

            // draw the line
            GL.Begin(PrimitiveType.Lines);
            {
                GL.Color3(Color.Black);
                for (int hi = 2; hi < 19; hi++)
                {
                    GL.Vertex2(1, hi);
                    GL.Vertex2(14, hi);
                }
                for (int si = 2; si < 15; si++)
                {
                    GL.Vertex2(si, 0);
                    GL.Vertex2(si, 19);
                }
            }

            GL.End();
            glControl1.SwapBuffers();
        }

        private int RandomCreate(Color color)
        {
            Random random = new Random();
            int randomNumber = random.Next(0, 4);
            switch (randomNumber)
            {
                case 0:
                    CreateLP(color);
                    return randomNumber;
                   // break;
                case 1:
                    CreateZP(color);
                    return randomNumber;
                  //  break;
                case 2:
                    CreateTP(color);
                    return randomNumber;
                  //  break;
                case 3:
                    CreateStickP(color);
                    return randomNumber;
                  //  break;
            }
            return 100;
        }

        private void Create(int next, Color color)
        {
            if (!test) { timer1.Stop(); }
            switch (next)
            {
                case 0:
                    CreateL(color);
                    break;
                case 1:
                    CreateZ(color);
                    break;
                case 2:
                    CreateT(color);
                    break;
                case 3:
                    CreateStick(color);
                    break;
            }
            ReDraw();
            if (!test) { timer1.Start(); }
        }

        private void CreateL(Color color)
        {
            Random random = new Random();
            int rand = random.Next(1, 9);
            if (test) { rand = 0; }
            int randR = random.Next(0, 4);
            if (test) { randR = 0; }
            grid[0][0] = Edefault;
            grid[0][1] = Edefault;
            grid[0][2] = Edefault;
            grid[1][0] = A;
            grid[1][1] = B;
            grid[1][2] = C;
            grid[2][0] = Edefault;
            grid[2][1] = Edefault;
            grid[2][2] = D;
            //6,7 18,17,16
            C.X = 1 + rand; C.Y = 18;
            B.X = 1 + rand; B.Y = 17;
            A.X = 1 + rand; A.Y = 16;
            D.X = 2 + rand; D.Y = 18;
            //Color color = RandomColor();
            board[18][1 + rand] = color;
            board[17][1 + rand] = color;
            board[16][1 + rand] = color;
            board[18][2 + rand] = color;
            for (int i = 0; i < randR;i++)
            {
                if (randR == 1) { RotateClock(); }
                RotateClock();
            }
        }
        private void CreateZ(Color color)
        {
            Random random = new Random();
            int rand = random.Next(0, 10);
            if (test) { rand = 0; }
            
            int randR = random.Next(0, 2);
            if (test) { randR = 0; }
            grid[0][0] = Edefault;
            grid[0][1] = Edefault;
            grid[0][2] = A;
            grid[1][0] = Edefault;
            grid[1][1] = C;
            grid[1][2] = B;
            grid[2][0] = Edefault;
            grid[2][1] = D;
            grid[2][2] = Edefault;
            //6,7 18,17,16
            A.X = 1 + rand; A.Y = 18;
            B.X = 2 + rand; B.Y = 18;
            C.X = 2 + rand; C.Y = 17;
            D.X = 3 + rand; D.Y = 17;
            //Color color = RandomColor();
            board[18][1 + rand] = color;
            board[18][2 + rand] = color;
            board[17][2 + rand] = color;
            board[17][3+ rand] = color;
            for (int i = 0; i < randR; i++)
            {
                RotateClock();
            }
        }
        private void CreateT(Color color)
        {
            Random random = new Random();
            int rand = random.Next(0, 10);
            if (test) { rand = 0; }
            int randR = random.Next(0, 4);
            if (test) { randR = 0; }
            grid[0][0] = Edefault;
            grid[0][1] = A;
            grid[0][2] = Edefault;
            grid[1][0] = Edefault;
            grid[1][1] = B;
            grid[1][2] = D;
            grid[2][0] = Edefault;
            grid[2][1] = C;
            grid[2][2] = Edefault;
            //6,7 18,17,16
            A.X = 1 + rand; A.Y = 17;
            B.X = 2 + rand; B.Y = 17;
            C.X = 3 + rand; C.Y = 17;
            D.X = 2 + rand; D.Y = 18;
            //Color color = RandomColor();
            board[18][2 + rand] = color;
            board[17][1 + rand] = color;
            board[17][2 + rand] = color;
            board[17][3+rand] = color;
            for (int i = 0; i < randR; i++)
            {
                if(randR==2){RotateClock();}
                RotateClock();
            }
        }
        private void CreateStick(Color color)
        {
            Random random = new Random();
            int rand = random.Next(0, 10);
            if (test) { rand = 0; }

            int randR = random.Next(0, 2);
            if (test) { randR = 0; }
            grid[0][0] = Edefault;
            grid[0][1] = A;
            grid[0][2] = Edefault;
            grid[1][0] = Edefault;
            grid[1][1] = B;
            grid[1][2] = Edefault;
            grid[2][0] = Edefault;
            grid[2][1] = C;
            grid[2][2] = Edefault;
            //6,7 18,17,16
            A.X = 1 + rand; A.Y = 18;
            B.X = 2 + rand; B.Y = 18;
            C.X = 3 + rand; C.Y = 18;
            //Color color = RandomColor();
            board[18][1 + rand] = color;
            board[18][2 + rand] = color;
            board[18][3 + rand] = color;
            for (int i = 0; i < randR; i++)
            {
                if(randR ==1){
                    MoveCurrentOneDown();
                    RotateClock();
                }
                
                
            }
        }

        private Color RandomColor()
        {
            Random random = new Random();
            int randomNumber = random.Next(0, 3);
            switch (randomNumber)
            {
                case 0:
                    return Color.Red;
                case 1:
                    return Color.Orange;
                case 2:
                    return Color.Green;
                default:
                    return Color.Purple;
            }

        }

        private void MoveCurrentDown()
        {
            while (MoveCurrentOneDown());
            GenerateNewPiece();
        }


        private bool MoveCurrentOneDown()
        {
            if (!Valid()) { return false; }
            bool goOn = true;
            bool notChecked = true;
            for (int o = 0; o < 3; o++)
            {
                notChecked = true;
                for (int i = 0; i < 3; i++)
                {

                    if (grid[o][i].X != 0)
                    {
                        int y = (int)grid[o][i].Y;
                        int x = (int)grid[o][i].X;
                        if (notChecked)
                        {
                            if (!board[y - 2][x].Equals(Color.White))
                            {
                                goOn = false;
                            }
                            notChecked = false;
                        }
                        board[y - 1][x] = board[y][x];
                        board[y][x] = Color.White;
                        grid[o][i].Y = y - 1;
                    }
                }
            }
            ReDraw();
            return goOn;
        }

        private bool MoveLeft()
        {
            bool goOn = true;
            bool notChecked = true;
            for (int o = 0; o < 3; o++)
            {
                notChecked = true;
                for (int i = 0; i < 3; i++)
                {

                    if (grid[i][o].X != 0)
                    {
                        int y = (int)grid[i][o].Y;
                        int x = (int)grid[i][o].X;
                        if (notChecked)
                        {
                            if (!board[y][x - 1].Equals(Color.White))
                            {
                                goOn = false;
                            }
                            notChecked = false;
                        }
                    }
                }
            }
            if (!goOn) { return goOn; }
            for (int o = 0; o < 3; o++)
            {
                for (int i = 0; i < 3; i++)
                {

                    if (grid[i][o].X != 0)
                    {
                        int y = (int)grid[i][o].Y;
                        int x = (int)grid[i][o].X;
                        board[y][x - 1] = board[y][x];
                        board[y][x] = Color.White;
                        grid[i][o].X = x - 1;
                    }
                }
            }
            ReDraw();
            if (!Valid())
            {
                GenerateNewPiece();
            }
            return goOn;
        }

        private bool MoveRight()
        {
            bool goOn = true;
            bool notChecked = true;
            for (int o = 0; o < 3; o++)
            {
                notChecked = true;
                for (int i = 2; i > -1; i--)
                {
                    if (grid[i][o].X != 0)
                    {
                        int y = (int)grid[i][o].Y;
                        int x = (int)grid[i][o].X;
                        if (notChecked)
                        {
                            if (!board[y][x + 1].Equals(Color.White))
                            {
                                return false;
                            }
                            notChecked = false;
                        }
                    }
                }
            }

            for (int o = 0; o < 3; o++)
            {
                for (int i = 2; i > -1; i--)
                {

                    if (grid[i][o].X != 0)
                    {
                        int y = (int)grid[i][o].Y;
                        int x = (int)grid[i][o].X;
                        board[y][x + 1] = board[y][x];
                        board[y][x] = Color.White;
                        grid[i][o].X = x + 1;
                    }
                }
            }
            ReDraw();
            
            if(!Valid()){
                GenerateNewPiece();
            }
            return goOn;
        }

        private bool RotateCounterClock()
        {
            if (grid[2][1].Y == 18 || grid[2][0].Y == 17)
            {
                return false;
            }
            if (!Valid()) { return false; }

            bool goOn = true;
            Point2D re = grid[0][1];
            if (re.X != 0)
            {
                if (grid[1][0].X == 0 && !Color.White.Equals(board[(int)re.Y - 1][(int)re.X + 1]))
                {
                    return false;
                }
            }
            re = grid[1][2];
            if (re.X != 0)
            {
                if (grid[0][1].X == 0 && !Color.White.Equals(board[(int)re.Y - 1][(int)re.X - 1]))
                {
                    return false;
                }
            }
            re = grid[2][1];
            if (re.X != 0)
            {
                if (grid[1][2].X == 0 && !Color.White.Equals(board[(int)re.Y + 1][(int)re.X - 1]))
                {
                    return false;
                }
            }
            re = grid[1][0];
            if (re.X != 0)
            {
                if (grid[2][1].X == 0 && !Color.White.Equals(board[(int)re.Y + 1][(int)re.X + 1]))
                {
                    return false;
                }
            }

            re = grid[0][0];
            if (re.X != 0)
            {
                if (grid[2][0].X == 0 && !Color.White.Equals(board[(int)re.Y][(int)re.X + 2]))
                {
                    return false;
                }
            }

            re = grid[0][2];
            if (re.X != 0)
            {
                if (grid[0][0].X == 0 && !Color.White.Equals(board[(int)re.Y - 2][(int)re.X]))
                {
                    return false;
                }
            }
            re = grid[2][2];
            if (re.X != 0)
            {
                if (grid[0][2].X == 0 && !Color.White.Equals(board[(int)re.Y][(int)re.X - 2]))
                {
                    return false;
                }
            }
            re = grid[2][0];
            if (re.X != 0)
            {
                if (grid[2][2].X == 0 && !Color.White.Equals(board[(int)re.Y + 2][(int)re.X]))
                {
                    return false;
                }
            }



            bool setWhite = false;
            re = grid[0][1];
            if (re.X != 0)
            {
                board[(int)re.Y - 1][(int)re.X + 1] = board[(int)re.Y][(int)re.X];
                board[(int)re.Y][(int)re.X] = Color.White;
                re.X += 1;
                re.Y -= 1;
            }
            else { setWhite = true; }
            re = grid[1][2];
            if (re.X != 0)
            {
                board[(int)re.Y - 1][(int)re.X - 1] = board[(int)re.Y][(int)re.X];
                board[(int)re.Y][(int)re.X] = Color.White;
                re.X -= 1;
                re.Y -= 1;
            }
            re = grid[2][1];
            if (re.X != 0)
            {
                board[(int)re.Y + 1][(int)re.X - 1] = board[(int)re.Y][(int)re.X];
                board[(int)re.Y][(int)re.X] = Color.White;
                re.X -= 1;
                re.Y += 1;
            }
            re = grid[1][0];
            if (re.X != 0)
            {
                board[(int)re.Y + 1][(int)re.X + 1] = board[(int)re.Y][(int)re.X];
                if (setWhite) { board[(int)re.Y][(int)re.X] = Color.White; }
                re.X += 1;
                re.Y += 1;
            }
            re = grid[1][0];
            grid[1][0] = grid[0][1];
            grid[0][1] = grid[1][2];
            grid[1][2] = grid[2][1];
            grid[2][1] = re;

            // kurosu houkou
            setWhite = false;

            re = grid[0][0];
            if (re.X != 0)
            {
                board[(int)re.Y][(int)re.X + 2] = board[(int)re.Y][(int)re.X];
                board[(int)re.Y][(int)re.X] = Color.White;
                re.X += 2;
            }
            else { setWhite = true; }

            re = grid[2][0];
            if (re.X != 0)
            {
                board[(int)re.Y + 2][(int)re.X] = board[(int)re.Y][(int)re.X];
                board[(int)re.Y][(int)re.X] = Color.White;
                re.Y += 2;
            }
            re = grid[2][2];
            if (re.X != 0)
            {
                board[(int)re.Y][(int)re.X - 2] = board[(int)re.Y][(int)re.X];
                board[(int)re.Y][(int)re.X] = Color.White;
                re.X -= 2;
            }
            re = grid[0][2];
            if (re.X != 0)
            {
                board[(int)re.Y - 2][(int)re.X] = board[(int)re.Y][(int)re.X];
                if (setWhite) { board[(int)re.Y][(int)re.X] = Color.White; }
                re.Y -= 2;
            }
            re = grid[2][0];
            grid[2][0] = grid[0][0];
            grid[0][0] = grid[0][2];
            grid[0][2] = grid[2][2];
            grid[2][2] = re;



            ReDraw();
            if (!Valid())
            {
                GenerateNewPiece();
            }
            return goOn;

        }

        

        private bool RotateClock()
        {
            if (grid[0][1].Y == 18 || grid[0][0].Y == 17)
            {
                return false;
            }
            if (!Valid()) { return false; }

            bool goOn = true;
            Point2D re = grid[0][1];
            if (re.X != 0)
            {
                if (grid[1][2].X == 0 && !Color.White.Equals(board[(int)re.Y + 1][(int)re.X + 1]))
                {
                    return false;
                }
            }
            re = grid[1][0];
            if (re.X != 0)
            {
                if (grid[0][1].X == 0 && !Color.White.Equals(board[(int)re.Y + 1][(int)re.X - 1]))
                {
                    return false;
                }
            }
            re = grid[2][1];
            if (re.X != 0)
            {
                if (grid[1][0].X == 0 && !Color.White.Equals(board[(int)re.Y - 1][(int)re.X - 1]))
                {
                    return false;
                }
            }
            re = grid[1][2];
            if (re.X != 0)
            {
                if (grid[2][1].X == 0 && !Color.White.Equals(board[(int)re.Y - 1][(int)re.X + 1]))
                {
                    return false;
                }
            }

            re = grid[0][0];
            if (re.X != 0)
            {
                if (grid[0][2].X == 0 && !Color.White.Equals(board[(int)re.Y + 2][(int)re.X]))
                {
                    return false;
                }
            }

            re = grid[2][0];
            if (re.X != 0)
            {
                if (grid[0][0].X == 0 && !Color.White.Equals(board[(int)re.Y][(int)re.X - 2]))
                {
                    return false;
                }
            }
            re = grid[2][2];
            if (re.X != 0)
            {
                if (grid[2][0].X == 0 && !Color.White.Equals(board[(int)re.Y - 2][(int)re.X]))
                {
                    return false;
                }
            }
            re = grid[0][2];
            if (re.X != 0)
            {
                if (grid[2][2].X == 0 && !Color.White.Equals(board[(int)re.Y][(int)re.X + 2]))
                {
                    return false;
                }
            }



            bool setWhite = false;
            re = grid[0][1];
            if (re.X != 0)
            {
                board[(int)re.Y + 1][(int)re.X + 1] = board[(int)re.Y][(int)re.X];
                board[(int)re.Y][(int)re.X] = Color.White;
                re.X += 1;
                re.Y += 1;
            }
            else { setWhite = true; }
            re = grid[1][0];
            if (re.X != 0)
            {
                board[(int)re.Y + 1][(int)re.X - 1] = board[(int)re.Y][(int)re.X];
                board[(int)re.Y][(int)re.X] = Color.White;
                re.X -= 1;
                re.Y += 1;
            }
            re = grid[2][1];
            if (re.X != 0)
            {
                board[(int)re.Y - 1][(int)re.X - 1] = board[(int)re.Y][(int)re.X];
                board[(int)re.Y][(int)re.X] = Color.White;
                re.X -= 1;
                re.Y -= 1;
            }
            re = grid[1][2];
            if (re.X != 0)
            {
                board[(int)re.Y - 1][(int)re.X + 1] = board[(int)re.Y][(int)re.X];
                if (setWhite) { board[(int)re.Y][(int)re.X] = Color.White; }
                re.X += 1;
                re.Y -= 1;
            }
            re = grid[1][2];
            grid[1][2] = grid[0][1];
            grid[0][1] = grid[1][0];
            grid[1][0] = grid[2][1];
            grid[2][1] = re;

            // kurosu houkou
            setWhite = false;

            re = grid[0][0];
            if (re.X != 0)
            {
                board[(int)re.Y + 2][(int)re.X] = board[(int)re.Y][(int)re.X];
                board[(int)re.Y][(int)re.X] = Color.White;
                re.Y += 2;
            }
            else { setWhite = true; }

            re = grid[2][0];
            if (re.X != 0)
            {
                board[(int)re.Y][(int)re.X - 2] = board[(int)re.Y][(int)re.X];
                board[(int)re.Y][(int)re.X] = Color.White;
                re.X -= 2;
            }
            re = grid[2][2];
            if (re.X != 0)
            {
                board[(int)re.Y - 2][(int)re.X] = board[(int)re.Y][(int)re.X];
                board[(int)re.Y][(int)re.X] = Color.White;
                re.Y -= 2;
            }
            re = grid[0][2];
            if (re.X != 0)
            {
                board[(int)re.Y][(int)re.X + 2] = board[(int)re.Y][(int)re.X];
                if (setWhite) { board[(int)re.Y][(int)re.X] = Color.White; }
                re.X += 2;
            }
            re = grid[0][2];
            grid[0][2] = grid[0][0];
            grid[0][0] = grid[2][0];
            grid[2][0] = grid[2][2];
            grid[2][2] = re;



             ReDraw();
            if (!Valid())
            {
                GenerateNewPiece();
            }
            return goOn;

        }

        private bool Reflect()
        {
            if (grid[0][0].Y == 17 || grid[1][0].Y == 17 || grid[2][0].Y == 17)
            {
                return false;
            }
            Color color = Color.White;
            
            if (!Valid()) { return false; }

            bool goOn = true;
            Point2D re = grid[0][0];
            if (re.X != 0)
            {
                if (grid[0][2].X == 0 && !Color.White.Equals(board[(int)re.Y + 2][(int)re.X]))
                {
                    return false;
                }
            }
            re = grid[1][0];
            if (re.X != 0)
            {
                if (grid[1][2].X == 0 && !Color.White.Equals(board[(int)re.Y + 2][(int)re.X]))
                {
                    return false;
                }
            }
            re = grid[2][0];
            if (re.X != 0)
            {
                if (grid[2][2].X == 0 && !Color.White.Equals(board[(int)re.Y + 2][(int)re.X]))
                {
                    return false;
                }
            }
            re = grid[0][2];
            if (re.X != 0)
            {
                if (grid[0][0].X == 0 && !Color.White.Equals(board[(int)re.Y - 2][(int)re.X]))
                {
                    return false;
                }
            }

            re = grid[1][2];
            if (re.X != 0)
            {
                if (grid[1][0].X == 0 && !Color.White.Equals(board[(int)re.Y - 2][(int)re.X]))
                {
                    return false;
                }
            }

            re = grid[2][2];
            if (re.X != 0)
            {
                if (grid[2][0].X == 0 && !Color.White.Equals(board[(int)re.Y -2][(int)re.X]))
                {
                    return false;
                }
            }
            


            bool setWhite = false;
            re = grid[0][0];
            if (re.X != 0)
            {
                board[(int)re.Y + 2][(int)re.X] = board[(int)re.Y][(int)re.X];
                board[(int)re.Y][(int)re.X] = Color.White;
                re.Y += 2;
                setWhite = false;
            }
            else { setWhite = true; }
            re = grid[0][2];
            if (re.X != 0)
            {
                board[(int)re.Y - 2][(int)re.X] = board[(int)re.Y][(int)re.X];
                if (setWhite) { board[(int)re.Y][(int)re.X] = Color.White; }
                re.Y -= 2;
                setWhite = false;
            }
            else { setWhite = true; }
            re = grid[1][0];
            if (re.X != 0)
            {
                board[(int)re.Y + 2][(int)re.X] = board[(int)re.Y][(int)re.X];
                board[(int)re.Y][(int)re.X] = Color.White;
                re.Y += 2;
                setWhite = false;
            }
            else { setWhite = true; }
            re = grid[1][2];
            if (re.X != 0)
            {
                board[(int)re.Y - 2][(int)re.X] = board[(int)re.Y][(int)re.X];
                if (setWhite) { board[(int)re.Y][(int)re.X] = Color.White; }
                re.Y -= 2;
                setWhite = false;
            }
            else { setWhite = true; }
            re = grid[2][0];
            if (re.X != 0)
            {
                board[(int)re.Y + 2][(int)re.X] = board[(int)re.Y][(int)re.X];
                board[(int)re.Y][(int)re.X] = Color.White;
                re.Y += 2;
                setWhite = false;
            }
            else { setWhite = true; }
            re = grid[2][2];
            if (re.X != 0)
            {
                board[(int)re.Y - 2][(int)re.X] = board[(int)re.Y][(int)re.X];
                if (setWhite) { board[(int)re.Y][(int)re.X] = Color.White; }
                re.Y -= 2;
                setWhite = false;
            }
            else { setWhite = true; }
            re = grid[0][2];
            grid[0][2] = grid[0][0];
            grid[0][0] = re;
            re = grid[1][2];
            grid[1][2] = grid[1][0];
            grid[1][0] = re;
            re = grid[2][2];
            grid[2][2] = grid[2][0];
            grid[2][0] = re;
            ReDraw();
            if (!Valid())
            {
                GenerateNewPiece();
            }
            return goOn;
        }

        private bool Valid()
        {
            bool goOn = true;
            bool notChecked = true;
            for (int o = 0; o < 3; o++)
            {
                notChecked = true;
                for (int i = 0; i < 3; i++)
                {

                    if (grid[o][i].X != 0)
                    {
                        int y = (int)grid[o][i].Y;
                        int x = (int)grid[o][i].X;
                        if (notChecked)
                        {
                            if (!board[y - 1][x].Equals(Color.White))
                            {
                                goOn = false;
                            }
                            notChecked = false;
                        }
                    }
                }
            }
            return goOn;
        }

        // glControl1.Width, glControl1.Height
        private void glControl1_Resize(object sender, EventArgs e)
        {
            //GL.Viewport(0, 0, glControl1.Width, glControl1.Height);
        }

        private void HellowWorldForm_Load(object sender, EventArgs e)
        {

        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Left)
            {
                MoveLeft();
                return true;
            }
            if (keyData == Keys.Right)
            {
                MoveRight();
                return true;
            }
            if (keyData == Keys.Space)
            {
                if (test)
                {
                    Reflect();
                    return true;
                }
            }
            return base.ProcessDialogKey(keyData);
        }

        private void glControl1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'd' || e.KeyChar == 'j')
            {
                if (!MoveCurrentOneDown())
                {
                    GenerateNewPiece();
                }
            }
            if (e.KeyChar == 'D')
            {
                MoveCurrentDown();
            }
            if (e.KeyChar == 'r')
            {
                RotateClock();
            }

            if (e.KeyChar == 'R')
            {
                RotateCounterClock();
            }

            if (e.KeyChar == 'p' || e.KeyChar == 'P')
            {
                if (!GamePause)
                {
                    timer1.Stop();
                    GamePause = true;
                }
                else
                {
                    timer1.Start();
                    GamePause = false;
                }
            }


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!test) { 
            if (!MoveCurrentOneDown())
            {
                GenerateNewPiece();
            }
        }
        }

        private void MoveUp()
        {
            P2 = P;
            P2color = Pcolor;
            Pcolor = RandomColor();
            board[12][15] = Color.Blue;
            board[13][15] = Color.Blue;
            board[14][15] = Color.Blue;
            board[12][16] = Color.Blue;
            board[13][16] = Color.Blue;
            board[14][16] = Color.Blue;
            board[12][17] = Color.Blue;
            board[13][17] = Color.Blue;
            board[14][17] = Color.Blue;


            for (int o = 0; o < 3; o++)
            {
                for (int i = 0; i < 3; i++)
                {
                    P2grid[i][o].X = Pgrid[i][o].X;
                    P2grid[i][o].Y = Pgrid[i][o].Y;
                }
            }

            for (int o = 0; o < 3; o++)
            {
                for (int i = 0; i < 3; i++)
                {
                    Point2D re = P2grid[i][o];
                    if(re.X != 0){
                    board[(int)re.Y + 8][(int)re.X] = board[(int)re.Y][(int)re.X];
                    }
                }
            }

            board[4][15] = Color.Blue;
            board[5][15] = Color.Blue;
            board[6][15] = Color.Blue;
            board[4][16] = Color.Blue;
            board[5][16] = Color.Blue;
            board[6][16] = Color.Blue;
            board[4][17] = Color.Blue;
            board[5][17] = Color.Blue;
            board[6][17] = Color.Blue;

            P = RandomCreate(Pcolor);
            ReDraw();
        }

        private void GenerateNewPiece()
        {
            deleteRow();
            Create(P2, P2color);
            MoveUp();
            ReDraw();
        }

        private void deleteRow()
        {
            int to = 0;
            int howMany = 0;
            for(int o= 1;o<19;o++){
                for (int i = 1; i< 13 ;i++ ) {
                    if(!board[o][i].Equals(Color.White)){
                        if(i == 12){
                            
                            to = o;
                            howMany++;
                        }
                    }
                    else{
                    i=13;
                    }
                }
            }
            if(to != 0){
            removeRow(to, howMany);
            }
        }
        private void removeRow(int to, int howMany) {

            for (int o = 0; o < howMany;o++ )
            {
                for (int r = 1; r<13 ;r++ )
                {
                    board[to - o][r] = Color.White;
                }
            }


            for (int o = to + 1; o < 19 ; o++ )
            {
                for (int i = 1; i < 13;i++ )
                {
                    board[o - howMany][i] = board[o][i];
                }
            }
            deletedLine += howMany;
            textBox1.Text = deletedLine.ToString();
            ReDraw();
            if (deletedLine > 50) { timer1.Interval =100; }
            else if (deletedLine > 40) {  timer1.Interval = 150; }
            else if (deletedLine > 30) { timer1.Interval = 250; }
            else if (deletedLine > 20) { timer1.Interval = 350; }
            else if (deletedLine > 10) { timer1.Interval = 450; }
            
        }

        private void CreateLP(Color color)
        {
            Pgrid[0][0] = Pdefault;
            Pgrid[0][1] = Pdefault;
            Pgrid[0][2] = Pdefault;
            Pgrid[1][0] = PA;
            Pgrid[1][1] = PB;
            Pgrid[1][2] = PC;
            Pgrid[2][0] = Pdefault;
            Pgrid[2][1] = Pdefault;
            Pgrid[2][2] = PD;
            //6,7 18,17,16
            PC.X = 6 + addX; PC.Y = 18 + subY;
            PB.X = 6 + addX; PB.Y = 17 + subY;
            PA.X = 6 + addX; PA.Y = 16 + subY;
            PD.X = 7 + addX; PD.Y = 18 + subY;
            board[18 + subY][6 + addX] = color;
            board[17 + subY][6 + addX] = color;
            board[16 + subY][6 + addX] = color;
            board[18 + subY][7 + addX] = color;
        }
        private void CreateZP(Color color)
        {
            Pgrid[0][0] = Pdefault;
            Pgrid[0][1] = Pdefault;
            Pgrid[0][2] = PA;
            Pgrid[1][0] = Pdefault;
            Pgrid[1][1] = PC;
            Pgrid[1][2] = PB;
            Pgrid[2][0] = Pdefault;
            Pgrid[2][1] = PD;
            Pgrid[2][2] = Pdefault;
            //6,7 18,17,16
            PA.X = 5 + addX; PA.Y = 18 + subY;
            PB.X = 6 + addX; PB.Y = 18 + subY;
            PC.X = 6 + addX; PC.Y = 17 + subY;
            PD.X = 7 + addX; PD.Y = 17 + subY;
            board[18 + subY][5 + addX] = color;
            board[18 + subY][6 + addX] = color;
            board[17 + subY][6 + addX] = color;
            board[17 + subY][7 + addX] = color;
        }
        private void CreateTP(Color color)
        {
            Pgrid[0][0] = Pdefault;
            Pgrid[0][1] = PA;
            Pgrid[0][2] = Pdefault;
            Pgrid[1][0] = Pdefault;
            Pgrid[1][1] = PB;
            Pgrid[1][2] = PD;
            Pgrid[2][0] = Pdefault;
            Pgrid[2][1] = PC;
            Pgrid[2][2] = Pdefault;
            //6,7 18,17,16
            PA.X = 5 + addX; PA.Y = 17 + subY;
            PB.X = 6 + addX; PB.Y = 17 + subY;
            PC.X = 7 + addX; PC.Y = 17 + subY;
            PD.X = 6 + addX; PD.Y = 18 + subY;
            board[18 + subY][6 + addX] = color;
            board[17 + subY][5 + addX] = color;
            board[17 + subY][6 + addX] = color;
            board[17 + subY][7 + addX] = color;
        }
        private void CreateStickP(Color color)
        {
            Pgrid[0][0] = Pdefault;
            Pgrid[0][1] = PA;
            Pgrid[0][2] = Pdefault;
            Pgrid[1][0] = Pdefault;
            Pgrid[1][1] = PB;
            Pgrid[1][2] = Pdefault;
            Pgrid[2][0] = Pdefault;
            Pgrid[2][1] = PC;
            Pgrid[2][2] = Pdefault;
            //6,7 18,17,16
            PA.X = 5 + addX; PA.Y = 18 + subY;
            PB.X = 6 + addX; PB.Y = 18 + subY;
            PC.X = 7 + addX; PC.Y = 18 + subY;
            board[18 + subY][5 + addX] = color;
            board[18 + subY][6 + addX] = color;
            board[18 + subY][7 + addX] = color;
        }

    }
}