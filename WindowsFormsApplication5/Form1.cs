﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;

using csci342;
using mygraphicslib;

namespace WindowsFormsApplication5
{
    public partial class Form1 : Form
    {
        private bool controlLoaded = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void glControl1_Load_1(object sender, EventArgs e)
        {
            controlLoaded = true;
        }

        private void glControl1_Paint_1(object sender, PaintEventArgs e)
        {
            if (!controlLoaded) return;

            GL.ClearColor(Color.White);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.MatrixMode(MatrixMode.Projection);
            GL.Ortho(-50, 50, -50, 50, -50, 50);
            GL.Begin(PrimitiveType.LineLoop);
        {
           GL.Color3(Color.Black);
           Utilities.DrawArc(0, 0, 25, 0, (float)(Math.PI * 2));
            }
            GL.End();

            GL.Begin(PrimitiveType.LineStrip);
            {
                Utilities.DrawArc(9, 7, 5, 0, (float)(Math.PI * 2)/2);
            }
            GL.End();
            GL.Begin(PrimitiveType.LineStrip);
            {
                Utilities.DrawArc(-9, 7, 5, 0, (float)(Math.PI * 2) / 2);
            }
            GL.End();

            GL.Begin(PrimitiveType.LineStrip);
            {
                Utilities.DrawArc(0, -5, 10, 0, (float)(Math.PI * 2) / 2*-1);
            }
            GL.End();
            glControl1.SwapBuffers();
        }

        private void glControl1_Resize_1(object sender, EventArgs e)
        {
           // GL.Viewport(0, 0, glControl1.Width, glControl1.Height);
        }
    }


}

