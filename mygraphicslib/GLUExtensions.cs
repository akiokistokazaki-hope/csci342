﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;
using csci342;

namespace mygraphicslib
{
    public static class GLUExtensions
    {
        public static void DrawCube(this GLU glu, bool solid = false)
        {
            PrimitiveType vertexType = PrimitiveType.LineLoop;
                        if (solid)
                            {
                vertexType = PrimitiveType.Polygon;
                            }
            GL.Begin(vertexType);
                        {
                GL.Vertex3(-0.5, -0.5, -0.5);
                GL.Vertex3(0.5, -0.5, -0.5);
                GL.Vertex3(0.5, 0.5, -0.5);
                GL.Vertex3(-0.5, 0.5, -0.5);
                            }
            GL.End();
            GL.Begin(vertexType);
                        {
                GL.Vertex3(-0.5, -0.5, 0.5);
                GL.Vertex3(0.5, -0.5, 0.5);
                GL.Vertex3(0.5, 0.5, 0.5);
                GL.Vertex3(-0.5, 0.5, 0.5);
                            }
            GL.End();
            GL.Begin(vertexType);
                        {
                GL.Vertex3(-0.5, -0.5, -0.5);
                GL.Vertex3(-0.5, 0.5, -0.5);
                GL.Vertex3(-0.5, 0.5, 0.5);
                GL.Vertex3(-0.5, -0.5, 0.5);
                            }
            GL.End();
            GL.Begin(vertexType);
                        {
                GL.Vertex3(0.5, -0.5, -0.5);
                GL.Vertex3(0.5, 0.5, -0.5);
                GL.Vertex3(0.5, 0.5, 0.5);
                GL.Vertex3(0.5, -0.5, 0.5);
                            }
            GL.End();
            GL.Begin(vertexType);
                        {
                GL.Vertex3(-0.5, 0.5, -0.5);
                GL.Vertex3(0.5, 0.5, -0.5);
                GL.Vertex3(0.5, 0.5, 0.5);
                GL.Vertex3(-0.5, 0.5, 0.5);
                            }
            GL.End();
            GL.Begin(vertexType);
                        {
                GL.Vertex3(-0.5, -0.5, -0.5);
                GL.Vertex3(0.5, -0.5, -0.5);
                GL.Vertex3(0.5, -0.5, 0.5);
                GL.Vertex3(-0.5, -0.5, 0.5);
                            }
            GL.End();
        }


        public static void SolidSphere(this GLU glu, double radius, int verticalDivisions, int horizontalDivisions)
        {

            float Ydis = (float)radius/((float)verticalDivisions/2);
            float currentYdis;
            float currentY;
            float currentX;
            float currentZ;
            float inc = (float)360 / horizontalDivisions;
            float currentAngle = 0;
            // o for outerloop, i for innerloop
            GL.Begin(PrimitiveType.QuadStrip);
            { 
                for (int o = 1; o <= verticalDivisions/2; o++)
                {
                    currentYdis = Ydis*o;
                    currentY = (float)radius - currentYdis;
                    currentAngle = 0;
                    for (int i = 0; i < horizontalDivisions; i++)
                    {
                        currentX = (float)Math.Sqrt(radius*radius - currentY*currentY)* (float)Math.Cos(currentAngle);
                        currentZ = (float)Math.Sqrt(radius*radius - currentY*currentY)* (float)Math.Sin(currentAngle);
                        GL.Normal3(currentX, currentY, currentZ);
                        GL.Vertex3(currentX, currentY, currentZ);


                        currentY = (float)radius - Ydis*(o + 1);
                        currentX = (float)Math.Sqrt(radius*radius - currentY*currentY)* (float)Math.Cos(currentAngle);
                        currentZ = (float)Math.Sqrt(radius*radius - currentY*currentY)* (float)Math.Sin(currentAngle);
                        GL.Normal3(currentX, currentY, currentZ);
                        GL.Vertex3(currentX, currentY, currentZ);
                        currentAngle += inc;
                        currentY = (float)radius - currentYdis;
                    }
                }
                for (int o = 1; o <= verticalDivisions/2; o++)
                {
                    currentYdis = Ydis*o;
                    currentY = (float)radius - currentYdis;
                    currentAngle = 0;
                    for (int i = 0; i < horizontalDivisions; i++)
                    {
                        currentX = (float)Math.Sqrt(radius*radius - currentY*currentY)* (float)Math.Cos(currentAngle);
                        currentZ = (float)Math.Sqrt(radius*radius - currentY*currentY)* (float)Math.Sin(currentAngle);
                        GL.Normal3(currentX, -1*currentY, currentZ);
                        GL.Vertex3(currentX, -1*currentY, currentZ);

                        currentY = (float)radius - Ydis*(o + 1);
                        currentX = (float)Math.Sqrt(radius*radius - currentY*currentY)* (float)Math.Cos(currentAngle);
                        currentZ = (float)Math.Sqrt(radius*radius - currentY*currentY)* (float)Math.Sin(currentAngle);
                        GL.Normal3(currentX, -1*currentY, currentZ);
                        GL.Vertex3(currentX, -1*currentY, currentZ);
                        currentAngle += inc;
                        currentY = (float)radius - currentYdis;
                    }
                }
            }
            GL.End();

            currentY = (float)radius - Ydis;
            GL.Begin(PrimitiveType.TriangleFan);
            {
                GL.Normal3(0, radius, 0);
                GL.Vertex3(0, radius, 0);
                currentAngle = 0;
                for (int i = 0; i < horizontalDivisions; i++)
                {
                    currentX = (float)Math.Sqrt(radius*radius - currentY*currentY)* (float)Math.Cos(currentAngle);
                    currentZ = (float)Math.Sqrt(radius*radius - currentY*currentY)* (float)Math.Sin(currentAngle);
                    GL.Normal3(currentX, currentY, currentZ);
                    GL.Vertex3(currentX, currentY, currentZ);
                    currentAngle += inc;
                }
            }
            GL.End();
            GL.Begin(PrimitiveType.TriangleFan);
            {
                GL.Vertex3(0, -1*radius, 0);
                currentAngle = 0;
                for (int i = 0; i < horizontalDivisions; i++)
                {
                    currentX = (float)Math.Sqrt(radius*radius - currentY*currentY)* (float)Math.Cos(currentAngle);
                    currentZ = (float)Math.Sqrt(radius*radius - currentY*currentY)* (float)Math.Sin(currentAngle);
                    GL.Normal3(currentX, -1*currentY, currentZ);
                    GL.Vertex3(currentX, -1*currentY, currentZ);
                    currentAngle += inc;
                }
            }
            GL.End();
        }


        public static void WireSphere(this GLU glu, double radius, int verticalDivisions, int horizontalDivisions)
        {
            float Ydis = (float)radius / ((float)verticalDivisions / 2);

            float currentYdis;
            float currentY;
            float currentX;
            float currentZ;
            float inc = (float)360 / horizontalDivisions;
            float currentAngle = 0;
            // o for outerloop, i for innerloop


            for (int o = 0; o <= verticalDivisions/2; o++)
            {
                currentYdis = Ydis*o;
                currentY = (float)radius - currentYdis;
                GL.Begin(PrimitiveType.LineStrip);
                {
                    for (int i = 0; i <= horizontalDivisions; i++)
                    {
                        currentX = (float)Math.Sqrt(radius*radius - currentY*currentY)* (float)Math.Cos(currentAngle);
                        currentZ = (float)Math.Sqrt(radius*radius - currentY*currentY)* (float)Math.Sin(currentAngle);
                        GL.Vertex3(currentX, currentY, currentZ);
                        currentAngle += inc;
                    }
                }
                GL.End();
            }

            for (int o = 0; o <= verticalDivisions/2; o++)
            {
                currentYdis = Ydis*o;
                currentY = (float)radius - currentYdis;
                GL.Begin(PrimitiveType.LineStrip);
                {
                    for (int i = 0; i <= horizontalDivisions; i++)
                    {
                        currentX = (float)Math.Sqrt(radius*radius - currentY*currentY)* (float)Math.Cos(currentAngle);
                        currentZ = (float)Math.Sqrt(radius*radius - currentY*currentY)* (float)Math.Sin(currentAngle);
                        GL.Vertex3(currentX, -1*currentY, currentZ);
                        currentAngle += inc;
                    }
                }
                GL.End();
            }

            float Xdis = (float)radius / ((float)horizontalDivisions / 2);
            float currentXdis;
            for (int o = 0; o <= horizontalDivisions/2; o++)
            {
                currentXdis = Xdis*o;
                currentX = (float)radius - currentXdis;

                GL.Begin(PrimitiveType.LineStrip);
                {
                    for (int i = 0; i <= verticalDivisions; i++)
                    {
                        currentY = (float)Math.Sqrt(radius*radius - currentX*currentX)* (float)Math.Cos(currentAngle);
                        currentZ = (float)Math.Sqrt(radius*radius - currentX*currentX)* (float)Math.Sin(currentAngle);
                        GL.Vertex3(currentX, currentY, currentZ);
                        currentAngle += inc;
                    }
                }
                GL.End();
            }
            for (int o = 0; o <= horizontalDivisions/2; o++)
            {
                currentXdis = Xdis*o;
                currentX = (float)radius - currentXdis;

                GL.Begin(PrimitiveType.LineStrip);
                {
                    for (int i = 0; i <= verticalDivisions; i++)
                    {
                        currentY = (float)Math.Sqrt(radius*radius - currentX*currentX)* (float)Math.Cos(currentAngle);
                        currentZ = (float)Math.Sqrt(radius*radius - currentX*currentX)* (float)Math.Sin(currentAngle);
                        GL.Vertex3(-1*currentX, currentY, currentZ);
                        currentAngle += inc;
                    }
                }
                GL.End();
            }
        }

        public static void WireCylinder(this GLU glu, double baseRadius = 1, double topRadius = 1, double height = 1,
            int numSlices = 50, int numStacks = 50)
        {
            // Z direnction 0 to 1
            float Zdis = (float)baseRadius / ((float)numSlices / 2);

            float currentZdis;
            float currentX;
            float currentY;
            float currentZ;
            float inc = (float)360 / numStacks;
            float currentAngle = 0;

            for (int o = 0; o <= numSlices / 2; o++)
            {
                currentZdis = Zdis * o;
                currentZ = (float)baseRadius - currentZdis;
                GL.Begin(PrimitiveType.LineStrip);
                {
                    for (int i = 0; i <= numStacks; i++)
                    {
                        currentX = (float)topRadius * (float)Math.Cos(currentAngle);
                        currentY = (float)topRadius * (float)Math.Sin(currentAngle);
                        GL.Vertex3(currentX, currentY, currentZ);
                        currentAngle += inc;
                    }
                }
                GL.End();
            }
            currentAngle = 0;
            for (int o = 0; o <= numSlices / 2; o++)
            {
                currentZdis = Zdis * o;
                currentZ = (float)baseRadius - currentZdis;
                GL.Begin(PrimitiveType.LineStrip);
                {
                    for (int i = 0; i <= numStacks; i++)
                    {
                        currentX = (float)topRadius * (float)Math.Cos(currentAngle);
                        currentY = (float)topRadius * (float)Math.Sin(currentAngle);
                        GL.Vertex3(currentX, currentY, -1*currentZ);
                        currentAngle += inc;
                    }
                }
                GL.End();
            }

            currentAngle = 0;
            for (int i = 0; i <= numStacks; i++)
            {
                currentX = (float)topRadius * (float)Math.Cos(currentAngle);
                currentY = (float)topRadius * (float)Math.Sin(currentAngle);
                currentAngle += inc;
                GL.Begin(PrimitiveType.LineStrip);
                {
                    for (int o = 0; o <= numSlices / 2; o++)
                    {
                        currentZ = (float)baseRadius - Zdis * o;
                        GL.Vertex3(currentX, currentY, currentZ);
                    }

                    for (int o = 0; o <= numSlices / 2; o++)
                    {
                        currentZ = (float)baseRadius - Zdis * o;
                        GL.Vertex3(currentX, currentY, -1 * currentZ);
                    }
                }
                GL.End();
                }
           }
        
    }
}