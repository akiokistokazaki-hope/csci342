﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using csci342;

using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;
using mygraphicslib;



namespace mygraphicslib
{
    public class Polyline : IEnumerable<Point2D>
    {
        public List<Point2D> points;
        //private Vector2 radiusVec;

        public Polyline()
        {
            points = new List<Point2D>();
        }

        public Polyline(Point2D pt) : this()
        {
            points.Add(pt);
            //AddPoint(pt);
        }



        public Polyline(params double[] values): this()
        {
            AddPoints(values);
        }

        public int Count()
        {
            return points.Count();
        }

        public void RemoveLast()
        {
            points.Remove(points[Count()-1]);
        }

        public void AddPoint(Point2D pt)
        {
            points.Add(pt);
        }

        public void AddPoints(params double[] values)
        {
            if(values.Length % 2 == 1){
                throw new ArgumentException("odd number of doubles invalid", "argument");
            }
            for (int i = 0; i < values.Length; i= i+2){
                AddPoint(new Point2D(values[i], values[i + 1]));
            }
        }

        public void Draw(PrimitiveType par)
        {
            GL.Begin(par);
            foreach(var point in points)
            {
                GL.Vertex2(point.X, point.Y);
            }
            GL.End();
        }

        public IEnumerator<Point2D> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
           return GetEnumerator();
        }

        public void DrawRectFromTwoPoints(PrimitiveType lines)
        {

            GL.Begin(PrimitiveType.LineLoop);
            if (points.Count() > 1)
            {
                GL.Vertex2(points[0].X, points[0].Y);
                GL.Vertex2(points[1].X, points[0].Y);
                GL.Vertex2(points[1].X, points[1].Y);
                GL.Vertex2(points[0].X, points[1].Y);
            }
            GL.End();
        }

        public void DrawRectPointFromTwoPoints(PrimitiveType lines)
        {
            GL.PointSize(7);

            GL.Begin(PrimitiveType.Points);
            if (points.Count() > 1)
            {
                GL.Vertex2(points[0].X, points[0].Y);
                GL.Vertex2(points[1].X, points[0].Y);
                GL.Vertex2(points[1].X, points[1].Y);
                GL.Vertex2(points[0].X, points[1].Y);
            }
            GL.End();
        }

        public void DrawCircleFromTwoPoints(PrimitiveType lines)
        {
            GL.Begin(PrimitiveType.LineStrip);
            Vector2 radiusVec = new Vector2((float)points[1].X - (float)points[0].X, (float)points[1].Y - (float)points[0].Y);
            Utilities.DrawCleanArc(points[0].X+ radiusVec.X/2, points[0].Y + radiusVec.Y/2, radiusVec.Length/2);
            GL.End();
        }

        public void DrawCirclePointFromTwoPoints(PrimitiveType lines)
        {
            GL.PointSize(7);
            GL.Begin(PrimitiveType.Points);
            Vector2 radiusVec = new Vector2((float)points[1].X - (float)points[0].X, (float)points[1].Y - (float)points[0].Y);
            Utilities.DrawArcPoints(points[0].X + radiusVec.X / 2, points[0].Y + radiusVec.Y / 2, radiusVec.Length / 2);
            GL.End();
        }



        public double GetRadius() {
                Vector2 radiusVec = new Vector2((float)points[1].X - (float)points[0].X, (float)points[1].Y - (float)points[0].Y);
                return radiusVec.Length / 2; 
        }
        public Point2D GetMidPoint(){
            Vector2 radiusVec = new Vector2((float)points[1].X - (float)points[0].X, (float)points[1].Y - (float)points[0].Y);
            return new Point2D(points[0].X + radiusVec.X / 2, points[0].Y + radiusVec.Y / 2);
        }
        public void DrawPolygonLine(bool isComp = false) {
            GL.Begin(PrimitiveType.Lines);
            {
                int last = points.Count()-1;
                for (int i = 0; i < last ;i++) {
                    GL.Vertex2(points[i].X, points[i].Y);
                    GL.Vertex2(points[i+1].X, points[i+1].Y);
                }
                if (isComp) {
                    GL.Vertex2(points[last].X, points[last].Y);
                    GL.Vertex2(points[0].X, points[0].Y);
                }
            }
            GL.End();
        }
        public void DrawPolygonPoint(bool isComp = false)
        {
            GL.PointSize(7);
            GL.Begin(PrimitiveType.Points);
            {
                int last = points.Count()-1;
                for (int i = 0; i < last; i++)
                {
                    GL.Vertex2(points[i].X, points[i].Y);
                }
                if (isComp)
                {
                    GL.Vertex2(points[last].X, points[last].Y);
                }
            }
            GL.End();
        }

        public void SetFourPoints()
        {
            if (points.Count ==2) {
                AddPoint(new Point2D(points[0].X, points[1].Y));
                AddPoint(new Point2D(points[1].X, points[0].Y));
            }
        }

        public void Set18Points()
        {
            if (points.Count == 2)
            {
                
                Vector2 radiusVec = new Vector2((float)points[1].X - (float)points[0].X, (float)points[1].Y - (float)points[0].Y);
                float r = radiusVec.Length / 2;
                double cx = points[0].X + radiusVec.X / 2;
                double cy = points[0].Y + radiusVec.Y / 2;
                points.RemoveAt(1);
                points.RemoveAt(0);
                
                int i, n = 18;
                double x, y = 0.5;
                double rate;

                for (i = 0; i < n; i++)
                {
                    rate = (double)i / n;
                    x = r * Math.Cos(2.0 * Math.PI * rate) + cx;
                    y = r * Math.Sin(2.0 * Math.PI * rate) + cy;
                    AddPoint(new Point2D(x, y));
                }
            }
        }
    }
}
