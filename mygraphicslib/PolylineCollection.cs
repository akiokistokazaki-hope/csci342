﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using csci342;

namespace mygraphicslib
{
    public class PolylineCollection
    {
        public List<Polyline> polylines;

        public PolylineCollection()
        {
            polylines = new List<Polyline>();
        }

        public PolylineCollection(Point2D pt) : this()
        {
            polylines.Add(new Polyline(pt));
            //AddPlyline(new Polyline(pt))
        }

        public PolylineCollection(Polyline polyline) : this()
        {
            polylines.Add(polyline);
            //AddPlyline(new Polyline(pt))
        }


        public PolylineCollection(params Polyline[] polys): this()
        {
            AddPolylines(polys);
        }

        public void RemoveLast()
        {
            polylines.Remove(polylines[polylines.Count() - 1]);
        }

        public void AddPolyline(Polyline polyline){
            polylines.Add(polyline);
         }

        public void AddPolylines(params Polyline[] polys)
        {
            for (int i = 0; i < polys.Length; i++)
            {
                AddPolyline(polys[i]);
            }
        }

        public void Draw(PrimitiveType par)
        {
            foreach (Polyline polyline in polylines) {
                polyline.Draw(par);
            }
        }

    }
}
