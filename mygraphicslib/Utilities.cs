﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using csci342;

namespace mygraphicslib
{

    public class Utilities
    {
        public static void SetWindow
            (double left, double right, double bottom, double top)
        {
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity(); try
            {
                GL.Ortho(left, right, bottom, top, -1, 1);
            }
            catch (Exception ex)
            {
                throw;
            }
            
        }

        public static void DrawArc(double cx=0, double cy=0, double r=1, float startAngle=0, float sweep= (float)(Math.PI * 2))
        {
            int n = 100;
            float increment = sweep/n;
            float currentAngle = startAngle;
            for (int i = 0; i < n; i++)
            {
                float currentX = (float)(r * Math.Cos(currentAngle) + cx);
                float currentY = (float)(r * Math.Sin(currentAngle) + cy);
                GL.Vertex2(currentX, currentY);
                currentAngle = currentAngle + increment;
            }
            GL.Vertex2(r * (float)Math.Cos(startAngle + sweep) + (float)cx, r * (float)Math.Sin(startAngle + sweep) + (float)cy);
        }

        public static void DrawArcPoints(double cx = 0, double cy = 0, float r = 1, float startAngle = 0, float sweep = 360)
        {

            int i, n = 18;
            double x, y = 0.5;
            double rate;

            for (i = 0; i < n; i++)
            {
                rate = (double)i / n;
                x = r * Math.Cos(2.0 * Math.PI * rate) + cx;
                y = r * Math.Sin(2.0 * Math.PI * rate) + cy;
                GL.Vertex2(x, y);
            }

            
        }



    public static void DrawCleanArc(double cx = 0, double cy = 0, float r = 1, float startAngle = 0, float sweep = 360)
    {
        int n = 360;//4960;
        double rate;

        for (int i = 0; i < n; i++)
        {
            rate = (double)i / n;
            double x = r * Math.Cos(2.0 * Math.PI * rate) + cx;
            double y = r * Math.Sin(2.0 * Math.PI * rate) + cy;
            GL.Vertex2(x, y);
        }

    }
}
}
