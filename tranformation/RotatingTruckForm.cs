﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK.Graphics;
using csci342;
using mygraphicslib;
using OpenTK;
using OpenTK.Graphics.OpenGL;


namespace tranformation
{
    public partial class RotatingTruckForm : Form
    {
        private double angle;

        public RotatingTruckForm()
        {
            angle = 0;
            InitializeComponent();
        }

        private void canvas_Load(object sender, EventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            Utilities.SetWindow(-10, 10, -10, 10);
        }

        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            GL.PushMatrix();
            {
               GL.Rotate(90, 0, 0, 1);
               GL.Scale(1, -1, 1);
               GL.Rotate(angle, 0, 0, -1);
               GL.Translate(0, 5, 0);
               DrawingTools.DrawTruck();
            }
            GL.PopMatrix();
            GL.Begin(PrimitiveType.LineLoop);
            Utilities.DrawArc(0,0,5,0, (float)(Math.PI * 2));
            GL.End();
            canvas.SwapBuffers();
        }


        private void canvas_Resize(object sender, EventArgs e)
        {
            canvas.Invalidate();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            angle = angle + 1;
            angle = angle%360;

            canvas.Invalidate();
        }
    }
}
